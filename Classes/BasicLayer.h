#ifndef __TEXT_LAYER_H__
#define __TEXT_LAYER_H__

#include "cocos2d.h"
USING_NS_CC;

#include "NGUI/NProtocol.h"
#include "NGUI/NWidget.h"
#include "NGUI/NButton.h"
#include "NGUI/NCheckBox.h"
#include "NGUI/NAudioManager.h"
#include "NGUI/NScrollView.h"
#include "NGUI/NListView.h"
#include "NGUI/NLabelTTF.h"
#include "NGUI/NRichText.h"
#include "NGUI/NProgressBar.h"
#include "NGUI/NSliderBar.h"
#include "NGUI/NBar.h"
#include "NGUI/NScrollBar.h"
//#include "NGUI/NLabel.h"
#include "NGUI/NTableView.h"
#include "NGUI/NPageView.h"
#include "NGUI/NGridView.h"
#include "NGUI/NMessageTips.h"
#include "NGUI/NJoystick.h"
#include "NGUI/NSwitchButton.h"
#include "NGUI/NXMLManager.h"

class BasicLayer : public Layer
{
public:
	BasicLayer(void);
	~BasicLayer(void);

	CREATE_FUNC(BasicLayer);
	virtual bool init();

protected:
	virtual void setDescription(const string& description);
	virtual void setTitle(const string& title);

	virtual void btnNextCallback(Ref* object, Touch_Period touch_period) { };
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period) { };
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period) { };
	virtual void btnMenuCallback(Ref* object, Touch_Period touch_period);

	void setIndex(int index);
	int getIndex() const;

protected:
	NButton* _btn_next;
	NButton* _btn_back;
	NButton* _btn_again;
	NButton* _btn_menu;
	LabelTTF* _lbl_title;
	LabelTTF* _lbl_description;

	int _index;
};

#endif // __TEXT_LAYER_H__