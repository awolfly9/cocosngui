#include "NSliderBar.h"

NSliderBar::NSliderBar(void)
	: _slider_bar(nullptr)
{
	this->setUniqueID(NPROGRESS_BAR);
}

NSliderBar::~NSliderBar(void)
{
}

NSliderBar* NSliderBar::create()
{
	return create("", "", nullptr);
}

NSliderBar* NSliderBar::create(const string& background, const string& progress_bar, NButton* slide_bar)
{
	NSliderBar* ret = new NSliderBar();

	if (ret && ret->init(background, progress_bar, slide_bar))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

NSliderBar* NSliderBar::create(const string& background, const string& progress_bar, const string& normal_image, const string& selected_image, const string& disabled_image, const string& click_effect)
{
	NSliderBar* ret = new NSliderBar();

	if (ret && ret->init(background, progress_bar, normal_image, selected_image, disabled_image, click_effect))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

bool NSliderBar::init(const string& background, const string& progress_bar, NButton* slide_bar)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NProgressBar::init(background, progress_bar));

	this->setSliderBar(slide_bar);
	this->setTouchEnabled(false);

	INIT_DO_WHILE_END
}

bool NSliderBar::init(const string& background, const string& progress_bar, const string& normal_image, const string& selected_image, const string& disabled_image, const string& click_effect)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NProgressBar::init(background, progress_bar));

	this->setSliderBar(normal_image, selected_image, disabled_image, click_effect);
	this->setTouchEnabled(false);

	INIT_DO_WHILE_END
}

void NSliderBar::setSliderBar(NButton* slider_ball)
{
	if (slider_ball == nullptr)
	{
		return ;
	}

	if (_slider_bar != nullptr)
	{
		_slider_bar->removeFromParent();
		_slider_bar = nullptr;
	}

	_slider_bar = slider_ball;
	this->addChild(_slider_bar, 1);
	_slider_bar->setAnchorPoint(Vec2(0.5f, 0.5f));
	_slider_bar->setPosition(Vec2(this->getContentSize().width / 2, this->getContentSize().height / 2));
	
	this->updateBar();
}

void NSliderBar::setSliderBar(const string& normal_image, const string& selected_image, const string& disabled_image, const string& click_effect)
{
	NButton* slider_bar = NButton::create(normal_image, selected_image, disabled_image, click_effect);
	this->setSliderBar(slider_bar);
}

void NSliderBar::interceptTouch(const Touch_Period& touch_period, Touch* touch, NWidget* widget)
{
	switch (touch_period)
	{
	case Touch_Period::kTouchBegan:
		{
			this->handlePressLogic(touch);
		}
		break;
	case Touch_Period::kTouchMoved:
		{
			this->handleMoveLogic(touch);
		}
		break;
	case Touch_Period::kTouchEnded:
	case Touch_Period::kTouchCancelled:
		{
			this->handleReleaseLogic(touch);
		}
		break;
	default:
		break;
	}
}

void NSliderBar::handlePressLogic(Touch *touch)
{
	this->activate(Bar_Period::kBar_Began);
}

void NSliderBar::handleMoveLogic(Touch *touch)
{
	Vec2 point = this->convertToNodeSpace(touch->getLocation());

	switch (this->getDirection())
	{
	case Bar_Direction::kBar_Dir_LeftToRight:
		{
			if (point.x < 0 || point.x > this->getContentSize().width)
			{
				return ;
			}

			if (this->getProgressBar() != nullptr)
			{
				this->setPercentage(point.x / _progress_bar_size.width);
			}
			else
			{
				this->setPercentage(point.x / this->getContentSize().width);
			}
		}
		break;
	case Bar_Direction::kBar_Dir_RightToLeft:
		{
			if (point.x < 0 || point.x > this->getContentSize().width)
			{
				return ;
			}

			if (this->getProgressBar() != nullptr)
			{
				this->setPercentage((_progress_bar_size.width - point.x) / (_progress_bar_size.width));
			}
			else
			{
				this->setPercentage((this->getContentSize().width - point.x) / (this->getContentSize().width));
			}
		}
		break;
	case Bar_Direction::kBar_Dir_TopToButtom:
		{
			if (point.y < 0.0f || point.y > this->getContentSize().height)
			{
				return ;
			}

			if (this->getProgressBar() != nullptr)
			{
				this->setPercentage((_progress_bar_size.height - point.y) / _progress_bar_size.height);
			}
			else
			{
				this->setPercentage((this->getContentSize().height - point.y) / this->getContentSize().height);
			}			
		}
		break;
	case Bar_Direction::kBar_Dir_ButtomToTop:
		{
			if (point.y < 0.0f || point.y > this->getContentSize().height)
			{
				return ;
			}

			if (this->getProgressBar() != nullptr)
			{
				this->setPercentage(point.y / _progress_bar_size.height);
			}
			else
			{
				this->setPercentage(point.y / this->getContentSize().height);
			}
		}
		break;
	default:
		break;
	}
}

void NSliderBar::handleReleaseLogic(Touch *touch)
{
	this->activate(Bar_Period::kBar_End);
}

void NSliderBar::updateBar()
{
	NProgressBar::updateBar();

	if (_slider_bar == nullptr)
	{
		return ; 
	}

	switch (this->getDirection())
	{
	case Bar_Direction::kBar_Dir_LeftToRight:
		{
			if (this->getProgressBar() != nullptr)
			{
				_slider_bar->setPosition(Vec2(_progress_bar_size.width * this->getPercentage(), _progress_bar_size.height / 2));
			}
			else
			{
				_slider_bar->setPosition(Vec2(this->getContentSize().width * this->getPercentage(), this->getContentSize().height / 2));
			}			
		}
		break;
	case Bar_Direction::kBar_Dir_RightToLeft:
		{
			if (this->getProgressBar() != nullptr)
			{
				_slider_bar->setPosition(Vec2(_progress_bar_size.width * (1 - this->getPercentage()), _progress_bar_size.height / 2));
			}
			else
			{
				_slider_bar->setPosition(Vec2(this->getContentSize().width * (1 - this->getPercentage()), this->getContentSize().height / 2));
			}
		}
		break;
	case Bar_Direction::kBar_Dir_TopToButtom:
		{
			if (this->getProgressBar() != nullptr)
			{
				_slider_bar->setPosition(Vec2(_progress_bar_size.width / 2, _progress_bar_size.height * (1 - this->getPercentage())));
			}
			else
			{
				_slider_bar->setPosition(Vec2(this->getContentSize().width / 2, this->getContentSize().height * (1 - this->getPercentage())));
			}
		}
		break;
	case Bar_Direction::kBar_Dir_ButtomToTop:
		{
			if (this->getProgressBar() != nullptr)
			{
				_slider_bar->setPosition(Vec2(_progress_bar_size.width / 2, _progress_bar_size.height * this->getPercentage()));
			}
			else
			{
				_slider_bar->setPosition(Vec2(this->getContentSize().width / 2, this->getContentSize().height * this->getPercentage()));
			}
		}
		break;
	default:
		break;
	}
}