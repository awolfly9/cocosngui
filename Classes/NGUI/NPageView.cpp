#include "NPageView.h"

NPageViewCell::NPageViewCell(void)
{
	this->setUniqueID(NPAGE_VIEW_CELL);
}

NPageViewCell::~NPageViewCell(void)
{
}

NPageView::NPageView(void)
	: _page_changed_callback(nullptr)
	, _page_index(0)
	, _is_show_tips_label(true)
	, _tips_label_distance(30)
	, _tips_label_position_percent(0.1f)
	, _tips_label_fore_file_name("")
	, _tips_label_back_file_name("")
	, _tips_label_fore(nullptr)
	, _tips_label(nullptr)
{
	this->setUniqueID(NPAGE_VIEW);
}

NPageView::~NPageView(void)
{
}

NPageView* NPageView::create(const Size&  view_size, const Size&  cell_size, int cell_count, const Load_Callback& load_callback)
{
	NPageView* ret = new NPageView();

	if (ret && ret->init(view_size, cell_size, cell_count, load_callback))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

bool NPageView::init(const Size& view_size, const Size&  cell_size, int cell_count, const Load_Callback& load_cell)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NTableView::init(view_size, cell_size, cell_count, load_cell));

	INIT_DO_WHILE_END
}

void NPageView::setPageChangedCallback(const PageChanged_Callback& page_changed_callback)
{
	_page_changed_callback = page_changed_callback;
}

PageChanged_Callback NPageView::getPageChangedCallback() const
{
	return _page_changed_callback;
}

int NPageView::getPageIndex()
{
	return _page_index;
}

void NPageView::setShowTipsLabel(bool is_show_tips_label)
{
	if (_is_show_tips_label != is_show_tips_label)
	{
		_is_show_tips_label = is_show_tips_label;
		this->updateTipsLabel();
	}
}

bool NPageView::isShowTipsLabel() const
{
	return _is_show_tips_label;
}

void NPageView::setTipsLabelDistance(float distance)
{
	if (_tips_label_distance != distance)
	{
		_tips_label_distance = distance;
		this->updateTipsLabel();
	}	
}

float NPageView::getTipsLabelDistance() const
{
	return _tips_label_distance;
}

void NPageView::setTipsLabelPositionPercent(float percent)
{
	if (percent >= 1)
	{
		percent = 1;
	}
	else if (percent <= 0)
	{
		percent = 0;
	}

	if (_tips_label_distance != percent)
	{
		_tips_label_position_percent = percent;
		this->updateTipsLabel();
	}
}

float NPageView::getTipsLabelPositionPercent() const
{
	return _tips_label_position_percent;
}

void NPageView::setTipsLabelFileName(const string& tips_label_fore_file_name, const string& tips_label_back_file_name)
{
	_tips_label_fore_file_name = tips_label_fore_file_name;
	_tips_label_back_file_name = tips_label_back_file_name;

	this->updateTipsLabel();
}

void NPageView::setDirection(const ScrollView_Direction& scroll_view_direction)
{
	if (scroll_view_direction != ScrollView_Direction::kScrollView_Dir_Both && scroll_view_direction != this->getDirection())
	{
		NTableView::setDirection(scroll_view_direction);

		this->updateTipsLabel();
	}
}

void NPageView::onScrolling()
{
	NTableView::onScrolling();

	Vec2 point = Vec2(0, 0);

	if (this->getDirection() == ScrollView_Direction::kScrollView_Dir_Horizontal)
	{
		point = this->getContainerPosition() - Vec2(this->getContentSize().width / 2, 0);
	}
	else
	{
		point = this->getContainerPosition() + Vec2(0, this->getContentSize().height / 2);
	}

	if (_page_index != this->getOriginIndex(point))
	{
		if (_page_changed_callback != nullptr)
		{
			_page_index = this->getOriginIndex(point);
			_page_changed_callback(this, _page_index);
		}

		if (_tips_label != nullptr && _tips_label_fore != nullptr)
		{
			_tips_label_fore->setPosition(_tips_label_position.at(this->getOriginIndex(point)));
		}
	}
}

void NPageView::loadCellWithIndex(int label)
{
	NPageViewCell* cell = (NPageViewCell*)_load_callback(this, label);

	if (cell == nullptr)
	{
		return ;
	}

	this->addChild(cell);
	cell->setPosition(_position.at(label));
	cell->setIndex(label);

	_indexs.insert(label);

	if (_cells->empty())
	{
		_cells->push_back(cell);
	}
	else
	{
		bool is_insert = false;
		for (list<NTableViewCell*>::iterator iter = _cells->begin(); iter != _cells->end(); ++ iter)
		{
			NTableViewCell* pCell = (NTableViewCell*)(*iter);

			if (pCell == nullptr)
			{
				continue;
			}

			if (pCell->getIndex() > label)
			{
				_cells->insert(iter, cell);
				is_insert = true;
				break;
			}
		}

		if (! is_insert)
		{
			_cells->push_back(cell);
		}
	}
}

void NPageView::updateTipsLabel()
{
	if (_tips_label_fore_file_name == "" || _tips_label_back_file_name == "")
	{
		return ;
	}

	_tips_label_position.clear();

	if (_tips_label != nullptr)
	{
		_tips_label->removeAllChildren();
	}
	else
	{
		_tips_label = Node::create();
		_tips_label->setAnchorPoint(Vec2(0.5f, 0.5f));
		this->addChild(_tips_label, 10, false);
	}

	_tips_label_fore = Sprite::create(_tips_label_fore_file_name.c_str());

	Sprite* sp_back = Sprite::create(_tips_label_back_file_name.c_str());

	Size size = Size(0, 0);
	
	switch (this->getDirection())
	{
	case ScrollView_Direction::kScrollView_Dir_Horizontal:
		{
			size = Size(_tips_label_distance * (this->getCellCount() - 1) + sp_back->getContentSize().width * this->getCellCount(), _tips_label_fore->getContentSize().height);
			_tips_label->setContentSize(size);
			_tips_label->setPosition(Vec2(this->getContentSize().width / 2, (this->getContentSize().height - _tips_label->getContentSize().height) * _tips_label_position_percent));
			
			for (int i = 0; i < _cell_count; ++ i)
			{
				Sprite* sp_back = Sprite::create(_tips_label_back_file_name.c_str());
				sp_back->setPosition(Vec2(i * sp_back->getContentSize().width + sp_back->getContentSize().width / 2 + _tips_label_distance * i, _tips_label->getContentSize().height / 2));
				_tips_label->addChild(sp_back);

				_tips_label_position.push_back(Vec2(i * sp_back->getContentSize().width + sp_back->getContentSize().width / 2 + _tips_label_distance * i, _tips_label->getContentSize().height / 2));
			}
		}
		break;
	case ScrollView_Direction::kScrollView_Dir_Vertical:
		{
			size = Size(_tips_label_fore->getContentSize().width, _tips_label_distance * (this->getCellCount() - 1) + sp_back->getContentSize().height * this->getCellCount());
			_tips_label->setContentSize(size);
			_tips_label->setPosition(Vec2((this->getContentSize().width - _tips_label->getContentSize().width) * (1 - _tips_label_position_percent), this->getContentSize().height / 2));
		
			for (int i = 0; i < _cell_count; ++ i)
			{
				Sprite* sp_back = Sprite::create(_tips_label_back_file_name.c_str());
				sp_back->setPosition(Vec2(_tips_label->getContentSize().width / 2, _tips_label->getContentSize().height - (i * sp_back->getContentSize().height + sp_back->getContentSize().height / 2 + _tips_label_distance * i)));
				_tips_label->addChild(sp_back);

				_tips_label_position.push_back(Vec2(_tips_label->getContentSize().width / 2, _tips_label->getContentSize().height - (i * sp_back->getContentSize().height + sp_back->getContentSize().height / 2 + _tips_label_distance * i)));
			}
		}
		break;
	default:
		break;
	}

	_tips_label->addChild(_tips_label_fore);
	_tips_label_fore->setPosition(_tips_label_position.at(this->getPageIndex()));
}