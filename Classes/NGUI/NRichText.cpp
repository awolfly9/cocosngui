#include "NRichText.h"

NRichText::NRichText(void)
	: _max_line_length(0)
	, _cursor(0)
	, _vertical_space(0)
{
	_rich_text_lines = new vector<Rich_text_line*>();
}

NRichText::~NRichText(void)
{
	this->removeAllElement();

	delete _rich_text_lines;
}

bool NRichText::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! Node::init());

	this->setAnchorPoint(Vec2(0.5f, 0.5f));

	INIT_DO_WHILE_END
}

void NRichText::setMaxLineLength(int max_line_length)
{
	if (max_line_length >= 2)
	{
		_max_line_length = max_line_length;
	}
}

int NRichText::getMaxLineLength() const
{
	return _max_line_length;
}

void NRichText::setVerticalSpace(float vertical_space)
{
	if (vertical_space > 0 && _vertical_space != vertical_space)
	{
		_vertical_space = vertical_space;
	}
}

float NRichText::getVerticalSpace() const
{
	return _vertical_space;
}

void NRichText::insertElement(const string str, const string font_name, float font_size, const Color3B& color, bool is_next_line)
{
	if (str == "")
	{
		return ;
	}

	if (is_next_line)
	{
		this->pushLine();
		_cursor = 0;
	}

	if (_max_line_length != 0)
	{
		if (_cursor == _max_line_length)
		{
			this->pushLine();
			_cursor = 0;
		}

		string buffer = "";
		char* pstr = const_cast<char*>(str.c_str());

		while(*pstr != 0)
		{
			int char_length = 1;
			int length = str_utf8_char_len(*pstr);

			if (length > 1)
			{
				char_length = 2;
			}

			if (_cursor + char_length > _max_line_length)
			{
				LabelTTF* lbl_label = LabelTTF::create(buffer.c_str(), font_name.c_str(),font_size);
				lbl_label->setColor(color);

				this->pushElement(lbl_label);
				this->pushLine();

				buffer.clear();
				_cursor = 0;
			}

			buffer.append(pstr, length);

			_cursor += char_length;
			pstr +=length;
		}

		if (buffer != "")
		{
			LabelTTF* lbl_label = LabelTTF::create(buffer.c_str(), font_name.c_str(),font_size);
			lbl_label->setColor(color);

			this->pushElement(lbl_label);
		}
	}
	else
	{
		LabelTTF* lbl_label = LabelTTF::create(str.c_str(), font_name.c_str(),font_size);
		lbl_label->setColor(color);

		this->pushElement(lbl_label);
	}
}

void NRichText::insertElement(Node* node, int length, bool is_next_line)
{
	if (is_next_line)
	{
		this->pushLine();
		this->pushElement(node);
		_cursor = length;

		return ;
	}

	if(_max_line_length != 0)
	{
		if (_cursor + length >= _max_line_length)
		{
			this->pushLine();
			_cursor = 0;
		}

		this->pushElement(node);
		_cursor += length;
	}
	else
	{
		this->pushElement(node);
		_cursor += length;
	}
}

void NRichText::removeAllElement()
{
	for (vector<Rich_text_line*>::iterator iter = _rich_text_lines->begin(); iter != _rich_text_lines->end(); iter ++)
	{
		(*iter)->clear();
		delete (*iter);
	}

	_rich_text_lines->clear();
	_cursor = 0;
}

void NRichText::reloadData()
{
	this->removeAllChildren();

	float total_width = 0.0f;
	float total_height = 0.0f;

	for (vector<Rich_text_line*>::iterator iter = _rich_text_lines->begin(); iter != _rich_text_lines->end(); iter ++)
	{
		float width = 0.0f;
		float height = 0.0f;

		for (vector<Node*>::iterator it = (*iter)->begin(); it != (*iter)->end(); it ++)
		{
			Node* node = (*it);

			if (node == nullptr)
			{
				continue;
			}

			width += node->getContentSize().width;

			if (node->getContentSize().height > height)
			{
				height = node->getContentSize().height;
			}			
		}

		if (width > total_width)
		{
			total_width = width;
		}

		total_height += height + _vertical_space;
	}

	this->setContentSize(Size(total_width, total_height));

	float height = 0.0f;
	for (vector<Rich_text_line*>::iterator iter = _rich_text_lines->begin(); iter != _rich_text_lines->end(); iter ++)
	{
		float width = 0.0f;
		float max_height = 0.0f;

		Node* last_node = nullptr;

		for (vector<Node*>::iterator it = (*iter)->begin(); it != (*iter)->end(); it ++)
		{
			Node* node = (*it);

			if (node == nullptr)
			{
				continue;
			}

			node->setPosition(Vec2(width + node->getContentSize().width * node->getAnchorPoint().x, this->getContentSize().height - height - node->getContentSize().height * node->getAnchorPoint().y));
			this->addChild(node);
			width += node->getContentSize().width;

			if (node->getContentSize().height > max_height)
			{
				max_height = node->getContentSize().height;
			}

			last_node = node;
		}

		height += max_height + _vertical_space;
	}

	this->removeAllElement();
}

void NRichText::pushElement(Node* node)
{
	if (node == nullptr)
	{
		return ;
	}

	if (_rich_text_lines->empty())
	{
		_rich_text_lines->push_back(new Rich_text_line());
	}

	_rich_text_lines->back()->push_back(node);
}

void NRichText::pushLine()
{
	_rich_text_lines->push_back(new Rich_text_line());
}