#ifndef __NSWITCH_BUTTON__
#define __NSWITCH_BUTTON__

#include "NGUI/NProtocol.h"
#include "NGUI/NWidget.h"

class NSwitchButton : public NWidget
{
public:
	NSwitchButton(void);
	~NSwitchButton(void);

	static NSwitchButton* create(const string& normal_image, const Size& show_size = Size(0, 0), const string& click_effect = "");
	virtual bool init(const string& file_path, const Size& show_size, const string& click_effect);

	virtual bool onTouchBegan(Touch* touch, Event* event);
	virtual void onTouchMoved(Touch* touch, Event* event);
	virtual void onTouchEnded(Touch* touch, Event* event);
	virtual void onTouchCancelled(Touch* touch, Event* event);

	void setSwitchTime(float switch_time);
	float getSwitchTime() const;

	void setShowSize(const Size& show_size);
	Size getShowSize() const;

	void setSwitchState(const Switch_State& switch_state);
	Switch_State getSwitchState() const;

protected:
	void switchState();
	virtual bool hitWidget(const Vec2& point);
	void callfunc();

	virtual void visit(Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags);
	virtual void beforeVisit();
	virtual void afterVisit();

private:
	Node* _image;
	bool _is_switching;
	Switch_State _switch_state;
	Size _show_size;
	float _switch_time;

	CustomCommand _before_visit;
	CustomCommand _after_visit;
};

#endif // __NSWITCH_BUTTON__
