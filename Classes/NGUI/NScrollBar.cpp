#include "NScrollBar.h"

NScrollBar::NScrollBar(void)
	: _btn_scroll_bar(nullptr)
	, _delta_position(Vec2(0, 0))
	, _is_scrolling(false)
{
	this->setUniqueID(NSCROLL_BAR);
}

NScrollBar::~NScrollBar(void)
{
}

NScrollBar* NScrollBar::create()
{
	return create("", nullptr);
}

NScrollBar* NScrollBar::create(const string& background, NButton* btn_scroll_bar)
{
	NScrollBar* ret = new NScrollBar();

	if (ret && ret->init(background, btn_scroll_bar))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

NScrollBar* NScrollBar::create(const string& background, const string& normal_image, const string& selected_image, const string& disabled_image, const string& click_effect)
{
	NScrollBar* ret = new NScrollBar();

	if (ret && ret->init(background, normal_image, selected_image, disabled_image, click_effect))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

bool NScrollBar::init(const string& background, NButton* btn_scroll_bar)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NBar::init(background));

	this->setScrollBar(btn_scroll_bar);
	this->setTouchEnabled(false);

	INIT_DO_WHILE_END
}

bool NScrollBar::init(const string& background, const string& normal_image, const string& selected_image, const string& disabled_image, const string& click_effect)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NBar::init(background));

	NButton* btn = NButton::create(normal_image, selected_image, disabled_image, click_effect);
	this->setScrollBar(btn);
	this->setTouchEnabled(false);

	INIT_DO_WHILE_END
}

void NScrollBar::setScrollBar(NButton* btn_scroll_bar)
{
	if (btn_scroll_bar == nullptr)
	{
		return ;
	}

	if (_btn_scroll_bar != nullptr)
	{
		_btn_scroll_bar->removeFromParent();
		_btn_scroll_bar = nullptr;
	}
	else
	{
		_btn_scroll_bar = btn_scroll_bar;
		_btn_scroll_bar->setAnchorPoint(Vec2(0.5f, 0.5f));
		this->addChild(_btn_scroll_bar);
		this->updateBar();
	}
}

void NScrollBar::setScrollBar(const string& normal_image, const string& selected_image, const string& disabled_image, const string& click_effect)
{
	NButton* btn = NButton::create(normal_image, selected_image, disabled_image, click_effect);
	this->setScrollBar(btn);
}

void NScrollBar::setScrolling(bool is_scrolling)
{
	_is_scrolling = is_scrolling;
}

bool NScrollBar::isScrolling() const
{
	return _is_scrolling;
}

void NScrollBar::interceptTouch(const Touch_Period& touch_period, Touch* touch, NWidget* widget)
{
	switch (touch_period)
	{
	case Touch_Period::kTouchBegan:
		{
			this->handlePressLogic(touch);
		}
		break;
	case Touch_Period::kTouchMoved:
		{
			this->handleMoveLogic(touch);
		}
		break;
		case Touch_Period::kTouchEnded:
		case Touch_Period::kTouchCancelled:
			{
				this->handleReleaseLogic(touch);
			}
			break;
	default:
		break;
	}
}

void NScrollBar::handlePressLogic(Touch *touch) 
{
	Vec2 point = this->convertToNodeSpace(touch->getLocation());
	_delta_position = point - _btn_scroll_bar->getPosition();

	this->setScrolling(true);
	this->activate(Bar_Period::kBar_Began);
}

void NScrollBar::handleMoveLogic(Touch *touch)
{
	Vec2 point = this->convertToNodeSpace(touch->getLocation());
	Vec2 position = point - _delta_position;

	switch (this->getDirection())
	{
	case Bar_Direction::kBar_Dir_LeftToRight:
		{
			if (position.x <= 0.0f || position.x >= this->getContentSize().width)
			{
				return ;
			}

			float percentage = (position.x - _btn_scroll_bar->getContentSize().width / 2) / (this->getContentSize().width - _btn_scroll_bar->getContentSize().width);
			this->setPercentage(percentage);
		}
		break;
	case Bar_Direction::kBar_Dir_RightToLeft:
		{
			if (position.x <= 0.0f || position.x >= this->getContentSize().width)
			{
				return ;
			}

			float percentage = (this->getContentSize().width - position.x - _btn_scroll_bar->getContentSize().width / 2) / (this->getContentSize().width - _btn_scroll_bar->getContentSize().width);
			this->setPercentage(percentage);
		}
		break;
	case Bar_Direction::kBar_Dir_TopToButtom:
		{
			if (position.y <= 0.0f || position.y >= this->getContentSize().height)
			{
				return ;
			}

			float percentage = (this->getContentSize().height - position.y - _btn_scroll_bar->getContentSize().height / 2) / (this->getContentSize().height - _btn_scroll_bar->getContentSize().height);
			this->setPercentage(percentage);
		}
		break;
	case Bar_Direction::kBar_Dir_ButtomToTop:
		{
			if (position.y <= 0.0f || position.y >= this->getContentSize().height)
			{
				return ;
			}

			float percentage = (position.y - _btn_scroll_bar->getContentSize().height / 2) / (this->getContentSize().height - _btn_scroll_bar->getContentSize().height);
			this->setPercentage(percentage);
		}
		break;
	default:
		break;
	}
}

void NScrollBar::handleReleaseLogic(Touch *touch)
{
	this->activate(Bar_Period::kBar_End);
	this->setScrolling(false);
	_delta_position = Vec2(0, 0);
}

void NScrollBar::updateBar()
{
	switch (this->getDirection())
	{
	case Bar_Direction::kBar_Dir_LeftToRight:
		{
			_btn_scroll_bar->setPosition(Vec2(_btn_scroll_bar->getContentSize().width / 2 + (this->getContentSize().width - _btn_scroll_bar->getContentSize().width) * this->getPercentage(), this->getContentSize().height / 2));
		}
		break;
	case Bar_Direction::kBar_Dir_RightToLeft:
		{
			_btn_scroll_bar->setPosition(Vec2((this->getContentSize().width - _btn_scroll_bar->getContentSize().width / 2) - (this->getContentSize().width - _btn_scroll_bar->getContentSize().width) * this->getPercentage(), this->getContentSize().height / 2));
		}
		break;
	case Bar_Direction::kBar_Dir_TopToButtom:
		{
			_btn_scroll_bar->setPosition(Vec2(this->getContentSize().width / 2, (this->getContentSize().height - _btn_scroll_bar->getContentSize().height / 2) - (this->getContentSize().height - _btn_scroll_bar->getContentSize().height) * this->getPercentage()));
		}
		break;
	case Bar_Direction::kBar_Dir_ButtomToTop:
		{
			_btn_scroll_bar->setPosition(Vec2(this->getContentSize().width / 2, _btn_scroll_bar->getContentSize().height / 2 + (this->getContentSize().height - _btn_scroll_bar->getContentSize().height) * this->getPercentage()));
		}
		break;
	default:
		break;
	}
}
