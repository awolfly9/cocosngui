#include "NJoystick.h"

#define  HANDLE_RESET_SPEED 200

NJoystick::NJoystick(void)
	: _background(nullptr)
	, _handle(nullptr)
	, _is_auto_reset(true)
	, _center(Vec2(0, 0))
{
	this->setUniqueID(NJOYSTICK);
}

NJoystick::~NJoystick(void)
{
}

NJoystick* NJoystick::create(const string& bg, const string& handle, float radius)
{
	NJoystick* ret = new NJoystick();

	if (ret && ret->init(bg, handle, radius))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

bool NJoystick::init(const string& bg, const string& handle, float radius)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NWidget::init());
	
	_background = Sprite::create(bg);
	this->setContentSize(_background->getContentSize());
	this->addChild(_background);
	_background->setPosition(POS_BY_NODE(this, 0.5f, 0.5f));

	_center = Vec2(this->getContentSize().width / 2, this->getContentSize().height / 2);

	_handle = Sprite::create(handle);
	this->addChild(_handle);
	_handle->setPosition(POS_BY_NODE(this, 0.5f, 0.5f));

	this->setRadius(radius);

	INIT_DO_WHILE_END
}

void NJoystick::setRadius(float radius)
{
	this->_radius = radius;
}

float NJoystick::getRadius() const
{
	return _radius;
}

void NJoystick::setAutoReset(bool is_auto_reset)
{
	this->_is_auto_reset = is_auto_reset;
}

bool NJoystick::isAutoReset() const
{
	return this->_is_auto_reset;
}

const Vec2& NJoystick::getDir() const
{
	Vec2 dis = _handle->getPosition() - _center;
	Vec2 dir = dis.getNormalized();
	return dir;
}

bool NJoystick::onTouchBegan(Touch* touch, Event* event)
{
	bool ret = NWidget::onTouchBegan(touch, event);

	if (ret)
	{
		Vec2 pos = this->convertToNodeSpace(touch->getLocation());
	
		if (_center.distance(pos) > _radius)
		{
		}
		else
		{
			_handle->setPosition(pos);
		}

		this->scheduleUpdate();
		return true;
	}

	return false;
}

void NJoystick::onTouchMoved(Touch* touch, Event* event)
{
	Vec2 pos = this->convertToNodeSpace(touch->getLocation());

	if (_center.distance(pos) >= _radius)
	{
		Vec2 dir = pos - _center;
		dir = dir.getNormalized();
		Vec2 newPos = Vec2(_radius * dir.x, _radius * dir.y);
		_handle->setPosition(newPos + _center);
	}
	else
	{
		_handle->setPosition(pos);
	}	
}

void NJoystick::onTouchEnded(Touch* touch, Event* event)
{
	this->unscheduleUpdate();
	this->activate(Touch_Period::kTouchEnded);
	Vec2 pos = this->convertToNodeSpace(touch->getLocation());
	
	if (_center.distance(pos) >= _radius)
	{
		Vec2 dir = pos - _center;
		dir = dir.getNormalized();

		Vec2 newPos = Vec2(_radius * dir.x, _radius * dir.y);
		_handle->setPosition(newPos + _center);
	}
	else
	{
		_handle->setPosition(pos);
	}	

	if (_is_auto_reset)
	{
		float dis = _center.distance(pos);
		
		MoveTo* move  = MoveTo::create(dis / HANDLE_RESET_SPEED, _center);
		_handle->runAction(move);
	}
}

void NJoystick::onTouchCancelled(Touch* touch, Event* event)
{
	this->unscheduleUpdate();
	this->activate(Touch_Period::kTouchCancelled);

	Vec2 pos = this->convertToNodeSpace(touch->getLocation());

	if (_center.distance(pos) >= _radius)
	{
		Vec2 dir = pos - _center;
		dir = dir.getNormalized();

		Vec2 newPos = Vec2(_radius * dir.x, _radius * dir.y);
		_handle->setPosition(newPos + _center);
	}
	else
	{
		_handle->setPosition(pos);
	}

	if (_is_auto_reset)
	{
		float dis = _center.distance(pos);

		MoveTo* move  = MoveTo::create(dis / HANDLE_RESET_SPEED, _center);
		_handle->runAction(move);
	}
}

bool NJoystick::hitWidget(const Vec2& point)
{
	if (NWidget::hitWidget(point))
	{
		Vec2 pt = this->convertToNodeSpace(point);
		Vec2 pos = _handle->getPosition();
		Size size = _handle->getContentSize();
		Vec2 anchor = _handle->getAnchorPoint();
		Rect rect = Rect(pos.x - size.width * anchor.x, pos.y - size.height * anchor.y, _handle->getContentSize().width, _handle->getContentSize().height);

		if (rect.containsPoint(pt))
		{
			return true;
		}
	}

	return false;
}

void NJoystick::update(float dt)
{
	this->activate(Touch_Period::kTouchMoved);
}