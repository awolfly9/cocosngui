#include "NProgressBar.h"

NProgressBar::NProgressBar(void)
	: _progress_bar(nullptr)
{
	this->setUniqueID(NPROGRESS_BAR);
}

NProgressBar::~NProgressBar(void)
{
}

NProgressBar* NProgressBar::create()
{
	return create("", "");
}

NProgressBar* NProgressBar::create(const string& background, const string& progress_bar)
{
	NProgressBar* ret = new NProgressBar();

	if (ret && ret->init(background, progress_bar))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

bool NProgressBar::init(const string& background, const string& progress_bar)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NBar::init(background));

	this->setProgressBar(progress_bar);
	this->setTouchEnabled(false);

	INIT_DO_WHILE_END
}

void NProgressBar::setProgressBar(const string& progress_bar)
{
	if (progress_bar == "")
	{
		return ;
	}

	this->setProgressBar(Sprite::create(progress_bar.c_str()));
}

void NProgressBar::setProgressBar(Sprite* progress_bar)
{
	if (progress_bar == nullptr)
	{
		return ;
	}

	if (_progress_bar != nullptr)
	{
		this->removeChild(_progress_bar);
		_progress_bar = nullptr;
	}
	else
	{
		_progress_bar = progress_bar;
		_progress_bar_size = _progress_bar->getContentSize();
		this->addChild(_progress_bar);

		this->updateBar();
	}
}

Sprite* NProgressBar::getProgressBar() const
{
	return _progress_bar;
}

void NProgressBar::updateBar()
{
	if (_progress_bar == nullptr)
	{
		return ;  
	}

	switch (this->getDirection())
	{
	case Bar_Direction::kBar_Dir_LeftToRight:
		{
			_progress_bar->setAnchorPoint(Vec2(0.0f, 0.5f));
			_progress_bar->setPosition(Vec2(this->getContentSize().width / 2 - _progress_bar_size.width / 2, this->getContentSize().height / 2));
			_progress_bar->setTextureRect(Rect(0, 0, _progress_bar_size.width * this->getPercentage(), _progress_bar_size.height));
		}
		break;
	case Bar_Direction::kBar_Dir_RightToLeft:
		{
			_progress_bar->setAnchorPoint(Vec2(1.0f, 0.5f));
			_progress_bar->setPosition(Vec2(this->getContentSize().width / 2 + _progress_bar_size.width / 2, this->getContentSize().height / 2));
			_progress_bar->setTextureRect(Rect(_progress_bar_size.width * (1 - this->getPercentage()), 0, _progress_bar_size.width * this->getPercentage(), _progress_bar_size.height));
		}
		break;
	case Bar_Direction::kBar_Dir_TopToButtom:
		{
			_progress_bar->setAnchorPoint(Vec2(0.5f, 1.0f));
			_progress_bar->setPosition(Vec2(this->getContentSize().width / 2, this->getContentSize().height / 2 + _progress_bar_size.height / 2));
			_progress_bar->setTextureRect(Rect(0, 0, _progress_bar_size.width, _progress_bar_size.height * this->getPercentage()));
		}
		break;
	case Bar_Direction::kBar_Dir_ButtomToTop:
		{
			_progress_bar->setAnchorPoint(Vec2(0.5f, 0.0f));
			_progress_bar->setPosition(Vec2(this->getContentSize().width / 2, this->getContentSize().height / 2 - _progress_bar_size.height / 2));
			_progress_bar->setTextureRect(Rect(0, _progress_bar_size.height * ( 1 - this->getPercentage()), _progress_bar_size.width, _progress_bar_size.height * this->getPercentage()));
		}
		break;
	default:
		break;
	}
}