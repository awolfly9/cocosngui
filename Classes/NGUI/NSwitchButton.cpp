#include "NSwitchButton.h"

#define MOVE_SPEED 200

NSwitchButton::NSwitchButton(void)
	: _switch_state(Switch_State::kSwitch_On)
	, _is_switching(false)
	, _switch_time(0.2)
{
	this->setUniqueID(NSWITCH_BUTTON);
}

NSwitchButton::~NSwitchButton(void)
{
}

NSwitchButton* NSwitchButton::create(const string& normal_image, const Size& show_size, const string& click_effect)
{
	NSwitchButton* ret = new NSwitchButton();

	if (ret && ret->init(normal_image, show_size, click_effect))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

bool NSwitchButton::init(const string& normal_image, const Size& show_size, const string& click_effect)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NWidget::init());

	_image = Sprite::create(normal_image);
	this->setContentSize(_image->getContentSize());
	_image->setAnchorPoint(Vec2(0, 0));
	_image->setPosition(0, 0);
	this->addChild(_image);

	this->setClickEffect(click_effect);

	if (show_size.equals(Size(0, 0)))
	{
		Size size = Size(this->getContentSize().width / 2, this->getContentSize().height);
		this->setShowSize(size);
	}
	else
	{
		this->setShowSize(show_size);
	}

	INIT_DO_WHILE_END
}

bool NSwitchButton::onTouchBegan(Touch* touch, Event* event)
{
	bool ret = NWidget::onTouchBegan(touch, event);
	return ret;
}

void NSwitchButton::onTouchMoved(Touch* touch, Event* event)
{
	NWidget::onTouchMoved(touch, event);
}

void NSwitchButton::onTouchEnded(Touch* touch, Event* event)
{
	Vec2 point = touch->getLocation();

	if (this->hitWidget(point))
	{
		if (this->isInterruptTouch())
		{
			this->activate(Touch_Period::kTouchCancelled);
		}
		else
		{
			if (this->getSwitchState() == Switch_State::kSwitch_On)
			{
				this->_switch_state = Switch_State::kSwitch_Off;
			}
			else
			{
				this->_switch_state = Switch_State::kSwitch_On;
			}

			this->activate(Touch_Period::kTouchEnded);
			this->switchState();
		}
	}

	this->setWidgetState(Widget_State::kWidgetStateNormal);
}

void NSwitchButton::onTouchCancelled(Touch* touch, Event* event)
{
	Vec2 point = touch->getLocation();

	if (this->hitWidget(point))
	{
		if (this->isInterruptTouch())
		{
			this->activate(Touch_Period::kTouchCancelled);
		}
		else
		{
			this->activate(Touch_Period::kTouchEnded);
			this->switchState();
		}
	}

	this->setWidgetState(Widget_State::kWidgetStateNormal);
}

void NSwitchButton::setSwitchTime(float switch_time)
{
	_switch_time = switch_time;
}

float NSwitchButton::getSwitchTime() const
{
	return _switch_time;
}

void NSwitchButton::setShowSize(const Size& show_size)
{
	this->_show_size = show_size;
}

Size NSwitchButton::getShowSize() const
{
	return _show_size;
}

void NSwitchButton::setSwitchState(const Switch_State& switch_state)
{
	if (this->_switch_state != switch_state)
	{
		this->_switch_state = switch_state;
		this->switchState();
	}
}

Switch_State NSwitchButton::getSwitchState() const
{
	return this->_switch_state;
}

void NSwitchButton::switchState()
{
	if (_is_switching == true)
	{
		return ;
	}

	if (this->getSwitchState() == Switch_State::kSwitch_On)
	{
		float length = this->getContentSize().width - this->_show_size.width;
		MoveTo* move = MoveTo::create(length / MOVE_SPEED, Vec2(0, 0));
		CallFunc* cf = CallFunc::create(CALLBACK_0(NSwitchButton::callfunc, this));
		Sequence* seq = Sequence::create(move, cf, nullptr);

		_image->runAction(seq);
		_is_switching = true;
	}
	else
	{
		float length = this->getContentSize().width - this->_show_size.width;
		MoveBy* move = MoveBy::create(length / MOVE_SPEED, Vec2(-length, 0));
		CallFunc* cf = CallFunc::create(CALLBACK_0(NSwitchButton::callfunc, this));
		Sequence* seq = Sequence::create(move, cf, nullptr);

		_image->runAction(seq);
		_is_switching = true;
	}
}

bool NSwitchButton::hitWidget(const Vec2& point)
{
	bool ret = NWidget::hitWidget(point);

	if (ret)
	{
		Vec2 pt = this->convertToNodeSpace(point);

		Rect rect = Rect(0, 0, this->_show_size.width, this->_show_size.height);

		if (rect.containsPoint(pt))
		{
			return true;
		}

		return false;
	}

	return false;
}

void NSwitchButton::callfunc()
{
	_is_switching = false;
}

void NSwitchButton::visit(Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags)
{
	_before_visit.init(_globalZOrder);
	_before_visit.func = CALLBACK_0(NSwitchButton::beforeVisit, this);
	renderer->addCommand(&_before_visit);

	NWidget::visit(renderer, parentTransform, parentFlags);

	_after_visit.init(_globalZOrder);
	_after_visit.func = CALLBACK_0(NSwitchButton::afterVisit, this);
	renderer->addCommand(&_after_visit);
}

void NSwitchButton::beforeVisit()
{
	Rect rect = Rect(0, 0, this->_show_size.width, this->_show_size.height);
	rect = RectApplyAffineTransform(rect, nodeToWorldTransform());

	glEnable(GL_SCISSOR_TEST);
	auto glview = Director::getInstance()->getOpenGLView();
	glview->setScissorInPoints(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
}

void NSwitchButton::afterVisit()
{
	glDisable(GL_SCISSOR_TEST);
}