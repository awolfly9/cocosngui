//#include "NLabel.h"
//
//NLabel::NLabel(void)
//	: _normal_color(Color3B(255, 255, 255))
//	, _selected_color(Color3B(255, 255, 0))
//	, _disabled_color(Color3B(105, 105, 105))
//{
//	this->setUniqueID(NLABEL);
//}
//
//NLabel::~NLabel(void)
//{
//}
//
//NLabel* NLabel::create()
//{
//	NLabel* ret = new NLabel();
//
//	if (ret)
//	{
//		ret->autorelease();
//	}
//	else
//	{
//		SAFE_DELETE_NULL(ret);
//	}
//
//	delete ret;
//}
//
//NLabel* NLabel::create(const std::string& text, const std::string& font, float fontSize, const Size& dimensions, TextHAlignment hAlignment, TextVAlignment vAlignment)
//{
//	if (FileUtils::getInstance()->isFileExist(font))
//	{
//		return create(text,font,fontSize,dimensions,hAlignment,vAlignment);
//	} 
//	else
//	{
//		return create(text,font,fontSize,dimensions,hAlignment,vAlignment);
//	}
//}
//
//NLabel* NLabel::create(const std::string& text, const std::string& font, float fontSize, const Size& dimensions /* = Size::ZERO */, TextHAlignment hAlignment /* = TextHAlignment::LEFT */, TextVAlignment vAlignment /* = TextVAlignment::TOP */)
//{
//	NLabel* ret = new NLabel(nullptr,hAlignment,vAlignment);
//
//	if (ret)
//	{
//		ret->setSystemFontName(font);
//		ret->setSystemFontSize(fontSize);
//		ret->setDimensions(dimensions.width, dimensions.height);
//		ret->setString(text);
//
//		ret->autorelease();
//	}
//	else
//	{
//		SAFE_DELETE_NULL(ret);
//	}
//
//	return ret;
//}
//
//NLabel* NLabel::create(const std::string& text, const std::string& fontFile, float fontSize, const Size& dimensions /* = Size::ZERO */, TextHAlignment hAlignment /* = TextHAlignment::LEFT */, TextVAlignment vAlignment /* = TextVAlignment::TOP */)
//{
//	NLabel* ret = new NLabel(nullptr,hAlignment,vAlignment);
//
//	if (ret && FileUtils::getInstance()->isFileExist(fontFile))
//	{
//		TTFConfig ttfConfig(fontFile.c_str(),fontSize,GlyphCollection::DYNAMIC);
//		if (ret->setTTFConfig(ttfConfig))
//		{
//			ret->setDimensions(dimensions.width,dimensions.height);
//			ret->setString(text);
//
//			ret->autorelease();
//		}
//	}
//	else
//	{
//		SAFE_DELETE_NULL(ret);
//	}
//
//	return ret;
//}
//
//NLabel* NLabel::create(const TTFConfig& ttfConfig, const std::string& text, TextHAlignment alignment /* = TextHAlignment::CENTER */, int maxLineWidth /* = 0 */)
//{
//	NLabel* ret = new NLabel(nullptr,alignment);
//
//	if (ret && FileUtils::getInstance()->isFileExist(ttfConfig.fontFilePath) && ret->setTTFConfig(ttfConfig))
//	{
//		ret->setMaxLineWidth(maxLineWidth);
//		ret->setString(text);
//		ret->autorelease();
//	}
//	else
//	{
//		SAFE_DELETE_NULL(ret);
//	}
//
//	return ret;
//}
//
//NLabel* NLabel::create(const std::string& bmfontFilePath, const std::string& text,const TextHAlignment& alignment /* = TextHAlignment::LEFT */, int maxLineWidth /* = 0 */, const Vec2& imageOffset /* = Vec2::ZERO */)
//{
//	NLabel* ret = new NLabel(nullptr,alignment);
//
//	if (ret && ret->setBMFontFilePath(bmfontFilePath,imageOffset))
//	{
//		ret->setMaxLineWidth(maxLineWidth);
//		ret->setString(text);
//		ret->autorelease();
//	}
//	else
//	{
//		SAFE_DELETE_NULL(ret);
//	}
//
//	return ret;
//}
//
//NLabel* NLabel::create(const std::string& plistFile)
//{
//	NLabel* ret = new NLabel();
//
//	if (ret && ret->setCharMap(plistFile))
//	{
//		ret->autorelease();
//	}
//	else
//	{
//		SAFE_DELETE_NULL(ret);
//	}
//
//	return ret;
//}
//
//NLabel* NLabel::create(Texture2D* texture, int itemWidth, int itemHeight, int startCharMap)
//{
//	auto ret = new NLabel();
//
//	if (ret && ret->setCharMap(texture,itemWidth,itemHeight,startCharMap))
//	{
//		ret->create();
//	}
//	else
//	{
//		SAFE_DELETE_NULL(ret);
//	}
//
//	return ret;
//}
//
//NLabel* NLabel::create(const std::string& charMapFile, int itemWidth, int itemHeight, int startCharMap)
//{
//	NLabel* ret = new NLabel();
//
//	if (ret && ret->setCharMap(charMapFile,itemWidth,itemHeight,startCharMap))
//	{
//		ret->autorelease();
//	}
//	else
//	{
//		SAFE_DELETE_NULL(ret);
//	}
//
//	return ret;
//}
//
//void NLabel::setNormalColor(const Color3B& normal_color)
//{
//	_normal_color = normal_color;
//}
//
//void NLabel::setSelectedColor(const Color3B& selected_color)
//{
//	_selected_color = selected_color;
//}
//
//void NLabel::setDisabledColor(const Color3B& disabled_color)
//{
//	_disabled_color = disabled_color;
//}
//
//Color3B NLabel::getNormalColor() const
//{
//	return _normal_color;
//}
//
//Color3B NLabel::getSelectedColor() const
//{
//	return _selected_color;
//}
//
//Color3B NLabel::getDisabledColor() const
//{
//	return _disabled_color;
//}
//
//void NLabel::setCallback(const Touch_Callback& call_back)
//{
//	_touch_call_back = call_back;
//}
//
//void NLabel::setName(const string& name)
//{
//	if (_name != name)
//	{
//		_name = name;
//	}
//}
//
//string NLabel::getName() const
//{
//	return _name;
//}
//
//void NLabel::setTouchEnabled(bool is_touch_enable)
//{
//	if (_is_touch_enabled != is_touch_enable)
//	{
//		_is_touch_enabled = is_touch_enable;
//
//		if (_is_touch_enabled && _touch_listener == nullptr)
//		{
//			_touch_listener = EventListenerTouchOneByOne::create();
//			_touch_listener->setSwallowTouches(true);
//
//			_touch_listener->onTouchBegan = CALLBACK_2(NLabel::onTouchBegan, this);
//			_touch_listener->onTouchMoved = CALLBACK_2(NLabel::onTouchMoved, this);
//			_touch_listener->onTouchEnded = CALLBACK_2(NLabel::onTouchEnded, this);
//			_touch_listener->onTouchCancelled = CALLBACK_2(NLabel::onTouchCancelled, this);
//
//			Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(_touch_listener, this);		
//		}
//		else
//		{
//			if (_touch_listener != nullptr)
//			{
//				Director::getInstance()->getEventDispatcher()->removeEventListener(_touch_listener);
//			}
//		}
//	}	
//}
//
//bool NLabel::isTouchEnabled() const
//{
//	return _is_touch_enabled;
//}
//
//void NLabel::setEnabled(bool is_enabled)
//{
//	if (_is_enabled != is_enabled)
//	{
//		_is_enabled = is_enabled;
//
//		if (_is_enabled)
//		{
//			this->setWidgetState(Widget_State::kWidgetStateNormal);
//		}
//		else
//		{
//			this->setWidgetState(Widget_State::kWidgetStateDisabled);
//		}
//	}
//}
//
//bool NLabel::isEnabled() const
//{
//	return _is_enabled;
//}
//
//void NLabel::setClickEffect(const string& click_effect)
//{
//	if (click_effect != "")
//	{
//		_click_effect = click_effect;
//		_is_play_click_effect = true;
//	}	
//}
//
//string NLabel::getClickEffect() const
//{
//	return _click_effect;
//}
//
//void NLabel::setPlayClickEffect(bool is_play_click_effect)
//{
//	_is_play_click_effect = is_play_click_effect;
//}
//
//bool NLabel::isPlayClickEffect() const
//{
//	return _is_play_click_effect;
//}
//
//void NLabel::setInterruptTouch(bool is_interrupt)
//{
//	if (_is_interrupt_touch != is_interrupt)
//	{
//		_is_interrupt_touch = is_interrupt;
//
//		if (_is_interrupt_touch)
//		{
//			this->setWidgetState(Widget_State::kWidgetStateNormal);
//		}
//	}
//}
//
//bool NLabel::isInterruptTouch() const
//{
//	return this->_is_interrupt_touch;
//}
//
//void NLabel::setWidgetState(const Widget_State& widget_state)
//{
//	if(_widgt_state != widget_state)
//	{
//		_widgt_state = widget_state;
//
//		this->updateWidget(_widgt_state);
//	}
//}
//
//Widget_State NLabel::getWidgetState() const
//{
//	return _widgt_state;
//}
//
//Vec2 NLabel::getBeginTouchVec2() const
//{
//	return _began_touch_point;
//}
//
//string NLabel::getUniqueID() const
//{
//	return _unique_id;
//}
//
//void NLabel::setUniqueID(const string& unique_id)
//{
//	this->_unique_id = unique_id;
//}
//
//bool NLabel::checkNodeVisible(Node* node)
//{
//	if (node == nullptr)
//	{
//		return true;
//	}
//
//	if (node->isVisible() == false)
//	{
//		return false;
//	}
//
//	Node* parent = node->getParent();
//
//	if (parent == nullptr)
//	{
//		return true;
//	}
//
//	return this->checkNodeVisible(parent);
//}
//
//bool NLabel::onTouchBegan(Touch* touch, Event* event)
//{
//	if (! _is_touch_enabled || ! _is_enabled)
//	{
//		return false;
//	}
//
//	_began_touch_point = touch->getLocation();
//
//	if (this->checkNodeVisible(this) && this->hitWidget(_began_touch_point))
//	{
//		this->interceptTouch(Touch_Period::kTouchBegan, touch, this);
//
//		if (! this->isInterruptTouch())
//		{
//			this->activate(Touch_Period::kTouchBegan);
//			this->setWidgetState(Widget_State::kWidgetStateSelected);
//
//			if (this->isPlayClickEffect())
//			{
//				AUDIO_MANAGER->playEffect(_click_effect);
//			}
//		}
//
//		return true;
//	}
//
//	return false;
//}
//
//void NLabel::onTouchMoved(Touch* touch, Event* event)
//{
//	Vec2 point = touch->getLocation();
//	this->interceptTouch(Touch_Period::kTouchMoved, touch, this);
//
//	if (this->hitWidget(point))
//	{
//		if (this->isInterruptTouch())
//		{
//			this->activate(Touch_Period::kTouchCancelled);
//			this->setWidgetState(Widget_State::kWidgetStateNormal);
//		}
//		else
//		{
//			this->activate(Touch_Period::kTouchMoved);
//			this->setWidgetState(Widget_State::kWidgetStateSelected);
//		}
//	}
//	else
//	{
//		this->setWidgetState(Widget_State::kWidgetStateNormal);
//	}
//}
//
//void NLabel::onTouchEnded(Touch* touch, Event* event)
//{
//	Vec2 point = touch->getLocation();
//	this->interceptTouch(Touch_Period::kTouchEnded, touch, this);
//	this->setInterruptTouch(false);
//
//	if (this->hitWidget(point))
//	{
//		if (this->isInterruptTouch())
//		{
//			this->activate(Touch_Period::kTouchCancelled);
//		}
//		else
//		{
//			this->activate(Touch_Period::kTouchEnded);
//		}
//	}
//
//	this->setWidgetState(Widget_State::kWidgetStateNormal);
//}
//
//void NLabel::onTouchCancelled(Touch* touch, Event* event)
//{	
//	this->setInterruptTouch(false);
//	this->activate(Touch_Period::kTouchCancelled);
//	this->setWidgetState(Widget_State::kWidgetStateNormal);
//}
//
//void NLabel::interceptTouch(const Touch_Period& touch_period, Touch* touch, NWidget* widget)
//{
//	Node* parent = this->getParent();
//
//	if (parent != nullptr)
//	{
//		NWidget* widget_parent = dynamic_cast<NWidget*>(parent);
//
//		if (widget_parent != nullptr)
//		{
//			return widget_parent->interceptTouch(touch_period, touch, widget);
//		}
//	}
//}
//
//bool NLabel::hitWidget(const Vec2& point)
//{
//	Vec2 pt = this->convertToNodeSpace(point);
//
//	Rect rect = Rect(0, 0, this->getContentSize().width, this->getContentSize().height);
//
//	if (rect.containsPoint(pt))
//	{
//		return true;
//	}
//
//	return false;
//}
//
//void NLabel::activate(const Touch_Period& touch_period)
//{
//	if (_touch_call_back != nullptr)
//	{
//		_touch_call_back(this, touch_period);
//	}	
//}
//
//void NLabel::updateWidget(const Widget_State& widget_state)
//{
//	switch (widget_state)
//	{
//	case Widget_State::kWidgetStateNormal:
//		{
//			this->setColor(_normal_color);
//		}
//		break;
//	case Widget_State::kWidgetStateSelected:
//		{
//			this->setColor(_selected_color);
//		}
//		break;
//	case Widget_State::kWidgetStateDisabled:
//		{
//			this->setColor(_disabled_color);
//		}
//		break;
//	default:
//		break;
//	}
//}
//
