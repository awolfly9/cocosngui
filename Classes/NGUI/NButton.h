#ifndef __NBUTTON_H__
#define __NBUTTON_H__

#include "NGUI/NProtocol.h"
#include "NGUI/NWidget.h"

class NButton : public NWidget
{
public:
	NButton(void);
	~NButton(void);

	static NButton* create(const string& normal_image);
	static NButton* create(const string& normal_image, const string& selected_image);
	static NButton* create(const string& normal_image, const string& selected_image, const string& click_effect);
	static NButton* create(const string& normal_image, const string& selected_image, const string& disabled_image, const string& click_effect);

	static NButton* create(Sprite* normal_image, const string& click_effect = "");
	static NButton* create(Sprite* normal_image, Sprite* selected_image, const string& click_effect = "");
	static NButton* create(Sprite* normal_image, Sprite* selected_image, Sprite* disabled_image, const string& click_effect = "");

	static NButton* create(const string& normal_image, Size size);
	static NButton* create(const string& normal_image, const string& selected_image, Size size, const string& click_effect = "");
	static NButton* create(const string& normal_image, const string& selected_image, const string& disabled_image, Size size, const string& click_effect);

	static NButton* create(NScale9Sprite* normal_image, const string& click_effect = "");
	static NButton* create(NScale9Sprite* normal_image, Size size, const string& click_effect = "");
	static NButton* create(NScale9Sprite* normal_image, NScale9Sprite* selected_image, const string& click_effect = "");
	static NButton* create(NScale9Sprite* normal_image, NScale9Sprite* selected_image, Size size, const string& click_effect = "");
	static NButton* create(NScale9Sprite* normal_image, NScale9Sprite* selected_image, NScale9Sprite* disabled_image, const string& click_effect = "");
	static NButton* create(NScale9Sprite* normal_image, NScale9Sprite* selected_image, NScale9Sprite* disabled_image, Size size, const string& click_effect = "");
	
	virtual bool initWithFile(const string& normal_image, const string& selected_image, const string& disabled_image, const string& click_effect);
	virtual bool initWithSprite(Sprite* normal_image, Sprite* selected_image, Sprite* disabled_image, const string& click_effect = "");
	virtual bool initWithScale9Sprite(const string& normal_image, const string& selected_image, const string& disabled_image, Size size, const string& click_effect);
	virtual bool initWithScale9Sprite(NScale9Sprite* normal_image, NScale9Sprite* selected_image, NScale9Sprite* disabled_image, Size size, const string& click_effect);

public:
	virtual void setNormalImage(const string& normal_image);
	virtual void setSelectedImage(const string& selected_image);
	virtual void setDisableImage(const string& disable_image);

	virtual void setNormalImage(Node* normal_image);
	virtual void setSelectedImage(Node* selected_image);
	virtual void setDisableImage(Node* disabled_image);

	virtual void setText(const string& str_text, const string& font_name = "Arial", float font_size = 24.0f, const Color3B& normal_color = Color3B(255, 255, 255), const Color3B& selected_color = Color3B(255, 204, 0), const Color3B& disabled_color = Color3B(105, 105, 105));
	void setTextLabel(LabelTTF* lbl_text);
	void setTextString(const string& str_text);
	void setTextFontName(const string& font_name);
	void setTextFontSize(float font_size);
	void setTextNormalColor(const Color3B& normal_color);
	void setTextSelectedColor(const Color3B& selected_color);
	void setTextDisabledColor(const Color3B& disabled_color);

	LabelTTF* getTextLabel();
	string getTextString() const;
	string getTextFontName() const;
	float getTextFontSize() const;
	Color3B getTextNormalColor() const;
	Color3B getTextSelectedColor() const;
	Color3B getTextDisabledColor() const;

	void setOriginalPosition(const Vec2& original_position);
	Vec2 getOriginalPosition() const;

	virtual bool isScale9SpriteEnabled() const;
	virtual void setScale9SpriteEnabled(bool is_scale_9_sprite_enabled);

	void setPressScale(float press_scale);
	float getPressScale() const;

	void setTips(bool is_tips);
	bool isTips() const;

	void setTipsImage(const string& tips_image);
	void setTipsImage(Node* tips_image);
	Node* getTipsImage() const;

	void setTipsImagePosition(const Vec2& position);
	Vec2 getTipsImagePosition() const;

	void setTipsImageAnchorVec2(const Vec2& anchor_point);
	Vec2 getTipsImageAnchorVec2() const;

protected:
	virtual void updateWidget(const Widget_State& widget_state);
	virtual bool hitWidget(const Vec2& point);

protected:
	Node* _normal_image;
	Node* _selected_image;
	Node* _disable_image;
	LabelTTF* _lbl_text;
	 
	Color3B _text_normal_color;
	Color3B _text_selected_color;
	Color3B _text_disabled_color;

	bool _is_scale_9_sprite_enabled;

	float _press_scale;
	Action* _press_action;
	Action* _release_action;

private:
	string _str_lbl_text; 
	string _text_font_name;
	float _text_font_size;
	
	Vec2 _original_position;
	Vec2 _position_percent;

	bool _is_tips;
	Node* _tips_image;
};

#endif // __NBUTTON_H__
