#ifndef __NPROGRESS_BAR_H__
#define __NPROGRESS_BAR_H__

#include "NGUI/NBar.h"

class NProgressBar : public NBar
{
public:
	NProgressBar(void);
	~NProgressBar(void);

	static NProgressBar* create();
	static NProgressBar* create(const string& background, const string& progress_bar);

	virtual bool init(const string& background, const string& progress_bar);

	void setProgressBar(const string& progress_bar);
	void setProgressBar(Sprite* progress_bar);
	Sprite* getProgressBar() const;

protected:
	virtual void updateBar();

protected:
	Size _progress_bar_size;

private:
	Sprite* _progress_bar;
};

#endif // __NPROGRESS_BAR_H__
