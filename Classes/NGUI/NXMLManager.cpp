#include "NXMLManager.h"

NXMLManager* NXMLManager::_instance = nullptr;

#define ROOT "Root"

NXMLManager::NXMLManager(void)
{
}

NXMLManager::~NXMLManager(void)
{
}

NXMLManager* NXMLManager::getInstance()
{
	if (_instance == nullptr)
	{
		_instance = new NXMLManager();
	}

	return _instance;
}

void NXMLManager::destroyInstance()
{
	if (_instance != nullptr)
	{
		delete _instance;
	}
}
//
//void NXMLManager::transferFileToWritablePath(const string& sourece_file_name, const string& destination_file_name)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(sourece_file_name.c_str());
//	ssize_t size;
//	const char* buffer = (const char*)FileUtils::getInstance()->getFileData(file_path, "rb", &size);
//	string buffer_file_path = FileUtils::getInstance()->getWritablePath() + destination_file_name;
//	FILE* file = fopen(buffer_file_path.c_str(), "wb");
//	fwrite(buffer, sizeof(char), size, file);
//	fclose(file);
//}
//
//bool NXMLManager::createXML(const string& file_path)
//{
//	bool ret = false;
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument(); 	
//	if (nullptr==doc) 
//	{  
//		return ret;  
//	}
//
//	tinyxml2::XMLDeclaration *dec = doc->NewDeclaration(nullptr);  
//	if (nullptr==dec)  
//	{  
//		return ret;  
//	} 
//
//	doc->LinkEndChild(dec); 
//
//	tinyxml2::XMLElement* root = doc->NewElement(ROOT);  
//	if (nullptr==root)  
//	{  
//		return false;  
//	}  
//
//	doc->LinkEndChild(root); 
//	ret = tinyxml2::XML_SUCCESS == doc->SaveFile(file_path.c_str());
//
//	if (doc)
//	{
//		delete doc;
//	}
//
//	return ret;
//}
//
//bool NXMLManager::isExistXML(const string& file_name, bool is_create)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	FILE* file = fopen(file_path.c_str(), "rb");
//
//	if (file == nullptr)
//	{
//		if (is_create)
//		{
//			this->createXML(file_path);	
//		}
//
//		return false;
//	}
//	else
//	{
//		fclose(file);
//		return true;
//	}
//}
//
//string NXMLManager::getNodeValue(const string& file_name, const string& key)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//	string data = "";
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc == nullptr)
//	{
//		return data;
//	}
//
//	doc->LoadFile(file_path.c_str());
//
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	if (root != nullptr)
//	{
//		bool ret = this->visitXMLNode(root, node, 1);
//
//		if (node != nullptr && ret == true)
//		{
//			data = string(node->GetText());
//		}
//	}
//
//	if (doc)
//	{
//		delete doc;
//	}
//
//	return data;
//}
//
//void NXMLManager::getNodeChildrenValue(const string& file_name, const string& key, unordered_map<string, string>& children_value)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc == nullptr)
//	{
//		return ;
//	}
//
//	doc->LoadFile(file_path.c_str());
//
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	if (root != nullptr)
//	{
//		bool ret = this->visitXMLNode(root, node, 0);
//
//		if (node != nullptr && ret == true)
//		{
//			for (tinyxml2::XMLElement* element = node->FirstChildElement(); element != nullptr; element = element->NextSiblingElement())
//			{
//				children_value.insert(make_pair(string(element->Value()), string(element->GetText())));
//			}
//		}
//	}
//
//	if (doc)
//	{
//		delete doc;
//	}
//}
//
//void NXMLManager::getNodeChildrenValue(const string& file_name, const string& key, map<string, string>& children_value)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc == nullptr)
//	{
//		return ;
//	}
//
//	doc->LoadFile(file_path.c_str());
//
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	if (root != nullptr)
//	{
//		bool ret = this->visitXMLNode(root, node, 0);
//
//		if (node != nullptr && ret == true)
//		{
//			for (tinyxml2::XMLElement* element = node->FirstChildElement(); element != nullptr; element = element->NextSiblingElement())
//			{
//				children_value.insert(make_pair(string(element->Value()), string(element->GetText())));
//			}
//		}
//	}
//
//	if (doc)
//	{
//		delete doc;
//	}
//}
//
//string NXMLManager::getNodeAttribute(const string& file_name, const string& key)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//	string data = "";
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc == nullptr)
//	{
//		return data;
//	}
//
//	doc->LoadFile(file_path.c_str());
//
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	do 
//	{
//		if (root != nullptr)
//		{
//			bool ret = this->visitXMLNode(root, node, 2);
//
//			if (node != nullptr && ret == true)
//			{
//				if (_keys.size() == 0)
//				{
//					break;
//				}
//
//				string name = _keys.at(0);
//
//				for (const tinyxml2::XMLAttribute* attribute = node->FirstAttribute(); ; attribute = attribute->Next())
//				{
//					if (string(attribute->Name()) == name)
//					{
//						data =  string(attribute->Value());
//						break;
//					}
//				}
//			}
//		}
//	} while (0);
//	
//	if (doc)
//	{
//		delete doc;
//	}
//
//	return data;
//}
//
//void NXMLManager::getNodeAttributes(const string&file_name, const string& key, unordered_map<string, string>& attributes)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc == nullptr)
//	{
//		return ;
//	}
//
//	doc->LoadFile(file_path.c_str());
//
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	do 
//	{
//		if (root != nullptr)
//		{
//			bool ret = this->visitXMLNode(root, node, 2);
//
//			if (node != nullptr && ret == true)
//			{
//				if (_keys.size() == 0)
//				{
//					break;
//				}
//
//				for (const tinyxml2::XMLAttribute* attribute = node->FirstAttribute(); ; attribute = attribute->Next())
//				{
//					attributes.insert(make_pair(string(attribute->Name()), string(attribute->Value())));
//				}
//			}
//		}
//	} while (0);
//	
//	if (doc)
//	{
//		delete doc;
//	}
//}
//
//void NXMLManager::getNodeAttributes(const string&file_name, const string& key, map<string, string>& attributes)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc == nullptr)
//	{
//		return ;
//	}
//
//	doc->LoadFile(file_path.c_str());
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	do 
//	{
//		if (root != nullptr)
//		{
//			bool ret = this->visitXMLNode(root, node, 2);
//
//			if (node != nullptr && ret == true)
//			{
//				if (_keys.size() == 0)
//				{
//					break;
//				}
//
//				for (const tinyxml2::XMLAttribute* attribute = node->FirstAttribute(); ; attribute = attribute->Next())
//				{
//					attributes.insert(make_pair(string(attribute->Name()), string(attribute->Value())));
//				}
//			}
//		}
//	} while (0);
//		
//	if (doc)
//	{
//		delete doc;
//	}
//}
//
//void NXMLManager::setNodeValue(const string& file_name, const string& key, const string& value)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc == nullptr)
//	{
//		return ;
//	}
//
//	doc->LoadFile(file_path.c_str());
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	do 
//	{
//		if (root != nullptr)
//		{
//			bool ret = this->visitXMLNode(root, node, 1);
//
//			if (node != nullptr && ret == true)
//			{
//				if (node->FirstChild())
//				{
//					node->FirstChild()->SetValue(value.c_str());
//				}
//				else
//				{
//					tinyxml2::XMLText* content = doc->NewText(value.c_str());
//					node->LinkEndChild(content);
//				}
//
//				doc->SaveFile(file_path.c_str());
//			}
//		}
//	} while (0);
//	
//	if (doc)
//	{
//		delete doc;
//	}
//}
//
//void NXMLManager::setNodeAttribute(const string& file_name, const string& key, const string& value)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc == nullptr)
//	{
//		return ;
//	}
//
//	doc->LoadFile(file_path.c_str());
//
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	do 
//	{
//	if (root != nullptr)
//	{
//		bool ret = this->visitXMLNode(root, node, 2);
//
//		if (node != nullptr && ret == true)
//		{
//			if (_keys.size() == 0)
//			{
//				break;
//			}
//
//			string name = _keys.at(0);
//			node->SetAttribute(name.c_str(), value.c_str());
//			doc->SaveFile(file_path.c_str());
//		}
//	}
//	} while (0);
//	
//	if (doc)
//	{
//		delete doc;
//	}
//}
//
//void NXMLManager::addNodeValue(const string& file_name, const string& key, const string& value)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc ==  nullptr)
//	{
//		return ;
//	}
//	doc->LoadFile(file_path.c_str());
//
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	do 
//	{
//		if (root != nullptr)
//		{
//			bool ret = this->visitXMLNode(root, node, 2);
//
//			if (node != nullptr && ret == true)
//			{
//				if (_keys.size() == 0)
//				{
//					break;
//				}
//
//				string name = _keys.at(0);
//
//				tinyxml2::XMLElement* ele = doc->NewElement(name.c_str());
//				ele->LinkEndChild(doc->NewText(value.c_str()));
//
//				node->LinkEndChild(ele);
//				doc->SaveFile(file_path.c_str());
//			}
//		}
//	} while (0);
//	
//	if (doc)
//	{
//		delete doc;
//	}
//}
//
//void NXMLManager::addNodeAttribute(const string& file_name, const string& key, const string& value)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc ==  nullptr)
//	{
//		return ;
//	}
//	doc->LoadFile(file_path.c_str());
//
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	do 
//	{
//		if (root != nullptr)
//		{
//			bool ret = this->visitXMLNode(root, node, 2);
//
//			if (node != nullptr && ret == true)
//			{
//				if (_keys.size() == 0)
//				{
//					break;
//				}
//
//				string name = _keys.at(0);
//				node->SetAttribute(name.c_str(), value.c_str());
//				doc->SaveFile(file_path.c_str());
//			}
//		}
//	} while (0);
//	
//	if (doc)
//	{
//		delete doc;
//	}
//}
//
//void NXMLManager::addNodeAttibutes(const string& file_name, const string& key, unordered_map<string, string>& attributes)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc ==  nullptr)
//	{
//		return ;
//	}
//	doc->LoadFile(file_path.c_str());
//
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	do 
//	{
//		if (root != nullptr)
//		{
//			bool ret = this->visitXMLNode(root, node, 1);
//
//			if (node != nullptr && ret == true)
//			{
//				if (_keys.size() == 0)
//				{
//					break;
//				}
//
//				for (unordered_map<string, string>::iterator iter = attributes.begin(); iter != attributes.end(); ++ iter)
//				{
//					string name = iter->first;
//					string value = iter->second;
//					node->SetAttribute(name.c_str(), value.c_str());
//				}
//
//				doc->SaveFile(file_path.c_str());
//			}
//		}
//	} while (0);
//	
//	if (doc)
//	{
//		delete doc;
//	}
//}
//
//void NXMLManager::addNodeAttibutes(const string& file_name, const string& key, map<string, string> attributes)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc ==  nullptr)
//	{
//		return ;
//	}
//	doc->LoadFile(file_path.c_str());
//
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	do 
//	{
//		if (root != nullptr)
//		{
//			bool ret = this->visitXMLNode(root, node, 1);
//
//			if (node != nullptr && ret == true)
//			{
//				if (_keys.size() == 0)
//				{
//					break;
//				}
//
//				for (map<string, string>::iterator iter = attributes.begin(); iter != attributes.end(); ++ iter)
//				{
//					string name = iter->first;
//					string value = iter->second;
//					node->SetAttribute(name.c_str(), value.c_str());
//				}
//
//				doc->SaveFile(file_path.c_str());
//			}
//		}
//	} while (0);
//	
//	if (doc)
//	{
//		delete doc;
//	}
//}
//
//void NXMLManager::deleteNodeValue(const string& file_name, const string& key)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc ==  nullptr)
//	{
//		return ;
//	}
//	doc->LoadFile(file_path.c_str());
//
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	do 
//	{
//		if (root != nullptr)
//		{
//			bool ret = this->visitXMLNode(root, node, 2);
//
//			if (node != nullptr && ret == true)
//			{
//				if (_keys.size() == 0)
//				{
//					break;
//				}
//
//				string name = _keys.at(0);
//
//				for (tinyxml2::XMLElement* element = node->FirstChildElement(); element != nullptr ; element = element->NextSiblingElement())
//				{
//					if (string(element->Value()) == name)
//					{
//						node->DeleteChild(element);
//						break;
//					}		
//				}
//
//				doc->SaveFile(file_path.c_str());
//			}
//		}
//	} while (0);
//	
//	if (doc)
//	{
//		delete doc;
//	}
//}
//
//void NXMLManager::deleteNodeAttribute(const string& file_name, const string& key)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc ==  nullptr)
//	{
//		return ;
//	}
//	doc->LoadFile(file_path.c_str());
//
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	do 
//	{
//		if (root != nullptr)
//		{
//			bool ret = this->visitXMLNode(root, node, 2);
//
//			if (node != nullptr && ret == true)
//			{
//				if (_keys.size() == 0)
//				{
//					break;
//				}
//
//				string name = _keys.at(0);
//				node->DeleteAttribute(name.c_str());
//				doc->SaveFile(file_path.c_str());
//			}
//		}
//	} while (0);
//	
//	if (doc)
//	{
//		delete doc;
//	}
//}
//
//void NXMLManager::deleteNodeAttributes(const string& file_name, const string& key, unordered_map<string, string> attributes)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc ==  nullptr)
//	{
//		return ;
//	}
//	doc->LoadFile(file_path.c_str());
//
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	do 
//	{
//		if (root != nullptr)
//		{
//			bool ret = this->visitXMLNode(root, node, 1);
//
//			if (node != nullptr && ret == true)
//			{
//				if (_keys.size() == 0)
//				{
//					break;
//				}
//
//				for (auto iter = attributes.begin(); iter != attributes.end(); ++ iter)
//				{
//					string name = iter->first;
//					node->DeleteAttribute(name.c_str());
//				}
//
//				doc->SaveFile(file_path.c_str());
//			}
//		}
//	} while (0);
//	
//	if (doc)
//	{
//		delete doc;
//	}
//}
//
//void NXMLManager::deleteNodeAttributes(const string& file_name, const string& key, map<string, string> attributes)
//{
//	string file_path = FileUtils::getInstance()->fullPathForFilename(file_name.c_str());
//	this->splitKey(key);
//
//	tinyxml2::XMLDocument* doc = new tinyxml2::XMLDocument();
//	if (doc ==  nullptr)
//	{
//		return ;
//	}
//	doc->LoadFile(file_path.c_str());
//
//	tinyxml2::XMLElement* root = doc->RootElement();
//	tinyxml2::XMLElement* node = nullptr;
//
//	do 
//	{
//		if (root != nullptr)
//		{
//			bool ret = this->visitXMLNode(root, node, 1);
//
//			if (node != nullptr && ret == true)
//			{
//				if (_keys.size() == 0)
//				{
//					break;
//				}
//
//				for (auto iter = attributes.begin(); iter != attributes.end(); ++ iter)
//				{
//					string name = iter->first;
//					node->DeleteAttribute(name.c_str());
//				}
//
//				doc->SaveFile(file_path.c_str());
//			}
//		}
//	} while (0);
//	
//	if (doc)
//	{
//		delete doc;
//	}
//}
//
//bool NXMLManager::visitXMLNode(tinyxml2::XMLElement* root, tinyxml2::XMLElement* &node, int count)
//{
//	if (_keys.size() <= 0)
//	{
//		return false;
//	}
//
//	if (_keys.size() <= count)
//	{
//		string name = _keys.at(0);
//		_keys.erase(_keys.begin());
//
//		if (string(root->Value()) == name)
//		{
//			node = root;
//			return true;
//		}
//	}
//	else
//	{
//		_keys.erase(_keys.begin());
//		return this->visitXMLNodeValue(root, node, count);
//	}
//
//	return false;
//}

//bool NXMLManager::visitXMLNodeValue(tinyxml2::XMLElement* root, tinyxml2::XMLElement* &node, int count)
//{
//	if (_keys.size() <= 0)
//	{
//		return false;
//	}
//
//	string key = _keys.at(0);
//	_keys.erase(_keys.begin());
//
//	for (tinyxml2::XMLElement* element = root->FirstChildElement(); element != nullptr ; element = element->NextSiblingElement())
//	{
//		if (string(element->Value()) == key)
//		{
//			if (_keys.size() >= count)
//			{
//				return this->visitXMLNodeValue(element, node, count);
//			}
//			else
//			{
//				node = element;
//				return true ;
//			}
//		}		
//	}
//
//	return false;
//}

void NXMLManager::splitKey(const string& key, const string& separator)
{
	_keys.clear();
	int start_pos_found = key.find(separator);
	int start_flag = 0;
	
	for (; start_pos_found != -1; start_pos_found = key.find(separator, start_pos_found))
	{
		string str = key.substr(start_flag, start_pos_found - start_flag);
		start_pos_found = start_pos_found + separator.size();
		start_flag = start_pos_found;

		_keys.push_back(str);
	}

	if (start_flag < key.size())
	{
		string str = key.substr(start_flag, key.size() - start_flag);
		_keys.push_back(str);
	}
}
