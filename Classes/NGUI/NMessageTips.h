#ifndef __NMESSAGE_TIPS_H__
#define __NMESSAGE_TIPS_H__

#include "NGUI/NProtocol.h"
#include <string>

using std::string;

class NMessageTips
{
public:
	NMessageTips(void);
	~NMessageTips(void);

	static NMessageTips* getInstance();
	static void destroyInstance();

	bool init(const string& background, const string& font_name , float font_size, const Color3B& color);

	void addTips(const string& tips);
	void addTips(Node* tips);
	
	void clear();

	void setBackground(const string& background);
	const string& getBackground() const;

	void setFontName(const string& font_name);
	const string& getFontName() const;

	void setFontSize(float font_size);
	float getFontSize() const;

	void setColor(const Color3B& color);
	const Color3B& getColor() const;

	void setDistance(float distance);
	float getDistance() const;

	void setAction(FiniteTimeAction* action);
	Action* getAction() const;

private:
	void updateTips();
	void callfunc(Ref* ref);

private:
	deque<Node*> _tips_deque;
	Action* _action;

	string _background;
	string _font_name;
	float _font_size;
	Color3B _color;
	float _distance;

	static NMessageTips* _instance;
};

#endif // __NMESSAGE_TIPS_H__