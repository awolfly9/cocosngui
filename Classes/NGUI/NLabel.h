//#ifndef __NLABEL_H__
//#define __NLABEL_H__
//
//#include "NGUI/NProtocol.h"
//#include "NGUI/NWidget.h"
//#include "NGUI/NLabelTTF.h"
//
//class NLabel : public NWidget
//{
//public:
//	NLabel(void);
//	~NLabel(void);
//
//	static NLabel* create();
//    static NLabel* create(const std::string& text, const std::string& font, float fontSize,
//        const Size& dimensions = Size::ZERO, TextHAlignment hAlignment = TextHAlignment::LEFT,
//        TextVAlignment vAlignment = TextVAlignment::TOP);
//
//    static NLabel * createWithTTF(const std::string& text, const std::string& fontFile, float fontSize,
//        const Size& dimensions = Size::ZERO, TextHAlignment hAlignment = TextHAlignment::LEFT,
//        TextVAlignment vAlignment = TextVAlignment::TOP);
//
//    static NLabel* createWithTTF(const TTFConfig& ttfConfig, const std::string& text, TextHAlignment alignment = TextHAlignment::LEFT, int maxLineWidth = 0);
//
//    static NLabel* create(const std::string& bmfontFilePath, const std::string& text,
//        const TextHAlignment& alignment = TextHAlignment::LEFT, int maxLineWidth = 0, 
//        const Vec2& imageOffset = Vec2::ZERO);
//    
//    static NLabel * create(const std::string& charMapFile, int itemWidth, int itemHeight, int startCharMap);
//    static NLabel * create(Texture2D* texture, int itemWidth, int itemHeight, int startCharMap);
//    static NLabel * create(const std::string& plistFile);
//
//	void setNormalColor(const Color3B& normal_color);
//	void setSelectedColor(const Color3B& selected_color);
//	void setDisabledColor(const Color3B& disabled_color);
//	Color3B getNormalColor() const;
//	Color3B getSelectedColor() const;
//	Color3B getDisabledColor() const;
//
//	virtual bool onTouchBegan(Touch* touch, Event* event);
//	virtual void onTouchMoved(Touch* touch, Event* event);
//	virtual void onTouchEnded(Touch* touch, Event* event);
//	virtual void onTouchCancelled(Touch* touch, Event* event);
//
//protected:
//	virtual bool hitWidget(const Vec2& point);
//	void updateWidget(const Widget_State& widget_state);
//
//private:
//	Color3B _normal_color;
//	Color3B _selected_color;
//	Color3B _disabled_color;
//
//	Label* _label;
//};
//
//#endif // __NLABEL_H__
//
