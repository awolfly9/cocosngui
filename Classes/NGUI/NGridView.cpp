#include "NGridView.h"


NGridViewCell::NGridViewCell(void)
{
	this->setUniqueID(NGRID_VIEW_CELL);
}

NGridViewCell::~NGridViewCell(void)
{

}

NGridView::NGridView(void)
	: _columns(0)
	, _row(0)
{
	this->setUniqueID(NGRID_VIEW);
}

NGridView::~NGridView(void)
{
}

NGridView* NGridView::create(const Size& view_size, const Size& cell_size, int cell_count, int columns, Load_Callback load_callback)
{
	NGridView* ret = new NGridView();

	if (ret && ret->init(view_size, cell_size, cell_count, columns, load_callback))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

bool NGridView::init(const Size& view_size, const Size& cell_size, int cell_count, int columns, Load_Callback load_cell)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NTableView::init(view_size, cell_size, cell_count, load_cell));

	this->setColumns(columns);

	_row = _cell_count / _columns;

	if (_cell_count % _columns != 0)
	{
		_row += 1;
	}

	INIT_DO_WHILE_END
}

void NGridView::setColumns(int columns)
{
	if (columns > 0 && columns <= this->getCellCount())
	{
		_columns = columns;
	}
}

int NGridView::getColumns() const
{
	return _columns;
}

void NGridView::reloadData()
{
	NTableView::reloadData();
}

void NGridView::setDirection(ScrollView_Direction scroll_view_direction)
{
	return ;
}

void NGridView::onScrolling()
{
	NTableView::onScrolling();
}

void NGridView::onScrollingEnd()
{
	if (! _is_bouncing && _is_auto_relocate)
	{
		int origin = this->getOriginIndex(this->getContainerPosition());
		origin /= _columns;

		Vec2 origin_pos = Vec2(this->getContainerPosition().x, origin * _cell_size.height);
		float offset = (this->getContainerSize().height - origin_pos.y - this->getContentSize().height - (- this->getContainerPosition().y));

		if (offset <= _cell_size.height / 2)
		{
			this->startAutoBounce(Vec2(this->getContainerPosition().x, this->getContainerPosition().y - offset));
		}
		else
		{
			this->startAutoBounce(Vec2(this->getContainerPosition().x, this->getContainerPosition().y + (_cell_size.height - offset)));
		}
	}
}

void NGridView::updatePosition()
{
	if (_cell_count <= 0)
	{
		return ;
	}

	float total_height = _cell_size.height * _cell_count / _columns;

	if (_cell_count % _columns != 0)
	{
		total_height += _cell_size.height;
	}

	this->setContainerSize(Size(this->getContentSize().width, total_height));

	for (int i = 0; i < _row; ++ i)
	{
		float height = _cell_size.height * 0.5f + _cell_size.height * i;

		for (int j = 0; j < _columns; ++ j)
		{
			float width = _cell_size.width * 0.5f + _cell_size.width * j;

			_position.push_back(Vec2(width, this->getContainerSize().height - height));
		}
	}
}

void NGridView::loadCellWithIndex(int index)
{
	NGridViewCell* cell = (NGridViewCell*)_load_callback(this, index);

	if (cell == nullptr)
	{
		return ;
	}

	this->addChild(cell);
	cell->setPosition(_position.at(index));
	cell->setIndex(index);

	_indexs.insert(index);

	if (_cells->empty())
	{
		_cells->push_back(cell);
	}
	else
	{
		bool is_insert = false;
		for (list<NTableViewCell*>::iterator iter = _cells->begin(); iter != _cells->end(); ++ iter)
		{
			NTableViewCell* pCell = (NTableViewCell*)(*iter);

			if (pCell == nullptr)
			{
				continue;
			}

			if (pCell->getIndex() > index)
			{
				_cells->insert(iter, cell);
				is_insert = true;
				break;
			}
		}

		if (! is_insert)
		{
			_cells->push_back(cell);
		}
	}
}

int NGridView::getOriginIndex(const Vec2& point)
{
	int index = (this->getContainerSize().height - (- point.y + this->getContentSize().height)) / _cell_size.height;

	index *= _columns;
	index = MAX(index, 0);
	index = MIN(index, _cell_count - 1);

	return index;
}

int NGridView::getEndIndex(const Vec2& point)
{
	int index =  (this->getContainerSize().height - (- point.y)) / _cell_size.height;

	index *= _columns;
	index += _columns - 1;
	index = MAX(index, 0);
	index = MIN(index, _cell_count - 1);

	return index;
}