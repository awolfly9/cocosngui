#include "NListView.h"

NListView::NListView(void)
	: _list_view_align_type(ListView_Align_Type::kListView_Align_Vertical_Center)
{
	this->setUniqueID(NLIST_VIEW);
	_nodes = new vector<Node*>();
}

NListView::~NListView(void)
{
	this->removeAllNodes();
	delete _nodes;	
}

NListView* NListView::create(Size view_size)
{
	NListView* ret = new NListView();

	if (ret && ret->init(view_size))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

bool NListView::init(Size view_size)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NScrollView::init(view_size));

	INIT_DO_WHILE_END
}

void NListView::setDirection(const ScrollView_Direction& scroll_view_direction)
{
	if (scroll_view_direction != ScrollView_Direction::kScrollView_Dir_Both && this->getDirection() != scroll_view_direction)
	{
		NScrollView::setDirection(scroll_view_direction);
	}
}

void NListView::setAlignType(const ListView_Align_Type& list_view_align_type)
{
	if (_list_view_align_type == list_view_align_type)
	{
		return ;
	}
	
	if (this->getDirection() == ScrollView_Direction::kScrollView_Dir_Vertical)
	{
		if ((int)list_view_align_type <= 2)
		{
			_list_view_align_type = list_view_align_type;
			this->reloadData();
			return ;
		}
	}

	if (this->getDirection() == ScrollView_Direction::kScrollView_Dir_Horizontal)
	{
		if ((int)list_view_align_type > 2)
		{
			_list_view_align_type = list_view_align_type;
			this->reloadData();
		}
	}
}

ListView_Align_Type NListView::getAlignType() const
{
	return _list_view_align_type;
}

void NListView::insertNodeAtLast(Node* node)
{
	_nodes->push_back(node);
	node->retain();
}

void NListView::insertNodeAtFront(Node* node)
{
	_nodes->insert(_nodes->begin(), node);
	node->retain();
}

void NListView::insertNode(Node* node, int index)
{
	if (this->isEmptyListNodes())
	{
		return ;
	}

	if (index >= (int)_nodes->size())
	{
		this->insertNodeAtLast(node);

		return ;
	}

	_nodes->insert(_nodes->begin() + index, node);
	node->retain();
}

void NListView::removeNodeAtIndex(int index)
{
	if (index < 0 || index >=(int) _nodes->size())
	{
		return ;
	}

	_nodes->at(index)->release();
	_nodes->erase(_nodes->begin() + index);
}

void NListView::removeNode(Node* node)
{
	if (node == nullptr)
	{
		return ;
	}

	if ( this->isEmptyListNodes() )
	{
		return ;
	}

	vector<Node*>::iterator iter = std::find(
		_nodes->begin(),
		_nodes->end(),
		node);

	if (iter != _nodes->end())
	{
		node->release();
		_nodes->erase(iter);
	}
}

void NListView::removeFrontNode()
{
	if (this->isEmptyListNodes())
	{
		return ;
	}

	_nodes->at(0)->release();
	_nodes->erase(_nodes->begin());
}

void NListView::removeLastNode()
{
	if (this->isEmptyListNodes())
	{
		return ;
	}

	_nodes->at(_nodes->size() - 1)->release();
	_nodes->pop_back();
}

void NListView::removeAllNodes()
{
	for (vector<Node*>::iterator iter = _nodes->begin(); iter != _nodes->end(); ++ iter)
	{
		Node* node = (Node*)(*iter);
		node->release();
	}

	_nodes->clear();
}

vector<Node*>* NListView::getNodes()
{
	return _nodes;
}

Node* NListView::getNodeAtIndex(int index)
{
	if (this->isEmptyListNodes() || index >= (int)_nodes->size() || index < 0)
	{
		return nullptr;
	}

	return _nodes->at(index);
}

int NListView::getNodesCount() const
{
	return _nodes->size();
}

bool NListView::isEmptyListNodes()
{
	if (_nodes->size() == 0)
	{
		return true;
	}

	return false;
}

void NListView::reloadData()
{
	if (this->getDirection() == ScrollView_Direction::kScrollView_Dir_Both)
	{
		return ;
	}

	this->updateNodesPosition();
	this->relocateContainer();
}

void NListView::updateNodesPosition()
{
	this->getContainer()->removeAllChildren();

	switch (this->getDirection())
	{
	case ScrollView_Direction::kScrollView_Dir_Vertical:
		{
			float total_height = 0.0f;

			for (vector<Node*>::iterator iter = _nodes->begin(); iter != _nodes->end(); ++ iter)
			{
				Node* node = (Node*)(*iter);

				if (node == nullptr)
				{
					continue ;
				}

				total_height += node->getContentSize().height;
			}
			
			total_height = MAX(total_height, this->getContentSize().height);
			this->setContainerSize(Size(this->getContainer()->getContentSize().width, total_height));

			float height = 0.0f;

			switch (_list_view_align_type)
			{
			case ListView_Align_Type::kListView_Align_Vertical_Left:
				{
					for (vector<Node*>::iterator iter = _nodes->begin(); iter != _nodes->end(); ++ iter)
					{
						Node* node = (Node*)(*iter);

						if (node == nullptr)
						{
							continue ;
						}

						node->setAnchorPoint(Vec2(0.0f, 1.0f));
						node->setPosition(Vec2(0, this->getContainer()->getContentSize().height - height));
						this->getContainer()->addChild(node);

						height += node->getContentSize().height;
					}					
				}
				break;
			case ListView_Align_Type::kListView_Align_Vertical_Right:
				{
					for (vector<Node*>::iterator iter = _nodes->begin(); iter != _nodes->end(); ++ iter)
					{
						Node* node = (Node*)(*iter);

						if (node == nullptr)
						{
							continue ;
						}

						node->setAnchorPoint(Vec2(1.0f, 1.0f));
						node->setPosition(Vec2(this->getContainer()->getContentSize().width, this->getContainer()->getContentSize().height - height));
						this->getContainer()->addChild(node);

						height += node->getContentSize().height;
					}
				}			
				break;
			case ListView_Align_Type::kListView_Align_Vertical_Center:
				{
					for (vector<Node*>::iterator iter = _nodes->begin(); iter != _nodes->end(); ++ iter)
					{
						Node* node = (Node*)(*iter);

						if (node == nullptr)
						{
							continue ;
						}

						node->setAnchorPoint(Vec2(0.5f, 1.0f));
						node->setPosition(Vec2(this->getContainer()->getContentSize().width * 0.5, this->getContainer()->getContentSize().height - height));
						this->getContainer()->addChild(node);

						height += node->getContentSize().height;
					}
				}
				break;
			default:
				break;
			}
			break;
		}
	case ScrollView_Direction::kScrollView_Dir_Horizontal:
		{
			float total_width = 0.0f;

			for (vector<Node*>::iterator iter = _nodes->begin(); iter != _nodes->end(); ++ iter)
			{
				Node* node = (Node*)(*iter);

				if (node == nullptr)
				{
					continue ;
				}

				total_width += node->getContentSize().width;
			}

			total_width = MAX(total_width, this->getContentSize().width);
			this->setContainerSize(Size(total_width, this->getContainer()->getContentSize().height));

			float width = 0.0f;

			switch (_list_view_align_type)
			{				
			case ListView_Align_Type::kListView_Aligan_Horizontal_Top:
				{			
					for (vector<Node*>::iterator iter = _nodes->begin(); iter != _nodes->end(); ++ iter)
					{
						Node* node = (Node*)(*iter);

						if (node == nullptr)
						{
							continue ;
						}

						node->setAnchorPoint(Vec2(0.0f, 1.0f));
						node->setPosition(Vec2(width, this->getContainer()->getContentSize().height));
						this->getContainer()->addChild(node);

						width += node->getContentSize().width;
					}
				}
				break;
			case ListView_Align_Type::kListView_Aligan_Horizontal_Bottom:
				{
					for (vector<Node*>::iterator iter = _nodes->begin(); iter != _nodes->end(); ++ iter)
					{
						Node* node = (Node*)(*iter);

						if (node == nullptr)
						{
							continue ;
						}

						node->setAnchorPoint(Vec2(0.0f, 0.0f));
						node->setPosition(Vec2(width, 0));
						this->getContainer()->addChild(node);

						width += node->getContentSize().width;
					}
				}
				break;
			case ListView_Align_Type::kListView_Aligan_Horizontal_Center:
				{
					for (vector<Node*>::iterator iter = _nodes->begin(); iter != _nodes->end(); ++ iter)
					{
						Node* node = (Node*)(*iter);

						if (node == nullptr)
						{
							continue ;
						}

						node->setAnchorPoint(Vec2(0.0f, 0.5f));
						node->setPosition(Vec2(width, this->getContainer()->getContentSize().height * 0.5));
						this->getContainer()->addChild(node);

						width += node->getContentSize().width;
					}
				}
				break;
			default:
				break;
			}
		}
		break;
	default:
		break;
	}
}

void NListView::relocateContainer()
{
	switch (this->getDirection())
	{
	case ScrollView_Direction::kScrollView_Dir_Vertical:
		{
			switch (_list_view_align_type)
			{
			case ListView_Align_Type::kListView_Align_Vertical_Left:
				{
					this->getContainer()->setPosition(Vec2(0, this->getContentSize().height - this->getContainerSize().height));
				}
				break;
			case ListView_Align_Type::kListView_Align_Vertical_Right:
				{
					this->getContainer()->setPosition(Vec2(this->getContentSize().width - this->getContainerSize().width, this->getContentSize().height - this->getContainerSize().height));
				}
				break;
			case ListView_Align_Type::kListView_Align_Vertical_Center:
				{
					this->getContainer()->setPosition(Vec2(this->getContentSize().width * 0.5 - this->getContainerSize().width * 0.5, this->getContentSize().height - this->getContainerSize().height));
				}
				break;
			default:
				break;
			}
		}
		break;
	case  ScrollView_Direction::kScrollView_Dir_Horizontal:
		{
			switch (_list_view_align_type)
			{
			case ListView_Align_Type::kListView_Aligan_Horizontal_Top:
				{
					this->getContainer()->setPosition(Vec2(0, this->getContentSize().height - this->getContainerSize().height));
				}
				break;
			case ListView_Align_Type::kListView_Aligan_Horizontal_Bottom:
				{
					this->getContainer()->setPosition(Vec2(0, 0));
				}
				break;
			case ListView_Align_Type::kListView_Aligan_Horizontal_Center:
				{
					this->getContainer()->setPosition(Vec2(0, this->getContentSize().height * 0.5 - this->getContainerSize().height * 0.5));
				}
				break;
			default:
				break;
			}
		}
		break;
	default:
		break;
	}
}
