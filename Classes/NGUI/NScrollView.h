#ifndef __NSCROLL_VIEW_H__
#define __NSCROLL_VIEW_H__

#include "NGUI/NProtocol.h"
#include "NGUI/NWidget.h"
#include "NGUI/NScrollBar.h"

class NScrollViewContainer : public NWidget
{
public:
	NScrollViewContainer(void);
	~NScrollViewContainer(void);

	CREATE_FUNC(NScrollViewContainer);
	virtual bool init();

	void reset();
};

class NScrollView : public NWidget
{
public:
	NScrollView(void);
	~NScrollView(void);

	static NScrollView* create(const Size& view_size);
	virtual bool init(const Size& view_size);

	NScrollViewContainer* getContainer();

	void setContainerPosition(const Vec2& position);
	Vec2 getContainerPosition() const;

	void setContainerSize(const Size& size);
	Size getContainerSize() const;

	void setDirection(const ScrollView_Direction& _direction);
	ScrollView_Direction getDirection() const;

	void setBounce(bool is_bounce);
	bool isBounce() const;

	void setDecelerate(bool is_decelerate);
	bool isDecelerate() const;

	void setDragable(bool is_dragable);
	bool isDragable() const;

	void setAutoScroll(bool is_auto_scroll);
	bool isAutoscroll() const;

	void setBouncePercent(const Vec2& bounce_percent);
	Vec2 getBouncePercent() const;

	void setScrollBar(NScrollBar* scroll_bar);
	void setScrollBar(const string& background, NButton* btn_scroll_bar = nullptr);
	void setScrollBar(NButton* btn_scroll_bar);
	void setScrollBar(const string& background, const string& normal_image, const string& selected_image = "", const string& disabled_image = "", const string& click_effect = "");
	NScrollBar* getScrollBar() const;

	void setScrollBarDirection(const Bar_Direction& bar_direction);
	Bar_Direction getScrollBarDirection() const;

	void setScrollBarPositionFactor(float factor);
	float getScrollBarPositionFactor() const;

	virtual void setContentSize(const Size& view_size);

	virtual bool onTouchBegan(Touch* touch, Event* event);
	virtual void onTouchMoved(Touch* touch, Event* event);
	virtual void onTouchEnded(Touch* touch, Event* event);
	virtual void onTouchCancelled(Touch* touch, Event* event);

	virtual void addChild(Node * child, bool is_container =  true);
	virtual void addChild(Node * child, int zorder, bool is_container = true);
	virtual void addChild(Node* child, int zorder, int tag, bool is_container = true);

protected:
	virtual void interceptTouch(const Touch_Period& touch_period, Touch* touch, NWidget* widget);

	virtual void handlePressLogic(Touch *touch);
	virtual void handleMoveLogic(Touch *touch);
	virtual void handleReleaseLogic(Touch *touch);

	virtual void onScrolling() { };
	virtual void onDraggingEnd() { };
	virtual void onScrollingEnd() { };
	virtual void onBouncingEnd() { };

	void scrollContainer(const Vec2& distance);
	Vec2 validatePosition(const Vec2& position, bool is_ignoce_bounce = false);
	bool rightfulPosition(const Vec2& position);

	virtual void startAutoScroll();
	virtual void autoScrolling(float dt);
	virtual void stopAutoScroll();

	virtual void startAutoBounce(const Vec2& position);
	virtual void autoBouncing(float dt);
	virtual void stopAutoBounce();

	void updateScrollBar(Ref* object, const Bar_Period& bar_period);

	virtual void visit(Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags);
	virtual void beforeVisit();
	virtual void afterVisit();

protected:
	NScrollViewContainer* _container;
	ScrollView_Direction _direction;

	bool _is_bounce;
	bool _is_bouncing;
	bool _is_decelerate;
	bool _is_decelerateing;
	bool _is_auto_scroll;
	bool _is_auto_scrolling;
	bool _is_dragable;
	bool _is_dragging;

private:
	Vec2 _move_touch_point;
	Vec2 _end_touch_point;

	Vec2 _bounce_percent;

	float _scroll_speed;
	float _drag_time;
	Vec2 _drag_direction;

	NScrollBar* _scroll_bar;
	float _scroll_bar_position_factor;

	CustomCommand _before_visit;
	CustomCommand _after_visit;
};

#endif // __NSCROLL_VIEW_H__
