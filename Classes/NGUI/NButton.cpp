#include "NButton.h"

#define TIPS_IMAGE_ZORDER 10

NButton::NButton(void)
	: _normal_image(nullptr)
	, _selected_image(nullptr)
	, _disable_image(nullptr)
	, _lbl_text(nullptr)
	, _str_lbl_text("")
	, _text_font_name("Arial")
	, _text_font_size(24.0f)
	, _text_normal_color(Color3B(255, 255, 255))
	, _text_selected_color(Color3B(255, 255, 0))
	, _text_disabled_color(Color3B(105, 105, 105))
	, _is_scale_9_sprite_enabled(false)
	, _original_position(Vec2(0, 0))
	, _position_percent(Vec2(0, 0))
	, _press_action(nullptr)
	, _press_scale(1.0)
	, _release_action(nullptr)
	, _is_tips(true)
	, _tips_image(nullptr)
{
	this->setUniqueID(NBUTTON);
}

NButton::~NButton(void)
{
	if (_press_action != nullptr)
	{
		_press_action->release();
		_press_action = nullptr;
	}

	if (_release_action != nullptr)
	{
		_release_action->release();
		_release_action = nullptr;
	}
}

NButton* NButton::create(const string& normal_image)
{
	return create(normal_image, "", "", "");
}

NButton* NButton::create(const string& normal_image, const string& selected_image)
{
	return create(normal_image, selected_image, "", "");
}

NButton* NButton::create(const string& normal_image, const string& selected_image, const string& click_effect)
{
	return create(normal_image, selected_image, "", click_effect);
}

NButton* NButton::create(const string& normal_image, const string& selected_image, const string& disabled_image, const string& click_effect)
{
	NButton* ret = new NButton();

	if (ret && ret->initWithFile(normal_image, selected_image, disabled_image, click_effect))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

NButton* NButton::create(Sprite* normal_image, const string& click_effect)
{
	return create(normal_image, nullptr, nullptr, click_effect);
}

NButton* NButton::create(Sprite* normal_image, Sprite* selected_image, const string& click_effect)
{
	return create(normal_image, selected_image, nullptr, click_effect);
}

NButton* NButton::create(Sprite* normal_image, Sprite* selected_image, Sprite* disabled_image, const string& click_effect)
{
	NButton* ret = new NButton();

	if (ret && ret->initWithSprite(normal_image, selected_image, disabled_image, click_effect))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

NButton* NButton::create(const string& normal_image, Size size)
{
	return create(normal_image, "", "", size, "");
}

NButton* NButton::create(const string& normal_image, const string& selected_image, Size size, const string& click_effect)
{
	return create(normal_image, selected_image, "", size, click_effect);
}

NButton* NButton::create(const string& normal_image, const string& selected_image, const string& disabled_image, Size size, const string& click_effect)
{
	NButton* ret = new NButton();

	if (ret && ret->initWithScale9Sprite(normal_image, selected_image, disabled_image, size, click_effect))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

NButton* NButton::create(NScale9Sprite* normal_image, const string& click_effect)
{
	return create(normal_image, normal_image->getContentSize(), click_effect);
}

NButton* NButton::create(NScale9Sprite* normal_image, Size size, const string& click_effect)
{
	return create(normal_image, nullptr, nullptr, size, click_effect);
}

NButton* NButton::create(NScale9Sprite* normal_image, NScale9Sprite* selected_image, const string& click_effect)
{
	return create(normal_image, selected_image, normal_image->getContentSize(), click_effect);
}

NButton* NButton::create(NScale9Sprite* normal_image, NScale9Sprite* selected_image, Size size, const string& click_effect)
{
	return create(normal_image, selected_image, nullptr, size, click_effect);
}

NButton* NButton::create(NScale9Sprite* normal_image, NScale9Sprite* selected_image, NScale9Sprite* disabled_image, const string& click_effect)
{
	return create(normal_image, selected_image, disabled_image, normal_image->getContentSize(), click_effect);
}

NButton* NButton::create(NScale9Sprite* normal_image, NScale9Sprite* selected_image, NScale9Sprite* disabled_image, Size size, const string& click_effect)
{
	NButton* ret = new NButton();

	if (ret && ret->initWithScale9Sprite(normal_image, selected_image, disabled_image, size, click_effect))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

bool NButton::initWithFile(const string& normal_image, const string& selected_image, const string& disabled_image, const string& click_effect)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NWidget::init());

	this->setNormalImage(normal_image);
	this->setSelectedImage(selected_image);
	this->setDisableImage(disabled_image);
	this->setClickEffect(click_effect);

	INIT_DO_WHILE_END
}

bool NButton::initWithSprite(Sprite* normal_image, Sprite* selected_image, Sprite* disabled_image, const string& click_effect)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NWidget::init());

	this->setNormalImage(normal_image);
	this->setSelectedImage(selected_image);
	this->setDisableImage(disabled_image);
	this->setClickEffect(click_effect);

	INIT_DO_WHILE_END
}

bool NButton::initWithScale9Sprite(const string& normal_image, const string& selected_image, const string& disabled_image, Size size, const string& click_effect)
{
	NScale9Sprite* normal = NScale9Sprite::create(normal_image.c_str());
	NScale9Sprite* selected = nullptr;

	if (selected_image != "")
	{
		selected = NScale9Sprite::create(selected_image.c_str());
	}

	NScale9Sprite* disabled = nullptr;

	if (disabled_image != "")
	{
		disabled = NScale9Sprite::create(disabled_image.c_str());
	}

	return initWithScale9Sprite(normal, selected, disabled, size, click_effect);
}

bool NButton::initWithScale9Sprite(NScale9Sprite* normal_image, NScale9Sprite* selected_image, NScale9Sprite* disabled_image, Size size, const string& click_effect)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NWidget::init());

	CCAssert(normal_image != nullptr, "normal image is null");
	this->setScale9SpriteEnabled(true);

	if (! size.equals(Size(0, 0)))
	{
		normal_image->setContentSize(size);

		if (selected_image != nullptr)
		{
			selected_image->setContentSize(size);
		}

		if (disabled_image != nullptr)
		{
			disabled_image->setContentSize(size);
		}
	}

	this->setNormalImage(normal_image);
	this->setSelectedImage(selected_image);
	this->setDisableImage(disabled_image);
	this->setClickEffect(click_effect);

	INIT_DO_WHILE_END
}

void NButton::setNormalImage(const string& normal_image)
{
	CCAssert(normal_image != "", "normal image is null");

	Sprite* sp_normal_image = Sprite::create(normal_image.c_str());
	this->setNormalImage(sp_normal_image);
}

void NButton::setSelectedImage(const string& selected_image)
{
	if (selected_image == "")
	{
		return ;
	}

	Sprite* sp_selected_image = Sprite::create(selected_image.c_str());
	this->setSelectedImage(sp_selected_image);
}

void NButton::setDisableImage(const string& disable_image)
{
	if (disable_image == "")
	{
		return ;
	}

	Sprite* sp_disabled_image = Sprite::create(disable_image.c_str());
	this->setDisableImage(sp_disabled_image);
}

void NButton::setNormalImage(Node* normal_image)
{
	CCAssert(normal_image != nullptr, "normal image is null");

	if (_normal_image != nullptr)
	{
		_normal_image->removeFromParent();
		_normal_image = nullptr;
	}

	_normal_image = normal_image;
	this->addChild(_normal_image);
	this->setContentSize(_normal_image->getContentSize());
	_normal_image->setPosition(NODE_CENTER(this));
}

void NButton::setSelectedImage(Node* selected_image)
{
	if (selected_image == nullptr)
	{
		return;
	}

	if (_selected_image != nullptr)
	{
		_selected_image->removeFromParent();
		_selected_image = nullptr;
	}

	_selected_image = selected_image;
	this->addChild(_selected_image);
	_selected_image->setPosition(NODE_CENTER(this));
	_selected_image->setVisible(false);
}

void NButton::setDisableImage(Node* disabled_image)
{
	if (disabled_image == nullptr)
	{
		return;
	}

	if (_disable_image != nullptr)
	{
		_disable_image->removeFromParent();
		_disable_image = nullptr;
	}

	_disable_image = disabled_image;
	this->addChild(_disable_image);
	_disable_image->setPosition(NODE_CENTER(this));
	_disable_image->setVisible(false);
}

void NButton::setText(const string& str_text, const string& font_name, float font_size, const Color3B& normal_color, const Color3B& selected_color, const Color3B& disabled_color)
{
	this->setTextString(str_text);
	this->setTextFontName(font_name);
	this->setTextFontSize(font_size);
	this->setTextNormalColor(normal_color);
	this->setTextSelectedColor(selected_color);
	this->setTextDisabledColor(disabled_color);
}

void NButton::setTextLabel(LabelTTF* lbl_text)
{
	if (_lbl_text != nullptr)
	{
		_lbl_text->removeFromParent();
		_lbl_text = nullptr;
	}

	_lbl_text = lbl_text;
	this->addChild(_lbl_text);
	_lbl_text->setPosition(NODE_CENTER(this));
}

void NButton::setTextString(const string& str_text)
{
	if (_lbl_text != nullptr)
	{
		_lbl_text->setString(str_text.c_str());
	}
	else
	{
		_lbl_text = LabelTTF::create(str_text.c_str(), _text_font_name.c_str(), _text_font_size);
		this->addChild(_lbl_text);
		_lbl_text->setPosition(NODE_CENTER(this));
	}
}

void NButton::setTextFontName(const string& font_name)
{
	if (_lbl_text != nullptr)
	{
		_lbl_text->setFontName(font_name.c_str());
	}
}

void NButton::setTextFontSize(float font_size)
{
	if (_lbl_text != nullptr)
	{
		_lbl_text->setFontSize(font_size);
	}
}

void NButton::setTextNormalColor(const Color3B& normal_color)
{
	_text_normal_color = normal_color;

	if (this->getWidgetState() == Widget_State::kWidgetStateNormal)
	{
		this->updateWidget(Widget_State::kWidgetStateNormal);
	}
}

void NButton::setTextSelectedColor(const Color3B& selected_color)
{
	_text_selected_color = selected_color;

	if (this->getWidgetState() == Widget_State::kWidgetStateSelected)
	{
		this->updateWidget(Widget_State::kWidgetStateSelected);
	}
}

void NButton::setTextDisabledColor(const Color3B& disabled_color)
{
	_text_disabled_color = disabled_color;

	if (this->getWidgetState() == Widget_State::kWidgetStateDisabled)
	{
		this->updateWidget(Widget_State::kWidgetStateDisabled);
	}
}

LabelTTF* NButton::getTextLabel()
{
	if (_lbl_text != nullptr)
	{
		return _lbl_text;
	}
	else
	{
		return nullptr;
	}	
}

string NButton::getTextString() const
{
	return _str_lbl_text;
}

string NButton::getTextFontName() const
{
	return _text_font_name;
}

float NButton::getTextFontSize() const
{
	return _text_font_size;
}

Color3B NButton::getTextNormalColor() const
{
	return _text_normal_color;
}

Color3B NButton::getTextSelectedColor() const
{
	return _text_selected_color;
}

Color3B NButton::getTextDisabledColor() const
{
	return _text_disabled_color;
}

void NButton::setOriginalPosition(const Vec2& original_position)
{
	_original_position = original_position;
}

Vec2 NButton::getOriginalPosition() const
{
	return _original_position;
}

bool NButton::isScale9SpriteEnabled() const
{
	return _is_scale_9_sprite_enabled;
}

void NButton::setScale9SpriteEnabled(bool is_scale_9_sprite_enabled)
{
	_is_scale_9_sprite_enabled = is_scale_9_sprite_enabled;
}

void NButton::setPressScale(float press_scale)
{
	if (_press_scale != press_scale)
	{
		if (_press_action != nullptr)
		{
			_press_action->release();
			_press_action = nullptr;

			_release_action->release();
			_release_action = nullptr;
		}		

		_press_scale = press_scale;

		_press_action = ScaleTo::create(0.05f, _press_scale);
		_press_action->retain();

		_release_action = ScaleTo::create(0.05f, 1.0f);
		_release_action->retain();
	}	
}

float NButton::getPressScale() const
{
	return _press_scale;
}

void NButton::setTips(bool is_tips)
{
	if (_is_tips != is_tips)
	{
		_is_tips = is_tips;

		if (_is_tips)
		{
			SAFE_SET_VISIBLE(_tips_image, true);
		}
		else
		{
			SAFE_SET_VISIBLE(_tips_image, false);
		}
	}
}

bool NButton::isTips() const
{
	return _is_tips;
}

void NButton::setTipsImage(const string& tips_image)
{
	if (tips_image == "")
	{
		return ;
	}

	Sprite* sp_tips_image = Sprite::create(tips_image.c_str());
	this->setTipsImage(sp_tips_image);
}

void NButton::setTipsImage(Node* tips_image)
{
	if (tips_image == nullptr)
	{
		return ;
	}

	Vec2 pos = Vec2(this->getContentSize().width, this->getContentSize().height);
	Vec2 anchor = Vec2(0.5f, 0.5f);

	if (_tips_image != nullptr)
	{
		pos = _tips_image->getPosition();
		anchor = tips_image->getAnchorPoint();
		_tips_image->removeFromParent();
		_tips_image = nullptr;
	}

	_tips_image = tips_image;
	this->addChild(_tips_image, TIPS_IMAGE_ZORDER);
	_tips_image->setPosition(pos);
	_tips_image->setAnchorPoint(anchor);
}

Node* NButton::getTipsImage() const
{
	return _tips_image;
}

void NButton::setTipsImagePosition(const Vec2& position)
{
	if (_tips_image != nullptr)
	{
		_tips_image->setPosition(position);
	}
}

Vec2 NButton::getTipsImagePosition() const
{
	if (_tips_image != nullptr)
	{
		return _tips_image->getPosition();
	}

	return Vec2(0, 0);
}

void NButton::setTipsImageAnchorVec2(const Vec2& anchor_point)
{
	if (_tips_image != nullptr)
	{
		_tips_image->setAnchorPoint(anchor_point);
	}
}

Vec2 NButton::getTipsImageAnchorVec2() const
{
	if (_tips_image != nullptr)
	{
		return _tips_image->getAnchorPoint();
	}

	return Vec2(0, 0);
}

void NButton::updateWidget(const Widget_State& widget_state)
{
	switch (widget_state)
	{
	case Widget_State::kWidgetStateNormal:
		{
			SAFE_SET_VISIBLE(_normal_image, true);
			SAFE_SET_VISIBLE(_selected_image, false);
			SAFE_SET_VISIBLE(_disable_image, false);

			if(_lbl_text != nullptr)
			{
				_lbl_text->setColor(_text_normal_color);
			}

			if (_release_action != nullptr)
			{
				this->stopAllActions();
				this->runAction(_release_action);
			}
		}
		break;
	case Widget_State::kWidgetStateSelected:
		{
			if(_selected_image != nullptr)
			{
				SAFE_SET_VISIBLE(_normal_image, false);
				SAFE_SET_VISIBLE(_selected_image, true);
				SAFE_SET_VISIBLE(_disable_image, false);
			}
			else
			{
				SAFE_SET_VISIBLE(_normal_image, true);
				SAFE_SET_VISIBLE(_selected_image, false);
				SAFE_SET_VISIBLE(_disable_image, false);
			}

			if (_lbl_text != nullptr)
			{
				_lbl_text->setColor(_text_selected_color);
			}

			if (_press_action != nullptr)
			{
				this->stopAllActions();
				this->runAction(_press_action);
			}
		}
		break;
	case Widget_State::kWidgetStateDisabled:
		{
			if (_disable_image != nullptr)
			{
				SAFE_SET_VISIBLE(_normal_image, false);
				SAFE_SET_VISIBLE(_selected_image, false);
				SAFE_SET_VISIBLE(_disable_image, true);
			}
			else
			{
				SAFE_SET_VISIBLE(_normal_image, true);
				SAFE_SET_VISIBLE(_selected_image, false);
				SAFE_SET_VISIBLE(_disable_image, false);
			}

			if (_lbl_text != nullptr)
			{
				_lbl_text->setColor(_text_disabled_color);
			}
		}
		break;
	default:
		break;
	}
}

bool NButton::hitWidget(const Vec2& point)
{
	Vec2 pt = this->convertToNodeSpace(point);

	if (this->getScale() < 1.0f)
	{
		pt.x = pt.x * this->getScaleX();
		pt.y = pt.y * this->getScaleY();
	}

	Rect rect = Rect(0, 0, this->getContentSize().width, this->getContentSize().height);

	if (rect.containsPoint(pt))
	{
		return true;
	}
	return false;
}