#ifndef __NXML_MANAGER_H__
#define __NXML_MANAGER_H__

#include "NGUI/NProtocol.h"
//#include "tinyxml2/tinyxml2.h"

class NXMLManager
{
public:
	NXMLManager(void);
	~NXMLManager(void);

	static NXMLManager* getInstance();
	static void destroyInstance();

	/*void transferFileToWritablePath(const string& sourece_file_name, const string& destination_file_name);
	bool createXML(const string& file_name);
	bool isExistXML(const string& file_name, bool is_create = true);

	string getNodeValue(const string& file_name, const string& key);
	void getNodeChildrenValue(const string& file_name, const string& key, unordered_map<string, string>& children_value);
	void getNodeChildrenValue(const string& file_name, const string& key, map<string, string>& children_value);
	string getNodeAttribute(const string& file_name, const string& key);
	void getNodeAttributes(const string&file_name, const string& key, unordered_map<string, string>& attributes);
	void getNodeAttributes(const string&file_name, const string& key, map<string, string>& attributes);

	void setNodeValue(const string& file_name, const string& key, const string& value);
	void setNodeAttribute(const string& file_name, const string& key, const string& value);

	void addNodeValue(const string& file_name, const string& key, const string& value);
	void addNodeAttribute(const string& file_name, const string& key, const string& value);
	void addNodeAttibutes(const string& file_name, const string& key, unordered_map<string, string>& attributes);
	void addNodeAttibutes(const string& file_name, const string& key, map<string, string> attributes);

	void deleteNodeValue(const string& file_name, const string& key);
	void deleteNodeAttribute(const string& file_name, const string& key);
	void deleteNodeAttributes(const string& file_name, const string& key, unordered_map<string, string> attributes);
	void deleteNodeAttributes(const string& file_name, const string& key, map<string, string> attributes);*/

private:
	//bool visitXMLNode(tinyxml2::XMLElement* root, tinyxml2::XMLElement* &node, int count);
	//bool visitXMLNodeValue(tinyxml2::XMLElement* root, tinyxml2::XMLElement* &node, int count);
	void splitKey(const string& key, const string& separator = "/");

private:
	vector<string> _keys;
	static NXMLManager* _instance;
};

#endif // __NXML_MANAGER_H__