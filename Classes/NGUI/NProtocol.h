#ifndef __NPROTOCOL_H__
#define __NPROTOCOL_H__

#include <deque>
#include <vector>
#include <string>
#include <set>
#include <map>
#include <unordered_map>
#include <list>
#include <stack>
#include <mutex>
#include <thread>
#include <queue>
#include <deque>
#include "cocos2d.h"
#include "NGUI/NMacro.h"
#include "NGUI/NAudioManager.h"

USING_NS_CC;

using std::deque;
using std::vector;
using std::string;
using std::set;
using std::map;
using std::unordered_map;
using std::list;
using std::stack;
using std::queue;
using std::deque;

enum class Touch_Period
{
	kTouchBegan = 0,
	kTouchMoved,
	kTouchEnded,
	kTouchCancelled
};

enum class Widget_State
{
	kWidgetStateNormal = 0,
	kWidgetStateSelected,
	kWidgetStateDisabled
};

enum class Tips_Type
{
	kAddTip = 1,
	kRemoveTip = -1
};

enum class Text_Direction
{
	kText_Dir_Vertical,
	kText_Dir_Horizontal
};

enum class ScrollView_Direction
{
	kScrollView_Dir_Vertical,
	kScrollView_Dir_Horizontal,
	kScrollView_Dir_Both
};

enum class ListView_Align_Type
{
	kListView_Align_Vertical_Left = 0,
	kListView_Align_Vertical_Right,
	kListView_Align_Vertical_Center,
	kListView_Aligan_Horizontal_Top,
	kListView_Aligan_Horizontal_Bottom,
	kListView_Aligan_Horizontal_Center,
};

enum class Bar_Direction
{
	kBar_Dir_LeftToRight = 1,
	kBar_Dir_RightToLeft,
	kBar_Dir_TopToButtom,
	kBar_Dir_ButtomToTop
};

enum class Bar_Period
{
	kBar_Began,
	kBar_Move,
	kBar_End
};

enum class Switch_State
{
	kSwitch_Off = 0,
	kSwitch_On = 1
};

typedef std::function<void(Ref*, Touch_Period)> Touch_Callback;
typedef std::function<void(Ref*, Bar_Period)> Bar_Callback;
typedef std::function<Ref*(Ref*, int)> Load_Callback;
typedef std::function<void(Ref*, int)> PageChanged_Callback;

// Widget Unique ID
#define NLAYOUT "nlayout"
#define NWIDGET "nwidget"
#define NBUTTON "nbutton"
#define NCHECK_BOX "ncheck_box"
#define NSCROLL_VIEW "nscroll_view"
#define NSCROLL_VIEW_CONTAINER "nscroll_view_container"
#define NLIST_VIEW "nlist_view"
#define NRICH_TEXT "nrich_text"
#define NBAR "nbar"
#define NPROGRESS_BAR "nprogress_bar"
#define NSLIDER_BAR "nslider_bar"
#define NSCROLL_BAR "nscroll_bar"
#define NLABEL "nlabel"
#define NTABLE_VIEW "ntable_view"
#define NTABLE_VIEW_CELL "ntable_view_cell"
#define NPAGE_VIEW "npage_view"
#define NPAGE_VIEW_CELL "npage_view_cell"
#define NGRID_VIEW "ngrid_view"
#define NGRID_VIEW_CELL "ngrid_view_cell"
#define NJOYSTICK "njoystick"
#define NSWITCH_BUTTON "nswitch_button"

static unsigned char utf8_look_for_table[] = 
{
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1, 1,
	2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
	2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2, 2,
	3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3, 3,
	4, 4, 4, 4, 4, 4, 4, 4, 5, 5, 5, 5, 6, 6, 1, 1
};

#define UTF8_LENGTH(_X_) utf8_look_for_table[(_X_)]

static int str_utf8_char_len(unsigned char ch)
{
	return (int)(UTF8_LENGTH((ch)));
}

#endif // __NPROTOCOL_H__
