#ifndef __NLIST_VIEW_H__
#define __NLIST_VIEW_H__

#include "NGUI/NProtocol.h"
#include "NGUI/NScrollView.h"

class NListView : public NScrollView
{
public:
	NListView(void);
	~NListView(void);

	static NListView* create(Size view_size);
	virtual bool init(Size view_size);

	virtual void setDirection(const ScrollView_Direction& scroll_view_direction);

	void setAlignType(const ListView_Align_Type& list_view_align_type);
	ListView_Align_Type getAlignType() const;

	void insertNodeAtLast(Node* node);
	void insertNodeAtFront(Node* node);
	void insertNode(Node* node, int index);

	void removeNodeAtIndex(int index);
	void removeNode(Node* node);
	void removeFrontNode();
	void removeLastNode();
	void removeAllNodes();

	vector<Node*>* getNodes();
	Node* getNodeAtIndex(int index);
	int getNodesCount() const;

	bool isEmptyListNodes();

	void reloadData();

protected:
	void updateNodesPosition();
	void relocateContainer();

private:
	vector<Node*>* _nodes;
	ListView_Align_Type _list_view_align_type;
};

#endif // __NLIST_VIEW_H__
