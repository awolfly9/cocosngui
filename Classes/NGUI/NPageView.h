#ifndef __NPAGE_VIEW_H__
#define __NPAGE_VIEW_H__

#include "NGUI/NTableView.h"

class NPageViewCell : public NTableViewCell
{
public:
	NPageViewCell(void);
	~NPageViewCell(void);

	CREATE_FUNC(NPageViewCell);
};

class NPageView : public NTableView
{
public:
	NPageView(void);
	~NPageView(void);

	static NPageView* create(const Size& view_size, const Size& cell_size, int cell_count, const Load_Callback& load_callback);
	virtual bool init(const Size& view_size, const Size& cell_size, int cell_count, const Load_Callback& load_cell);

	void setPageChangedCallback(const PageChanged_Callback& page_changed_callback);
	PageChanged_Callback getPageChangedCallback() const;

	int getPageIndex();

	void setShowTipsLabel(bool is_show_tips_label);
	bool isShowTipsLabel() const;

	void setTipsLabelDistance(float distance);
	float getTipsLabelDistance() const;

	void setTipsLabelPositionPercent(float percent);
	float getTipsLabelPositionPercent() const;

	void setTipsLabelFileName(const string& tips_label_fore_file_name, const string& tips_label_back_file_name);

public: 
	virtual void setDirection(const ScrollView_Direction& scroll_view_direction);

protected:
	virtual void onScrolling();
	virtual void loadCellWithIndex(int index);

	virtual void updateTipsLabel();

private:
	PageChanged_Callback _page_changed_callback;

	int _page_index;

	bool _is_show_tips_label;
	float _tips_label_distance;
	float _tips_label_position_percent;

	string _tips_label_fore_file_name;
	string _tips_label_back_file_name;

	Node* _tips_label_fore;
	Node* _tips_label;

	vector<Vec2> _tips_label_position;
};

#endif // __NPAGE_VIEW_H__
