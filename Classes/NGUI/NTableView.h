#ifndef __NTABLE_VIEW__H__
#define __NTABLE_VIEW__H__

#include "NGUI/NScrollView.h"

class NTableViewCell : public NWidget
{
public:
	NTableViewCell();
	~NTableViewCell();

	CREATE_FUNC(NTableViewCell);
	virtual bool init();

	void setIndex(int index);
	int getIndex() const;

private:
	int _index;
};

class NTableView : public NScrollView
{
public:
	NTableView(void);
	~NTableView(void);

	static NTableView* create(const Size& view_size, const Size& cell_size, int cell_count, const Load_Callback& load_callback);
	virtual bool init(const Size& view_size, const Size& cell_size, int cell_count, const Load_Callback& load_cell);

	void setCellSize(const Size& cell_size);
	Size getCellSize() const;

	void setCellCount(int cell_count);
	int getCellCount() const;

	void setAutoRelocate(bool is_auto_relocate);
	bool isAutoRelocate() const;

	void setLoadCallback(const Load_Callback& load_callback);
	Load_Callback getLoadCallback() const;

	list<NTableViewCell*>* getCells();
	NTableViewCell* getCellAtIndex(int index);

	virtual void reloadData();

public:
	virtual void setDirection(const ScrollView_Direction& scroll_view_direction);

protected:
	virtual void onScrolling();
	virtual void onDraggingEnd();
	virtual void onScrollingEnd();
	virtual void onBouncingEnd();

	virtual void updatePosition();
	virtual void loadCellWithIndex(int index);

	virtual int getOriginIndex(const Vec2& point);
	virtual int getEndIndex(const Vec2& point);

protected:
	Load_Callback _load_callback;

	Size _cell_size;
	int _cell_count;
	bool _is_auto_relocate;	

	vector<Vec2> _position;
	set<int> _indexs;
	list<NTableViewCell*>* _cells;	
};

#endif // __NTABLE_VIEW__H__
