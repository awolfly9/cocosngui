#ifndef __NAUDIO_MANAGER_H__
#define __NAUDIO_MANAGER_H__

#include "NGUI/NProtocol.h"
#include "SimpleAudioEngine.h"
#include <vector>
#include <string>
#include <queue>
#include <deque>

using namespace CocosDenshion;
using std::string;
using std::vector;
using std::queue;
using std::deque;

class AsyncBgMusic
{
public:
	AsyncBgMusic(const string& file_path, const std::function<void(string)>& callback)
		: _file_path(file_path)
		, _callback(callback)
	{
	}

	~AsyncBgMusic()
	{
	};

	string _file_path;
	std::function<void(string)> _callback;
};

class BgMusicInfo
{
public:
	BgMusicInfo()
		: _async_bg_music(nullptr)
	{
	}

	~BgMusicInfo()
	{
	}

	AsyncBgMusic* _async_bg_music;
};

class AsyncEffect
{
public:
	AsyncEffect(const string& file_path, const std::function<void(string)>& callback)
		: _file_path(file_path)
		, _callback(callback)
	{
	};

	~AsyncEffect() 
	{
	};

	string _file_path;
	std::function<void(string)> _callback;
};

class EffectInfo
{
public:
	EffectInfo()
		: _async_effect(nullptr)
	{
	}

	~EffectInfo()
	{
	}

	AsyncEffect* _async_effect;
};

/*
 *	新增了多线程加载音效和背景音乐
 * 可是看了源码发现在 SimpleAudioEngine 中预加载背景音乐是空的，也就是没有任何实现
 * 所以多此一举了
 */
class NAudioManager
{
public:
	NAudioManager(void);
	~NAudioManager(void);

	static NAudioManager* getInstance();
	static void destroyInstance();

	void end();

    void preloadBgMusic(const string& file_path);
    void preloadBgMusic(vector<string>* file_path);

	//void preloadBgMusicAsync(const string& file_path, const std::function<void(const string& file_path)>& callback);

    void playBgMusic(const string& file_path, bool is_loop = true);   
    void stopBgMusic(bool is_release_data = false);
    void pauseBgMusic();

    void resumeBgMusic();
    void rewindBgMusic();
   
    bool willPlayBgMusic();
    bool isBgMusicPlaying();

    float getBgMusicVolume();
    void setBgMusicVolume(float volume);

    float getEffectsVolume();
    void setEffectsVolume(float volume);

    unsigned int playEffect(const string& file_path, bool is_loop = false);  
    void pauseEffect(unsigned int sound_id);
    void pauseAllEffects();

    void resumeEffect(unsigned int sound_id);
    void resumeAllEffects();

    void stopEffect(unsigned int sound_id);
    void stopAllEffects();

    void preloadEffect(const string& file_path);
	void preloadEffect(vector<string> file_path);
    void unloadEffect(const string& file_path);

	void preloadEffectAsync(const string& file_path, const std::function<void(const string& file_path)>& callback);

	void setPlayBgMusic(bool is_play_bg_music);
	bool isPlayBgMusic();

	void setPlayClickEffect(bool is_play_click_effect);
	bool isPlayClickEffect();

	void waitForBgMusicQuit();
	void waitForEffectQuit();

private:
	//void loadBgMusic();
	void loadEffect();
	
	//void loadBgAsyncCallback(float dt);
	void loadEffectAsyncCallback(float dt);

private:
	bool _is_play_bg_music;
	bool _is_play_click_effect;
	
	queue<AsyncBgMusic*>* _async_bg_music_queue;
	queue<AsyncEffect*>* _async_effect_queue;

	deque<BgMusicInfo*>* _bg_music_info_deque;
	deque<EffectInfo*>* _effect_info_deque;

	std::mutex _async_bg_music_mutex;
	std::mutex _async_effect_mutex;

	std::mutex _bg_music_info_mutex;
	std::mutex _effect_info_mutex;

	std::thread* _loading_bg;
	std::thread* _loading_effect;

	std::mutex _sleep_mutex_bg_music;
	std::mutex _sleep_mutex_effect;

	std::condition_variable _sleep_bg_music_condition;
	std::condition_variable _sleep_effect_condition;

	bool _need_bg_quit;
	bool _need_effect_quit;

	int _async_bg_music_count;
	int _async_effect_count;

	static NAudioManager* _instance;
};

#endif // __NAUDIO_MANAGER_H__
