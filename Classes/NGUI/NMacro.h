#ifndef __NMACRO_H__
#define __NMACRO_H__


// macro
#define ORIGINAL_POINT				Director::getInstance()->getVisibleOrigin()
#define WINSIZE                           Director::getInstance()->getWinSize()
#define WINSIZE_CENTER              Vec2(WINSIZE.width / 2, WINSIZE.height / 2)
#define VISIBLE_SIZE                     Director::getInstance()->getVisibleSize()
#define VISIBLE_SIZE_CENTER        Vec2(VISIBLE_SIZE.width / 2 + ORIGINAL_POINT.x, VISIBLE_SIZE.height / 2 + ORIGINAL_POINT.y)
#define POS_BY_NODE(__node__, __x_factor__, __y_factor__)    Vec2((__node__)->getContentSize().width * (__x_factor__), (__node__)->getContentSize().height * (__y_factor__))
#define POS_BY_VISIBLE_SIZE(__x_factor__, __y_factor__)			Vec2(VISIBLE_SIZE.width * (__x_factor__) + ORIGINAL_POINT.x, VISIBLE_SIZE.height * (__y_factor__) + ORIGINAL_POINT.y)
#define NODE_CENTER(__node__)	                                    Vec2((__node__)->getContentSize().width / 2, (__node__)->getContentSize().height / 2)

#define TEXTURE_CACHE					TextureCache::getInstance()
#define SPRITEFRAME_CACHE				SpriteFrameCache::getInstance()
#define ANIMATION_CAHCE				CCAnimationCache::getInstance()
#define DIRECTOR								Director::getInstance()
#define USER_DEFAULT						UserDefault::getInstance()
#define NOTIFICATIONCENTER			NotificationCenter::getInstance()
#define SIMPLE_AUDIO_ENGINE			SimpleAudioEngine::getInstance()

#define AUDIO_MANAGER           NAudioManager::getInstance()
#define MESSAGE_TIPS                NMessageTips::getInstance()


#define DESTORY_ALL_INSTANCE  {  \
	NAudioManager::destroyInstance();  \
	NMessageTips::destroyInstance();  \
	NXMLManager::destroyInstance();  \
}

// init 
#define INIT_DO_WHILE_BEGIN          bool bRet = false;	\
	do			\
{

#define INIT_DO_WHILE_END				bRet = true;	\
} while (0);		\
	return bRet;

// safe
#define SAFE_SET_VISIBLE(__Target__, __Visible__)        do { if ((__Target__) != nullptr) { (__Target__)->setVisible(__Visible__); } }while(0)
#define SAFE_RETAIN(__Target__)                               do { if ((__Target__) != nullptr) { (__Target__)->retain(); } } while(0)
#define SAFE_RELEASE_NULL(__Target__)                     do { if ((__Target__) != nullptr) { (__Target__)->release(); __Target__ = nullptr; } } while(0)
#define SAFE_DELETE_NULL(__Target__)                       do { if((__Target__) != nullptr) { delete (__Target__); __Target__ = nullptr; } } while(0)
#define BREAK_IF(cond)                                             if(cond) break


// C++ 11 Touch_Callback
#define CALLBACK_0(__selector__,__target__, ...) std::bind(&__selector__,__target__, ##__VA_ARGS__)
#define CALLBACK_1(__selector__,__target__, ...) std::bind(&__selector__,__target__, std::placeholders::_1, ##__VA_ARGS__)
#define CALLBACK_2(__selector__,__target__, ...) std::bind(&__selector__,__target__, std::placeholders::_1,  std::placeholders::_2, ##__VA_ARGS__)
#define CALLBACK_3(__selector__,__target__, ...) std::bind(&__selector__,__target__, std::placeholders::_1,  std::placeholders::_2, std::placeholders::_3, ##__VA_ARGS__)












#endif // __NMACRO_H__
