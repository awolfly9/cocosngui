#include "NTableView.h"

NTableViewCell::NTableViewCell()
	: _index(-1)
{
	this->setUniqueID(NTABLE_VIEW_CELL);
}

NTableViewCell::~NTableViewCell()
{
}

bool NTableViewCell::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NWidget::init());

	INIT_DO_WHILE_END
}

void NTableViewCell::setIndex(int index)
{
	_index = index;
}

int NTableViewCell::getIndex() const
{
	return _index;
}

NTableView::NTableView(void)
	: _cell_size(Vec2(0, 0))
	, _cell_count(0)
	, _load_callback(nullptr)
	, _is_auto_relocate(false)
{
	this->setUniqueID(NTABLE_VIEW);

	_cells = new list<NTableViewCell*>();
}

NTableView::~NTableView(void)
{
	delete _cells;
}

NTableView* NTableView::create(const Size& view_size, const Size& cell_size, int cell_count, const Load_Callback& load_callback)
{
	NTableView* ret = new NTableView();

	if (ret && ret->init(view_size, cell_size, cell_count, load_callback))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

bool NTableView::init(const Size& view_size, const Size& cell_size, int cell_count, const Load_Callback& load_callback)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NScrollView::init(view_size));

	_cell_size = cell_size;
	_cell_count = cell_count;
	_load_callback = load_callback;

	INIT_DO_WHILE_END
}

void NTableView::setDirection(const ScrollView_Direction& scroll_view_direction)
{
	if (scroll_view_direction != ScrollView_Direction::kScrollView_Dir_Both)
	{
		if (this->getDirection() != scroll_view_direction)
		{
			NScrollView::setDirection(scroll_view_direction);
		}		
	}
}

void NTableView::setCellSize(const Size& cell_size)
{
	_cell_size = cell_size;
}

Size NTableView::getCellSize() const
{
	return _cell_size;
}

void NTableView::setCellCount(int cell_count)
{
	_cell_count = cell_count;
}

int NTableView::getCellCount() const
{
	return _cell_count;
}

void NTableView::setAutoRelocate(bool is_auto_relocate)
{
	_is_auto_relocate = is_auto_relocate;
}

bool NTableView::isAutoRelocate() const
{
	return _is_auto_relocate;
}

void NTableView::setLoadCallback(const Load_Callback& load_callback)
{
	_load_callback = load_callback;
}
Load_Callback NTableView::getLoadCallback() const
{
	return _load_callback;
}

list<NTableViewCell*>* NTableView::getCells()
{
	return _cells;
}

NTableViewCell* NTableView::getCellAtIndex(int index)
{
	for (list<NTableViewCell*>::iterator iter = _cells->begin(); iter != _cells->end(); ++ iter)
	{
		NTableViewCell* cell = (NTableViewCell*)(*iter);

		if (cell->getIndex() == index)
		{
			return cell;
		}
	}

	return nullptr;
}

void NTableView::reloadData()
{
	for (list<NTableViewCell*>::iterator iter = _cells->begin(); iter != _cells->end(); ++ iter)
	{
		NTableViewCell* cell = (NTableViewCell*)(*iter);
		cell->removeFromParent();
	}

	_cells->clear();
	_position.clear();
	_indexs.clear();

	this->updatePosition();	
	this->onScrolling();

	if (_direction == ScrollView_Direction::kScrollView_Dir_Horizontal)
	{
		this->setContainerPosition(Vec2(0, 0));
	}
	else
	{
		this->setContainerPosition(Vec2(0, this->getContentSize().height - this->getContainerSize().height));
	}
}

void NTableView::onScrolling()
{
	int origin = this->getOriginIndex(this->getContainerPosition());
	int end = this->getEndIndex(this->getContainerPosition());

	while (! _cells->empty())
	{
		if (origin > (int)((NTableViewCell*)_cells->front()->getIndex()))
		{
			NTableViewCell* cell = (NTableViewCell*)_cells->front();
			_indexs.erase(cell->getIndex());
			cell->removeFromParent();			
			_cells->pop_front();
		}
		else
		{
			break;
		}
	}

	while (! _cells->empty())
	{
		if (end < (int)((NTableViewCell*)_cells->back()->getIndex()))
		{
			NTableViewCell* cell = (NTableViewCell*)_cells->back();
			_indexs.erase(cell->getIndex());
			cell->removeFromParent();			
			_cells->pop_back();
		}
		else
		{
			break;
		}
	}

	for (int i = origin; i <= end; ++ i)
	{
		if (_indexs.find(i) == _indexs.end())
		{
			this->loadCellWithIndex(i);
		}
	}
}

void NTableView::onDraggingEnd()
{
	if (! _is_auto_scrolling && ! _is_bouncing)
	{
		this->onScrollingEnd();
	}
}

void NTableView::onScrollingEnd()
{
	if (! _is_bouncing && _is_auto_relocate)
	{
		int origin = this->getOriginIndex(this->getContainerPosition());

		switch (_direction)
		{
		case ScrollView_Direction::kScrollView_Dir_Vertical:
			{
				Vec2 origin_pos = Vec2(this->getContainerPosition().x, origin * _cell_size.height);
				float offset = (this->getContainerSize().height - origin_pos.y - this->getContentSize().height - (- this->getContainerPosition().y));

				if (offset <= _cell_size.height / 2)
				{
					this->startAutoBounce(Vec2(this->getContainerPosition().x, this->getContainerPosition().y - offset));
				}
				else
				{
					this->startAutoBounce(Vec2(this->getContainerPosition().x, this->getContainerPosition().y + (_cell_size.height - offset)));
				}
			}
			break;
		case ScrollView_Direction::kScrollView_Dir_Horizontal:
			{
				Vec2 origin_pos = Vec2(origin * _cell_size.width, this->getContainerPosition().y);
				float offset = - (origin_pos.x + this->getContainerPosition().x);

				if (offset <= _cell_size.width / 2)
				{
					this->startAutoBounce(Vec2(-origin_pos.x, origin_pos.y));
				}
				else
				{
					this->startAutoBounce(Vec2((- (origin_pos.x + _cell_size.width)), origin_pos.y));
				}
			}
			break;
		default:
			break;
		}
	}
}

void NTableView::onBouncingEnd()
{

}

void NTableView::updatePosition()
{
	if (_cell_count <= 0)
	{
		return ;
	}

	switch (this->getDirection())
	{
	case ScrollView_Direction::kScrollView_Dir_Vertical:
		{
			float total_height = _cell_size.height * _cell_count;
			this->setContainerSize(Size(this->getContentSize().width, total_height));

			for (int i = 0; i != _cell_count; i ++)
			{
				float height = _cell_size.height * 0.5 + _cell_size.height * i;

				_position.push_back(Vec2(this->getContainerSize().width / 2, this->getContainerSize().height - height));
			}
		}
		break;
	case ScrollView_Direction::kScrollView_Dir_Horizontal:
		{
			float total_width = _cell_size.width * _cell_count;
			this->setContainerSize(Size(total_width, this->getContainerSize().height));

			for (int i = 0; i != _cell_count; i ++)
			{
				float width = _cell_size.width * 0.5 + _cell_size.width * i;

				_position.push_back(Vec2(width, this->getContainerSize().height / 2));
			}
		}
		break;
	default:
		break;
	}
}

int NTableView::getOriginIndex(const Vec2& point)
{
	int index = 0;

	switch (this->getDirection())
	{
	case ScrollView_Direction::kScrollView_Dir_Vertical:
		{
			index = (this->getContainerSize().height - (- point.y + this->getContentSize().height)) / _cell_size.height;
		}
		break;
	case ScrollView_Direction::kScrollView_Dir_Horizontal:
		{
			index = (- point.x) / _cell_size.width;
		}
		break;
	default:
		break;
	}

	index = MAX(index, 0);
	index = MIN(index, _cell_count - 1);

	return index;
}

int NTableView::getEndIndex(const Vec2& point)
{
	int index = 0;

	switch (this->getDirection())
	{
	case ScrollView_Direction::kScrollView_Dir_Vertical:
		{
			index =  (this->getContainerSize().height - (- point.y)) / _cell_size.height;
		}
		break;
	case ScrollView_Direction::kScrollView_Dir_Horizontal:
		{
			index = (- point.x + this->getContentSize().width) / _cell_size.width;
		}
		break;
	default:
		break;
	}

	index = MAX(index, 0);
	index = MIN(index, _cell_count - 1);

	return index;
}

void NTableView::loadCellWithIndex(int index)
{
	NTableViewCell* cell = (NTableViewCell*)_load_callback(this, index);

	if (cell == nullptr)
	{
		return ;
	}

	this->addChild(cell);
	cell->setPosition(_position.at(index));
	cell->setIndex(index);

	_indexs.insert(index);

	if (_cells->empty())
	{
		_cells->push_back(cell);
	}
	else
	{
		bool is_insert = false;
		for (list<NTableViewCell*>::iterator iter = _cells->begin(); iter != _cells->end(); ++ iter)
		{
			NTableViewCell* pCell = (NTableViewCell*)(*iter);

			if (pCell == nullptr)
			{
				continue;
			}

			if (pCell->getIndex() > index)
			{
				_cells->insert(iter, cell);
				is_insert = true;
				break;
			}
		}

		if (! is_insert)
		{
			_cells->push_back(cell);
		}
	}
}