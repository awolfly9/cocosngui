#include "NCheckBox.h"

NCheckBox::NCheckBox(void)
	: _check_image(nullptr)
	, _check_selected_image(nullptr)
	, _check_disable_image(nullptr)
	, _text_check_color(Color3B(255, 255, 255))
	, _text_check_selected_color(Color3B(255, 255, 0))
	, _is_check(false)
	, _is_hide_normal_image(true)
{
	this->setUniqueID(NCHECK_BOX);
}

NCheckBox::~NCheckBox(void)
{
}

NCheckBox* NCheckBox::create(const string& click_effect)
{
	NCheckBox* ret = new NCheckBox();

	if (ret && ret->init(click_effect))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

bool NCheckBox::init(const string& click_effect)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NButton::init());

	this->setClickEffect(click_effect);

	INIT_DO_WHILE_END
}

void NCheckBox::setCheckImage(const string& check_image)
{
	if (check_image == "")
	{
		return;
	}

	Sprite* sp_check_image = Sprite::create(check_image.c_str());
	this->setCheckImage(sp_check_image);
}

void NCheckBox::setCheckSelectedImage(const string& check_selected_image)
{
	if (check_selected_image == "")
	{
		return;
	}

	Sprite* sp_check_selected_image = Sprite::create(check_selected_image.c_str());
	this->setCheckSelectedImage(sp_check_selected_image);
}

void NCheckBox::setCheckDisableImage(const string& check_disable_image)
{
	if (check_disable_image == "")
	{
		return ;
	}

	Sprite* sp_check_disable_image = Sprite::create(check_disable_image.c_str());
	this->setCheckDisableImage(sp_check_disable_image);
}

void NCheckBox::setCheckImage(Node* check_image)
{
	if (check_image == nullptr)
	{
		return ;
	}

	if (_check_image != nullptr)
	{
		_check_image->removeFromParent();
		_check_image = nullptr;
	}

	_check_image = check_image;
	this->addChild(_check_image);
	_check_image->setPosition(NODE_CENTER(this));
	_check_image->setVisible(false);
}

void NCheckBox::setCheckSelectedImage(Node* check_selected_image)
{
	if (check_selected_image == nullptr)
	{
		return ;
	}

	if (_check_selected_image != nullptr)
	{
		_check_selected_image->removeFromParent();
		_check_selected_image = nullptr;
	}

	_check_selected_image = check_selected_image;
	this->addChild(_check_selected_image);
	_check_selected_image->setPosition(NODE_CENTER(this));
	_check_selected_image->setVisible(false);
}

void NCheckBox::setCheckDisableImage(Node* check_disable_image)
{
	if (check_disable_image == nullptr)
	{
		return ;
	}

	if (_check_disable_image != nullptr)
	{
		_check_disable_image->removeFromParent();
		_check_disable_image = nullptr;
	}

	_check_disable_image = check_disable_image;
	this->addChild(_check_disable_image);
	_check_disable_image->setPosition(NODE_CENTER(this));
	_check_disable_image->setVisible(false);
}

bool NCheckBox::isChecked() const
{
	return _is_check;
}

void NCheckBox::setChecked(bool is_check)
{
	if (_is_check != is_check)
	{
		_is_check = is_check;
		this->updateWidget(this->getWidgetState());
	}
}

void NCheckBox::setTextCheckColor(const Color3B& check_color)
{
	_text_check_color = check_color;

	if (this->isChecked() && this->getWidgetState() == Widget_State::kWidgetStateNormal)
	{
		this->updateWidget(Widget_State::kWidgetStateNormal);
	}
}

Color3B NCheckBox::getTextCheckColor() const
{
	return _text_check_color;
}

void NCheckBox::setTextCheckSelectedColor(const Color3B& check_selected_color)
{
	_text_check_selected_color = check_selected_color;

	if (this->isChecked() && this->getWidgetState() == Widget_State::kWidgetStateSelected)
	{
		this->updateWidget(Widget_State::kWidgetStateSelected);
	}
}

Color3B NCheckBox::getTextCheckSelectedColor() const
{
	return _text_check_selected_color;
}

void NCheckBox::setHideNormalImage(bool is_hide_normal_image)
{
	if (_is_hide_normal_image != is_hide_normal_image)
	{
		_is_hide_normal_image = is_hide_normal_image;
		this->updateWidget(this->getWidgetState());
	}
}

bool NCheckBox::isHideNormalImage() const
{
	return _is_hide_normal_image;
}

void NCheckBox::activate(const Touch_Period& touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		if (this->isChecked())
		{
			this->setChecked(false);
		}
		else
		{
			this->setChecked(true);
		}
	}	

	NButton::activate(touch_period);	
}

void NCheckBox::updateWidget(const Widget_State& widget_state)
{
	switch (widget_state)
	{
	case Widget_State::kWidgetStateNormal:
		{
			if (this->isChecked())
			{
				if (_check_image != nullptr)
				{
					SAFE_SET_VISIBLE(_normal_image, false);
					SAFE_SET_VISIBLE(_selected_image, false);
					SAFE_SET_VISIBLE(_check_image, true);
					SAFE_SET_VISIBLE(_check_selected_image, false);
					SAFE_SET_VISIBLE(_disable_image, false);
					SAFE_SET_VISIBLE(_check_disable_image, false);
				}
				else
				{
					SAFE_SET_VISIBLE(_normal_image, true);
					SAFE_SET_VISIBLE(_selected_image, false);
					SAFE_SET_VISIBLE(_check_image, false);
					SAFE_SET_VISIBLE(_check_selected_image, false);
					SAFE_SET_VISIBLE(_disable_image, false);
					SAFE_SET_VISIBLE(_check_disable_image, false);
				}

				if (_lbl_text != nullptr)
				{
					_lbl_text->setColor(_text_check_color);
				}
			}
			else
			{
				SAFE_SET_VISIBLE(_normal_image, true);
				SAFE_SET_VISIBLE(_selected_image, false);
				SAFE_SET_VISIBLE(_check_image, false);
				SAFE_SET_VISIBLE(_check_selected_image, false);
				SAFE_SET_VISIBLE(_disable_image, false);
				SAFE_SET_VISIBLE(_check_disable_image, false);

				if (_lbl_text != nullptr)
				{
					_lbl_text->setColor(_text_normal_color);
				}
			}

			if (_release_action != nullptr)
			{
				this->stopAllActions();
				this->runAction(_release_action);
			}
		}
		break;
	case Widget_State::kWidgetStateSelected:
		{
			if (this->isChecked())
			{
				if (_check_selected_image != nullptr)
				{
					SAFE_SET_VISIBLE(_normal_image, false);
					SAFE_SET_VISIBLE(_selected_image, false);
					SAFE_SET_VISIBLE(_check_image, false);
					SAFE_SET_VISIBLE(_check_selected_image, true);
					SAFE_SET_VISIBLE(_disable_image, false);
					SAFE_SET_VISIBLE(_check_disable_image, false);
				}
				else
				{
					if (_check_image != nullptr)
					{
						SAFE_SET_VISIBLE(_normal_image, false);
						SAFE_SET_VISIBLE(_selected_image, false);
						SAFE_SET_VISIBLE(_check_image, true);
						SAFE_SET_VISIBLE(_check_selected_image, false);
						SAFE_SET_VISIBLE(_disable_image, false);
						SAFE_SET_VISIBLE(_check_disable_image, false);
					}
					else
					{
						SAFE_SET_VISIBLE(_normal_image, true);
						SAFE_SET_VISIBLE(_selected_image, false);
						SAFE_SET_VISIBLE(_check_image, false);
						SAFE_SET_VISIBLE(_check_selected_image, false);
						SAFE_SET_VISIBLE(_disable_image, false);
						SAFE_SET_VISIBLE(_check_disable_image, false);
					}					
				}

				if (_lbl_text != nullptr)
				{
					_lbl_text->setColor(_text_check_selected_color);
				}
			}
			else
			{
				if (_selected_image != nullptr)
				{
					SAFE_SET_VISIBLE(_normal_image, false);
					SAFE_SET_VISIBLE(_selected_image, true);
					SAFE_SET_VISIBLE(_check_image, false);
					SAFE_SET_VISIBLE(_check_selected_image, false);
					SAFE_SET_VISIBLE(_disable_image, false);
					SAFE_SET_VISIBLE(_check_disable_image, false);
				}
				else
				{
					SAFE_SET_VISIBLE(_normal_image, true);
					SAFE_SET_VISIBLE(_selected_image, false);
					SAFE_SET_VISIBLE(_check_image, false);
					SAFE_SET_VISIBLE(_check_selected_image, false);
					SAFE_SET_VISIBLE(_disable_image, false);
					SAFE_SET_VISIBLE(_check_disable_image, false);
				}
				
				if (_lbl_text != nullptr)
				{
					_lbl_text->setColor(_text_selected_color);
				}
			}

			if (_press_action != nullptr)
			{
				this->stopAllActions();
				this->runAction(_press_action);
			}
		}
		break;
	case Widget_State::kWidgetStateDisabled:
		{
			if (this->isChecked())
			{
				if (_check_selected_image != nullptr)
				{
					SAFE_SET_VISIBLE(_normal_image, false);
					SAFE_SET_VISIBLE(_selected_image, false);
					SAFE_SET_VISIBLE(_check_image, false);
					SAFE_SET_VISIBLE(_check_selected_image, false);
					SAFE_SET_VISIBLE(_disable_image, false);
					SAFE_SET_VISIBLE(_check_disable_image, true);
				}
				else
				{
					if (_disable_image != nullptr)
					{
						SAFE_SET_VISIBLE(_normal_image, false);
						SAFE_SET_VISIBLE(_selected_image, false);
						SAFE_SET_VISIBLE(_check_image, false);
						SAFE_SET_VISIBLE(_check_selected_image, false);
						SAFE_SET_VISIBLE(_disable_image, true);
						SAFE_SET_VISIBLE(_check_disable_image, false);
					}
					else if (_check_image != nullptr)
					{
						SAFE_SET_VISIBLE(_normal_image, false);
						SAFE_SET_VISIBLE(_selected_image, false);
						SAFE_SET_VISIBLE(_check_image, true);
						SAFE_SET_VISIBLE(_check_selected_image, false);
						SAFE_SET_VISIBLE(_disable_image, false);
						SAFE_SET_VISIBLE(_check_disable_image, false);
					}
					else
					{
						SAFE_SET_VISIBLE(_normal_image, true);
						SAFE_SET_VISIBLE(_selected_image, false);
						SAFE_SET_VISIBLE(_check_image, false);
						SAFE_SET_VISIBLE(_check_selected_image, false);
						SAFE_SET_VISIBLE(_disable_image, false);
						SAFE_SET_VISIBLE(_check_disable_image, false);
					}
				}
			}
			else
			{
				if (_disable_image != nullptr)
				{
					SAFE_SET_VISIBLE(_normal_image, false);
					SAFE_SET_VISIBLE(_selected_image, false);
					SAFE_SET_VISIBLE(_check_image, false);
					SAFE_SET_VISIBLE(_check_selected_image, false);
					SAFE_SET_VISIBLE(_disable_image, true);
					SAFE_SET_VISIBLE(_check_disable_image, false);
				}
				else
				{
					SAFE_SET_VISIBLE(_normal_image, true);
					SAFE_SET_VISIBLE(_selected_image, false);
					SAFE_SET_VISIBLE(_check_image, false);
					SAFE_SET_VISIBLE(_check_selected_image, false);
					SAFE_SET_VISIBLE(_disable_image, false);
					SAFE_SET_VISIBLE(_check_disable_image, false);
				}
			}
		}
		break;
	default:
		break;
	}

	if (! this->isHideNormalImage())
	{
		SAFE_SET_VISIBLE(_normal_image, true);
	}
}