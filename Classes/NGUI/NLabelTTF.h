#ifndef __NLABELTTF_H__
#define __NLABELTTF_H__

#include "NGUI/NProtocol.h"

class NLabelTTF : public LabelTTF
{
public:
	NLabelTTF(void);
	~NLabelTTF(void);

	static NLabelTTF * create(const string& str_text, const string& font_name, float font_size, Text_Direction direction = Text_Direction::kText_Dir_Horizontal);
    static NLabelTTF * create(const string& str_text, const string& font_name, float font_size, Text_Direction direction,
                               const Size& dimensions, TextHAlignment hAlignment);
    static NLabelTTF * create(const string& str_text, const string& font_name, float font_size,  Text_Direction direction,
                               const Size& dimensions, TextHAlignment hAlignment, 
                               TextVAlignment vAlignment);

	virtual bool initWithString(const string& str_text, const string& font_name, float font_size, Text_Direction direction,
		const Size& dimensions, TextHAlignment hAlignment, 
		TextVAlignment vAlignment);

	void setTextDirection(Text_Direction direction);
	Text_Direction getTextDirection() const;

	virtual void setString(const string& str_text);

private:
	virtual string verticalText(const string& str_text);
	virtual string horizontalText(const string& str_text);

private:
	Text_Direction _direction;
};

#endif // __NLABELTTF_H__
