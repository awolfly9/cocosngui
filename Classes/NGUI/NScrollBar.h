#ifndef __NSCROLL_BAR_H__
#define __NSCROLL_BAR_H__

#include "NGUI/NBar.h"
#include "NGUI/NButton.h"

class NScrollBar : public NBar
{
public:
	NScrollBar(void);
	~NScrollBar(void);

	static NScrollBar* create();
	static NScrollBar* create(const string& background, NButton* btn_scroll_bar = nullptr);
	static NScrollBar* create(const string& background, const string& normal_image, const string& selected_image = "", const string& disabled_image = "", const string& click_effect = "");
	
	virtual bool init(const string& background, NButton* btn_scroll_bar);
	virtual bool init(const string& background, const string& normal_image, const string& selected_image = "", const string& disabled_image = "", const string& click_effect = "");

	void setScrollBar(NButton* btn_scroll_bar);
	void setScrollBar(const string& normal_image, const string& selected_image = "", const string& disabled_image = "", const string& click_effect = "");

	void setScrolling(bool is_scrolling);
	bool isScrolling() const;

protected:
	virtual void interceptTouch(const Touch_Period& touch_period, Touch* touch, NWidget* widget);
	virtual void handlePressLogic(Touch *touch);
	virtual void handleMoveLogic(Touch *touch);
	virtual void handleReleaseLogic(Touch *touch);

	virtual void updateBar();

private:
	NButton* _btn_scroll_bar;
	Vec2 _delta_position;

	bool _is_scrolling;
};

#endif // __NSCROLL_BAR_H__
