#ifndef __NJOYSTICK_H__
#define __NJOYSTICK_H__

#include "NGUI/NProtocol.h"
#include "NGUI/NWidget.h"

class NJoystick : public NWidget
{
public:
	NJoystick(void);
	~NJoystick(void);

	static NJoystick* create(const string& bg, const string& handle, float radius);
	virtual bool init(const string& bg, const string& handle, float radius);

	void setRadius(float radius);
	float getRadius() const;

	void setAutoReset(bool is_auto_reset);
	bool isAutoReset() const;

	const Vec2& getDir() const;
	
	virtual bool onTouchBegan(Touch* touch, Event* event);
	virtual void onTouchMoved(Touch* touch, Event* event);
	virtual void onTouchEnded(Touch* touch, Event* event);
	virtual void onTouchCancelled(Touch* touch, Event* event);

protected:
	virtual bool hitWidget(const Vec2& point);
	virtual void update(float dt);

private:
	Node* _background;
	Node* _handle;
	float _radius;

	bool _is_auto_reset;
	Vec2 _center;
};

#endif // __NJOYSTICK_H__