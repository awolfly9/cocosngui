#ifndef __NBAR_H__
#define __NBAR_H__

#include "NGUI/NProtocol.h"
#include "NGUI/NWidget.h"

class NBar : public NWidget
{
public:
	NBar(void);
	~NBar(void);

	virtual bool init(const string& background);

	void setBackground(const string& background);
	void setBackground(Node* background);
	Node* getBackground() const;

	void setPercentage(float percentage);
	float getPercentage() const;

	void setDirection(const Bar_Direction& direction);
	Bar_Direction getDirection() const;

	void setMaxValue(int max_value);
	int getMaxValue() const;

	void setMinValue(int min_value);
	int getMinValue() const;

	void setCurrentValue(int current_value);
	int getCurrentValue() const;

	void setBarCallback(const Bar_Callback& bar_callback);

	void startValueTo(float duration, int value);
	void startPercentageTo(float duration, float percentage);
	void startValueBy(float duration, int value);
	void startPercentageBy(float duration, float percentage);

	void stopUpdatePercentage();

protected:
	virtual void handlePressLogic(Touch *touch) { };
	virtual void handleMoveLogic(Touch *touch) { };
	virtual void handleReleaseLogic(Touch *touch) { };

	virtual void startUpdatePercentage(float duration, float percentage);
	virtual void updatePercentage(float dt);

	virtual void updateBar() { };

	virtual void activate(const Bar_Period& bar_period);

protected:
	float _percentage;

private:
	float _previous_percentage;	
	float _to_percentage;

	float _cur_time;
	float _duration;	
	bool _is_update_percentage;

	int _max_value;
	int _min_value;	

	Bar_Direction _direction;
	Bar_Callback _bar_callback;

	Node* _background;
};

#endif // __NBAR_H__