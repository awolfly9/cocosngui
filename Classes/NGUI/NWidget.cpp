#include "NWidget.h"

NWidget::NWidget(void)
	: _name("")
	, _click_effect("")	
	, _is_touch_enabled(false)
	, _is_enabled(true)
	, _is_interrupt_touch(false)
	, _is_play_click_effect(false)
	, _touch_call_back(nullptr)
	, _widgt_state(Widget_State::kWidgetStateNormal)	
	, _began_touch_point(Vec2(0, 0))
	, _touch_listener(nullptr)
{
	this->setUniqueID(NWIDGET);
}

NWidget::~NWidget(void)
{
}

NWidget* NWidget::create()
{
	NWidget* ret = new NWidget();

	if (ret && ret->init())
	{ 
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

bool NWidget::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF( ! Node::init());

	this->setAnchorPoint(Vec2(0.5f, 0.5f));
	this->setPosition(Vec2(0, 0));

	this->setCascadeColorEnabled(true);
	this->setCascadeOpacityEnabled(true);

	this->setTouchEnabled(true);

	INIT_DO_WHILE_END
}

void NWidget::setCallback(const Touch_Callback& call_back)
{
	_touch_call_back = call_back;
}

void NWidget::setName(const string& name)
{
	if (_name != name)
	{
		_name = name;
	}
}

string NWidget::getName() const
{
	return _name;
}

void NWidget::setTouchEnabled(bool is_touch_enable)
{
	if (_is_touch_enabled != is_touch_enable)
	{
		_is_touch_enabled = is_touch_enable;

		if (_is_touch_enabled && _touch_listener == nullptr)
		{
			_touch_listener = EventListenerTouchOneByOne::create();
			 _touch_listener->setSwallowTouches(true);

			 _touch_listener->onTouchBegan = CALLBACK_2(NWidget::onTouchBegan, this);
			 _touch_listener->onTouchMoved = CALLBACK_2(NWidget::onTouchMoved, this);
			 _touch_listener->onTouchEnded = CALLBACK_2(NWidget::onTouchEnded, this);
			 _touch_listener->onTouchCancelled = CALLBACK_2(NWidget::onTouchCancelled, this);

			 Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(_touch_listener, this);		
		}
		else
		{
			if (_touch_listener != nullptr)
			{
				Director::getInstance()->getEventDispatcher()->removeEventListener(_touch_listener);
			}
		}
	}	
}

bool NWidget::isTouchEnabled() const
{
	return _is_touch_enabled;
}

void NWidget::setEnabled(bool is_enabled)
{
	if (_is_enabled != is_enabled)
	{
		_is_enabled = is_enabled;

		if (_is_enabled)
		{
			this->setWidgetState(Widget_State::kWidgetStateNormal);
		}
		else
		{
			this->setWidgetState(Widget_State::kWidgetStateDisabled);
		}
	}
}

bool NWidget::isEnabled() const
{
	return _is_enabled;
}

void NWidget::setClickEffect(const string& click_effect)
{
	if (click_effect != "")
	{
		_click_effect = click_effect;
		_is_play_click_effect = true;
	}	
}

string NWidget::getClickEffect() const
{
	return _click_effect;
}

void NWidget::setPlayClickEffect(bool is_play_click_effect)
{
	_is_play_click_effect = is_play_click_effect;
}

bool NWidget::isPlayClickEffect() const
{
	return _is_play_click_effect;
}

void NWidget::setInterruptTouch(bool is_interrupt)
{
	if (_is_interrupt_touch != is_interrupt)
	{
		_is_interrupt_touch = is_interrupt;

		if (_is_interrupt_touch)
		{
			this->setWidgetState(Widget_State::kWidgetStateNormal);
		}
	}
}

bool NWidget::isInterruptTouch() const
{
	return this->_is_interrupt_touch;
}

void NWidget::setWidgetState(const Widget_State& widget_state)
{
	if(_widgt_state != widget_state)
	{
		_widgt_state = widget_state;

		this->updateWidget(_widgt_state);
	}
}

Widget_State NWidget::getWidgetState() const
{
	return _widgt_state;
}

Vec2 NWidget::getBeginTouchVec2() const
{
	return _began_touch_point;
}

string NWidget::getUniqueID() const
{
	return _unique_id;
}

void NWidget::setUniqueID(const string& unique_id)
{
	this->_unique_id = unique_id;
}

bool NWidget::checkNodeVisible(Node* node)
{
	if (node == nullptr)
	{
		return true;
	}

	if (node->isVisible() == false)
	{
		return false;
	}

	Node* parent = node->getParent();

	if (parent == nullptr)
	{
		return true;
	}

	return this->checkNodeVisible(parent);
}

bool NWidget::onTouchBegan(Touch* touch, Event* event)
{
	if (! _is_touch_enabled || ! _is_enabled)
	{
		return false;
	}

	_began_touch_point = touch->getLocation();

	if (this->checkNodeVisible(this) && this->hitWidget(_began_touch_point))
	{
		this->interceptTouch(Touch_Period::kTouchBegan, touch, this);

		if (! this->isInterruptTouch())
		{
			this->activate(Touch_Period::kTouchBegan);
			this->setWidgetState(Widget_State::kWidgetStateSelected);

			if (this->isPlayClickEffect())
			{
				AUDIO_MANAGER->playEffect(_click_effect);
			}
		}
		
		return true;
	}

	return false;
}

void NWidget::onTouchMoved(Touch* touch, Event* event)
{
	Vec2 point = touch->getLocation();
	this->interceptTouch(Touch_Period::kTouchMoved, touch, this);

	if (this->hitWidget(point))
	{
		if (this->isInterruptTouch())
		{
			this->activate(Touch_Period::kTouchCancelled);
			this->setWidgetState(Widget_State::kWidgetStateNormal);
		}
		else
		{
			this->activate(Touch_Period::kTouchMoved);
			this->setWidgetState(Widget_State::kWidgetStateSelected);
		}
	}
	else
	{
		this->setWidgetState(Widget_State::kWidgetStateNormal);
	}
}

void NWidget::onTouchEnded(Touch* touch, Event* event)
{
	Vec2 point = touch->getLocation();
	this->interceptTouch(Touch_Period::kTouchEnded, touch, this);
	
	if (this->hitWidget(point))
	{
		if (this->isInterruptTouch())
		{
			this->activate(Touch_Period::kTouchCancelled);
		}
		else
		{
			log("onTouchEnded isInterruptTouch false");
			this->activate(Touch_Period::kTouchEnded);
		}
	}
	
	this->setWidgetState(Widget_State::kWidgetStateNormal);
	this->setInterruptTouch(false);
}

void NWidget::onTouchCancelled(Touch* touch, Event* event)
{	
	this->setInterruptTouch(false);
	this->activate(Touch_Period::kTouchCancelled);
	this->setWidgetState(Widget_State::kWidgetStateNormal);
}

void NWidget::interceptTouch(const Touch_Period& touch_period, Touch* touch, NWidget* widget)
{
	Node* parent = this->getParent();

	if (parent != nullptr)
	{
		NWidget* widget_parent = dynamic_cast<NWidget*>(parent);

		if (widget_parent != nullptr)
		{
			return widget_parent->interceptTouch(touch_period, touch, widget);
		}
	}
}

bool NWidget::hitWidget(const Vec2& point)
{
	Vec2 pt = this->convertToNodeSpace(point);

	Rect rect = Rect(0, 0, this->getContentSize().width, this->getContentSize().height);

	if (rect.containsPoint(pt))
	{
		return true;
	}

	return false;
}

void NWidget::activate(const Touch_Period& touch_period)
{
	if (_touch_call_back != nullptr)
	{
		_touch_call_back(this, touch_period);
	}	
}