#ifndef __NGRID_VIEW_H__
#define __NGRID_VIEW_H__

#include "NTableView.h"

class NGridViewCell : public NTableViewCell
{
public:
	NGridViewCell(void);
	~NGridViewCell(void);

	CREATE_FUNC(NGridViewCell);
};

class NGridView : public NTableView
{
public:
	NGridView(void);
	~NGridView(void);

	static NGridView* create(const Size& view_size, const Size& cell_size, int cell_count, int columns, Load_Callback load_callback);
	virtual bool init(const Size& view_size, const Size& cell_size, int cell_count, int columns, Load_Callback load_cell);

	void setColumns(int columns);
	int getColumns() const;

	virtual void reloadData();

public:
	virtual void setDirection(ScrollView_Direction scroll_view_direction);

protected:
	virtual void onScrolling();
	virtual void onScrollingEnd();

	virtual void updatePosition();
	virtual void loadCellWithIndex(int index);

	virtual int getOriginIndex(const Vec2& point);
	virtual int getEndIndex(const Vec2& point);

private:
	int _columns;
	int _row;
};

#endif // __NGRID_VIEW_H__
