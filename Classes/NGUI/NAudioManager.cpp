#include "NAudioManager.h"

NAudioManager* NAudioManager::_instance = nullptr;

#define LOAD_BG_MUSIC_CALLBACK "load_bg_music_callback"
#define LOAD_EFFECT_CALLBACK "load_effect_callback"

NAudioManager::NAudioManager(void)
: _is_play_click_effect(true)
, _is_play_bg_music(true)
, _async_bg_music_queue(nullptr)
, _async_effect_queue(nullptr)
, _bg_music_info_deque(nullptr)
, _effect_info_deque(nullptr)
, _loading_bg(nullptr)
, _loading_effect(nullptr)
, _need_bg_quit(false)
, _need_effect_quit(false)
, _async_bg_music_count(0)
, _async_effect_count(0)
{
}

NAudioManager::~NAudioManager(void)
{
	this->waitForBgMusicQuit();
	this->waitForEffectQuit();

	SAFE_DELETE_NULL(_loading_bg);
	SAFE_DELETE_NULL(_loading_effect);

	this->end();
}

void NAudioManager::end()
{
	SIMPLE_AUDIO_ENGINE->end();
}

NAudioManager* NAudioManager::getInstance()
{
	if (_instance == nullptr)
	{
		_instance = new NAudioManager();
	}

	return _instance;
}

void NAudioManager::destroyInstance()
{
	if (_instance != nullptr)
	{
		delete _instance;
	}
}

void NAudioManager::preloadBgMusic(const string& file_path)
{
	SIMPLE_AUDIO_ENGINE->preloadBackgroundMusic(file_path.c_str());
}

void NAudioManager::preloadBgMusic(vector<string>* file_path)
{
	for (vector<string>::iterator iter = file_path->begin(); iter != file_path->end(); ++iter)
	{
		this->preloadBgMusic(string(*iter).c_str());
	}
}

//void NAudioManager::preloadBgMusicAsync(const string& file_path, const std::function<void(const string& file_path)>& callback)
//{
//	// lazy init
//	if (_async_bg_music_queue == nullptr)
//	{
//		_async_bg_music_queue = new queue<AsyncBgMusic*>();
//		_bg_music_info_deque = new deque<BgMusicInfo*>();
//
//		// create a new thread to load music
//		_loading_bg = new std::thread(&NAudioManager::loadBgMusic, this);	
//
//		_need_bg_quit = false;
//	}
//
//	if (0 == _async_bg_music_count)
//	{
//		Director::getInstance()->getScheduler()->schedule(CALLBACK_1(NAudioManager::loadBgAsyncCallback, this), this, 0, false, LOAD_BG_MUSIC_CALLBACK);
//	}
//
//	++ _async_bg_music_count;
//	AsyncBgMusic* data = new AsyncBgMusic(file_path, callback);
//	_async_bg_music_mutex.lock();
//	_async_bg_music_queue->push(data);
//	_async_bg_music_mutex.unlock();
//
//	_sleep_bg_music_condition.notify_one();
//}

void NAudioManager::playBgMusic(const string& file_path, bool is_loop)
{
	if (_is_play_bg_music)
	{
		SIMPLE_AUDIO_ENGINE->playBackgroundMusic(file_path.c_str(), is_loop);
	}
}

void NAudioManager::stopBgMusic(bool is_release_data)
{
	SIMPLE_AUDIO_ENGINE->stopBackgroundMusic(is_release_data);
}

void NAudioManager::pauseBgMusic()
{
	SIMPLE_AUDIO_ENGINE->pauseBackgroundMusic();
}

void NAudioManager::resumeBgMusic()
{
	if (_is_play_bg_music)
	{
		SIMPLE_AUDIO_ENGINE->resumeBackgroundMusic();
	}
}

void NAudioManager::rewindBgMusic()
{
	if (_is_play_bg_music)
	{
		SIMPLE_AUDIO_ENGINE->rewindBackgroundMusic();
	}
}

bool NAudioManager::willPlayBgMusic()
{
	return SIMPLE_AUDIO_ENGINE->willPlayBackgroundMusic();
}

bool NAudioManager::isBgMusicPlaying()
{
	return SIMPLE_AUDIO_ENGINE->isBackgroundMusicPlaying();
}

float NAudioManager::getBgMusicVolume()
{
	return SIMPLE_AUDIO_ENGINE->getBackgroundMusicVolume();
}

void NAudioManager::setBgMusicVolume(float volume)
{
	SIMPLE_AUDIO_ENGINE->setBackgroundMusicVolume(volume);
}

float NAudioManager::getEffectsVolume()
{
	return SIMPLE_AUDIO_ENGINE->getEffectsVolume();
}

void NAudioManager::setEffectsVolume(float volume)
{
	SIMPLE_AUDIO_ENGINE->setEffectsVolume(volume);
}

unsigned int NAudioManager::playEffect(const string& file_path, bool is_loop)
{
	if (_is_play_click_effect)
	{
		return SIMPLE_AUDIO_ENGINE->playEffect(file_path.c_str(), is_loop);
	}
	else
	{
		return 1;
	}
}

void NAudioManager::pauseEffect(unsigned int sound_id)
{
	SIMPLE_AUDIO_ENGINE->pauseEffect(sound_id);
}

void NAudioManager::pauseAllEffects()
{
	SIMPLE_AUDIO_ENGINE->pauseAllEffects();
}

void NAudioManager::resumeEffect(unsigned int sound_id)
{
	if (_is_play_click_effect)
	{
		SIMPLE_AUDIO_ENGINE->resumeEffect(sound_id);
	}
}

void NAudioManager::resumeAllEffects()
{
	if (_is_play_click_effect)
	{
		SIMPLE_AUDIO_ENGINE->resumeAllEffects();
	}
}

void NAudioManager::stopEffect(unsigned int sound_id)
{
	SIMPLE_AUDIO_ENGINE->stopEffect(sound_id);
}

void NAudioManager::stopAllEffects()
{
	SIMPLE_AUDIO_ENGINE->stopAllEffects();
}

void NAudioManager::preloadEffect(const string& file_path)
{
	SIMPLE_AUDIO_ENGINE->preloadEffect(file_path.c_str());
}

void NAudioManager::preloadEffect(vector<string> file_path)
{
	for (vector<string>::iterator iter = file_path.begin(); iter != file_path.end(); ++iter)
	{
		SIMPLE_AUDIO_ENGINE->preloadEffect(string(*iter).c_str());
	}
}

void NAudioManager::unloadEffect(const string& file_path)
{
	SIMPLE_AUDIO_ENGINE->unloadEffect(file_path.c_str());
}

void NAudioManager::preloadEffectAsync(const string& file_path, const std::function<void(const string& file_path)>& callback)
{
	// lazy init
	if (_async_effect_queue == nullptr)
	{
		_async_effect_queue = new queue<AsyncEffect*>();
		_effect_info_deque = new deque<EffectInfo*>();

		// create a new thread to load music
		_loading_effect = new std::thread(&NAudioManager::loadEffect, this);

		_need_effect_quit = false;
	}

	if (0 == _async_effect_count)
	{
		Director::getInstance()->getScheduler()->schedule(CALLBACK_1(NAudioManager::loadEffectAsyncCallback, this), this, 0, false, LOAD_EFFECT_CALLBACK);
	}

	++_async_effect_count;
	AsyncEffect* data = new AsyncEffect(file_path, callback);
	_async_effect_mutex.lock();
	_async_effect_queue->push(data);
	_async_effect_mutex.unlock();

	_sleep_effect_condition.notify_one();
}

void NAudioManager::setPlayBgMusic(bool is_play_bg_music)
{
	if (_is_play_bg_music != is_play_bg_music)
	{
		_is_play_bg_music = is_play_bg_music;
	}
}

bool NAudioManager::isPlayBgMusic()
{
	return _is_play_bg_music;
}

void NAudioManager::setPlayClickEffect(bool is_play_click_effect)
{
	if (_is_play_click_effect != is_play_click_effect)
	{
		_is_play_click_effect = is_play_click_effect;
	}
}

void NAudioManager::waitForBgMusicQuit()
{
	_need_bg_quit = true;
	_sleep_bg_music_condition.notify_one();

	if (_loading_bg)
	{
		_loading_bg->join();
	}
}

void NAudioManager::waitForEffectQuit()
{
	_need_effect_quit = true;
	_sleep_effect_condition.notify_one();

	if (_loading_effect)
	{
		_loading_effect->join();
	}
}

bool NAudioManager::isPlayClickEffect()
{
	return _is_play_click_effect;
}

//void NAudioManager::loadBgMusic()
//{
//	AsyncBgMusic* async_bg_music = nullptr;
//
//	while (true)
//	{
//		std::queue<AsyncBgMusic*>* async_bg_music_queue = _async_bg_music_queue;
//		_async_bg_music_mutex.lock();
//
//		if (async_bg_music_queue->empty())
//		{
//			_async_bg_music_mutex.unlock();
//
//			if (_need_bg_quit)
//			{
//				break;
//			}
//			else
//			{
//				std::unique_lock<std::mutex> lk(_sleep_mutex_bg_music);
//				_sleep_bg_music_condition.wait(lk);
//				continue;
//			}
//		}
//		else
//		{
//			async_bg_music = async_bg_music_queue->front();
//			async_bg_music_queue->pop();
//			_async_bg_music_mutex.unlock();
//		}
//
//		this->preloadBgMusic(async_bg_music->_file_path);
//
//		BgMusicInfo* bg_music_info = new BgMusicInfo();
//		bg_music_info->_async_bg_music = async_bg_music;
//
//		_bg_music_info_mutex.lock();
//		_bg_music_info_deque->push_back(bg_music_info);
//		_bg_music_info_mutex.unlock();
//	}
//
//	if (_async_bg_music_queue != nullptr)
//	{
//		delete _async_bg_music_queue;
//		_async_bg_music_queue = nullptr;
//		delete _bg_music_info_deque;
//		_bg_music_info_deque = nullptr;
//	}
//}

void NAudioManager::loadEffect()
{
	AsyncEffect* async_effect = nullptr;

	while (true)
	{
		std::queue<AsyncEffect*>* async_effect_queue = _async_effect_queue;
		_async_effect_mutex.lock();

		if (async_effect_queue->empty())
		{
			_async_effect_mutex.unlock();

			if (_need_effect_quit)
			{
				break;
			}
			else
			{
				std::unique_lock<std::mutex> lk(_sleep_mutex_effect);
				_sleep_effect_condition.wait(lk);
				continue;
			}
		}
		else
		{
			async_effect = async_effect_queue->front();
			async_effect_queue->pop();
			_async_effect_mutex.unlock();
		}

		this->preloadEffect(async_effect->_file_path);

		EffectInfo* effect_info = new EffectInfo();
		effect_info->_async_effect = async_effect;

		_bg_music_info_mutex.lock();
		_effect_info_deque->push_back(effect_info);
		_bg_music_info_mutex.unlock();
	}

	if (_async_effect_queue != nullptr)
	{
		delete _async_effect_queue;
		_async_effect_queue = nullptr;
		delete _effect_info_deque;
		_effect_info_deque = nullptr;
	}
}

//void NAudioManager::loadBgAsyncCallback(float dt)
//{
//	std::deque<BgMusicInfo*>* bg_music_info_deque = _bg_music_info_deque;
//	_bg_music_info_mutex.lock();
//
//	if (bg_music_info_deque->empty())
//	{
//		_bg_music_info_mutex.unlock();
//	}
//	else
//	{
//		BgMusicInfo* music_info = bg_music_info_deque->front();
//		bg_music_info_deque->pop_front();
//		_bg_music_info_mutex.unlock();
//
//		AsyncBgMusic* async_bg_music = music_info->_async_bg_music;
//
//		if (async_bg_music->_callback)
//		{
//			async_bg_music->_callback(async_bg_music->_file_path);
//		}
//
//		delete async_bg_music;
//		delete music_info;
//
//		-- _async_bg_music_count;
//
//		if (0 == _async_bg_music_count)
//		{
//			Director::getInstance()->getScheduler()->unschedule(LOAD_BG_MUSIC_CALLBACK, this);
//		}
//	}
//}

void NAudioManager::loadEffectAsyncCallback(float dt)
{
	std::deque<EffectInfo*>* effect_info_deque = _effect_info_deque;
	_effect_info_mutex.lock();

	if (effect_info_deque->empty())
	{
		_effect_info_mutex.unlock();
	}
	else
	{
		EffectInfo* effect_info = effect_info_deque->front();
		effect_info_deque->pop_front();
		_effect_info_mutex.unlock();

		AsyncEffect* async_effect = effect_info->_async_effect;

		if (async_effect->_callback)
		{
			async_effect->_callback(async_effect->_file_path);
		}

		delete async_effect;
		delete effect_info;

		--_async_effect_count;

		if (0 == _async_effect_count)
		{
			Director::getInstance()->getScheduler()->unschedule(LOAD_EFFECT_CALLBACK, this);
		}
	}
}