#include "NMessageTips.h"
#include "NGUI/NScale9Sprite.h"

NMessageTips* NMessageTips::_instance = nullptr;

NMessageTips::NMessageTips(void)
	: _color(Color3B::RED)
	, _distance(10)
	, _font_name("Arial")
	, _font_size(30)
	, _background("")
{
	auto delay = DelayTime::create(2.0f);
	auto fade = FadeOut::create(0.5f);
	auto callfunc = CallFuncN::create(CALLBACK_1(NMessageTips::callfunc, this));
	_action = Sequence::create(delay, fade, callfunc, nullptr);
	_action->retain();
}

NMessageTips::~NMessageTips(void)
{
	SAFE_RELEASE_NULL(_action);
}

NMessageTips* NMessageTips::getInstance()
{
	if (_instance == nullptr)
	{
		_instance = new NMessageTips();
	}

	return _instance;
}

void NMessageTips::destroyInstance()
{
	if (_instance != nullptr)
	{
		delete _instance;
	}
}

bool NMessageTips::init(const string& background, const string& font_name, float font_size, const Color3B& color)
{
	this->setBackground(background);
	this->setFontName(font_name);
	this->setFontSize(font_size);
	this->setColor(color);

	return true;
}

void NMessageTips::addTips(const string& tips)
{
	if (tips == "")
	{
		return ;
	}

	Label* lbl = Label::createWithSystemFont(tips, _font_name, _font_size);
	lbl->setColor(this->_color);
	this->addTips(lbl);
}

void NMessageTips::addTips(Node* tips)
{
	if (tips == nullptr)
	{
		return ;
	}

	if (this->_background != "")
	{
		NScale9Sprite* bg = NScale9Sprite::create(this->_background);
		bg->setContentSize(Size(tips->getContentSize().width * 1.1, tips->getContentSize().height * 1.05));
		bg->addChild(tips);
		tips->setAnchorPoint(Vec2(0.5f, 0.5f));
		tips->setPosition(bg->getContentSize().width / 2, bg->getContentSize().height / 2);
		bg->setAnchorPoint(Vec2(0.5f, 0.5f));

		_tips_deque.push_front(bg);
		Action* action = _action->clone();
		bg->runAction(action);

		Director::getInstance()->getRunningScene()->addChild(bg);
	}
	else
	{
		_tips_deque.push_front(tips);
		Action* action = _action->clone();
		tips->runAction(action);
		tips->setAnchorPoint(Vec2(0.5f, 0.5f));
		Director::getInstance()->getRunningScene()->addChild(tips);
	}

	this->updateTips();
}

void NMessageTips::clear()
{
	for (auto iter = _tips_deque.begin(); iter != _tips_deque.end(); ++ iter)
	{
		Node* node = (Node*)(*iter);

		if (node == nullptr)
		{
			continue;
		}

		node->stopAllActions();
		node->removeFromParent();
	}

	_tips_deque.clear();
}

void NMessageTips::updateTips()
{
	float height = 0;
	auto visible_size = Director::getInstance()->getVisibleSize();
	int count = 0;
	for (auto iter = _tips_deque.begin(); iter != _tips_deque.end(); ++ iter)
	{
		Node* node = (Node*)(*iter);

		if (node == nullptr)
		{
			continue;
		}

		if (count >= 1)
		{
			height += node->getContentSize().height / 2;
		}

		node->setPosition(visible_size.width * 0.5, visible_size.height * 0.5 + height);
		height += node->getContentSize().height / 2 + this->_distance;
		count ++;
	}
}

void NMessageTips::setBackground(const string& background)
{
	this->_background = background;
}

const string& NMessageTips::getBackground() const
{
	return this->_background;
}

void NMessageTips::setFontName(const string& font_name)
{
	this->_font_name = font_name;
}

const string& NMessageTips::getFontName() const
{
	return this->_font_name;
}

void NMessageTips::setFontSize(float font_size)
{
	this->_font_size = font_size;
}

float NMessageTips::getFontSize() const
{
	return this->_font_size;
}

void NMessageTips::setColor(const Color3B& color)
{
	this->_color = color;
}

const Color3B& NMessageTips::getColor() const
{
	return this->_color;
}

void NMessageTips::setDistance(float distance)
{
	this->_distance = distance;
}

float NMessageTips::getDistance() const
{
	return _distance;
}

void NMessageTips::setAction(FiniteTimeAction* action)
{
	if (action == nullptr)
	{
		return ;
	}

	if (this->_action != nullptr)
	{
		SAFE_RELEASE_NULL(_action);
	}

	auto callfunc = CallFuncN::create(CALLBACK_1(NMessageTips::callfunc, this));
	_action = Sequence::create(action, callfunc, nullptr);
	_action->retain();
}

Action* NMessageTips::getAction() const
{
	return this->_action;
}

void NMessageTips::callfunc(Ref* ref)
{
	Node* node = (Node*)ref;
	_tips_deque.pop_back();

	if (node == nullptr)
	{
		return ;
	}
	
	node->removeFromParent();
}
