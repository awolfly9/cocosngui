#include "NLabelTTF.h"


NLabelTTF::NLabelTTF(void)
	: _direction(Text_Direction::kText_Dir_Horizontal)
{
}

NLabelTTF::~NLabelTTF(void)
{
}

NLabelTTF * NLabelTTF::create(const string& str_text, const string& font_name, float font_size, Text_Direction direction)
{
	return NLabelTTF::create(str_text, font_name, font_size, direction,
		Size(0, 0), kCCTextAlignmentCenter, kCCVerticalTextAlignmentTop);
}

NLabelTTF * NLabelTTF::create(const string& str_text, const string& font_name, float font_size, Text_Direction direction,
						   const Size& dimensions, TextHAlignment hAlignment)
{
	return NLabelTTF::create(str_text, font_name, font_size, direction, dimensions, hAlignment, kCCVerticalTextAlignmentTop);
}

NLabelTTF * NLabelTTF::create(const string& str_text, const string& font_name, float font_size, Text_Direction direction,
						   const Size& dimensions, TextHAlignment hAlignment, 
						   TextVAlignment vAlignment)
{
	NLabelTTF *ret = new NLabelTTF();

	if (ret && ret->initWithString(str_text, font_name, font_size, direction, dimensions, hAlignment, vAlignment))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}
	
	return ret;
}

bool NLabelTTF::initWithString(const string& str_text, const string& font_name, float font_size, Text_Direction direction,
							const Size& dimensions, TextHAlignment hAlignment, 
							TextVAlignment vAlignment)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! LabelTTF::initWithString(str_text.c_str(), font_name.c_str(), font_size, dimensions, hAlignment, vAlignment));
	
	this->setTextDirection(direction);

	INIT_DO_WHILE_END
}

void NLabelTTF::setTextDirection(Text_Direction ditection)
{
	if (_direction != ditection)
	{
		_direction = ditection;

		switch (_direction)
		{
		case Text_Direction::kText_Dir_Vertical:
			{
				string buffer = verticalText(string(this->getString()));
				LabelTTF::setString(buffer.c_str());
			}
			break;
		case Text_Direction::kText_Dir_Horizontal:
			{
				string buffer = horizontalText(string(this->getString()));
				LabelTTF::setString(buffer.c_str());
			}
			break;
		default:
			break;
		}
	}
}

Text_Direction NLabelTTF::getTextDirection() const
{
	return _direction;
}

void NLabelTTF::setString(const string& str_text)
{
	switch (_direction)
	{
	case Text_Direction::kText_Dir_Vertical:
		{
			LabelTTF::setString(verticalText(str_text).c_str());
		}
		break;
	case Text_Direction::kText_Dir_Horizontal:
		{
			LabelTTF::setString(horizontalText(str_text).c_str());
		}
		break;
	default:
		break;
	}
}

string NLabelTTF::verticalText(const string& str_text)
{
	if (str_text == "")
	{
		return "";
	}

	string buffer = "";
	char* pstr = const_cast<char*>(str_text.c_str());

	while ( *pstr != 0 )
	{
		int length = str_utf8_char_len(*pstr);
	
		buffer.append(pstr, (unsigned int)length);
		buffer.append("\n");

		pstr += length;
	}

	if (buffer.size() >= 1 && buffer.at(buffer.size() -1) == '\n')
	{
		buffer.erase(buffer.size() - 1, buffer.size());
	}

	return buffer;
}

string NLabelTTF::horizontalText(const string& str_text)
{
	if (str_text == "")
	{
		return "";
	}

	string buffer = "";
	char* pstr = const_cast<char*>(str_text.c_str());

	while ( *pstr != 0 )
	{
		int length = str_utf8_char_len(*pstr);
		
		if (*pstr != '\n')
		{
			buffer.append(pstr,(unsigned int)length);
		}

		pstr += length;
	}

	return buffer;
}
