#include "NBar.h"

NBar::NBar(void)
	: _previous_percentage(0.0f)
	, _percentage(0.0f)
	, _to_percentage(0.0f)
	, _cur_time(0.0f)
	, _duration(0.0f)
	, _is_update_percentage(false)
	, _max_value(0)
	, _min_value(0)
	, _direction(Bar_Direction::kBar_Dir_LeftToRight)
	, _bar_callback(nullptr)
	, _background(nullptr)
{
	this->setUniqueID(NBAR);
}

NBar::~NBar(void)
{
}

bool NBar::init(const string& background)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF( ! NWidget::init());

	this->setEnabled(false);

	this->setBackground(background);

	INIT_DO_WHILE_END
}

void NBar::setBackground(const string& background)
{
	if (background == "")
	{
		return ;
	}

	this->setBackground(Sprite::create(background.c_str()));
}

void NBar::setBackground(Node* background)
{
	if (background == nullptr)
	{
		return ;
	}

	if (_background != nullptr)
	{
		_background->removeFromParent();
		_background = nullptr;
	}
	else
	{
		_background = background;
		this->setContentSize(_background->getContentSize());
		_background->setAnchorPoint(Vec2(0.5f, 0.5f));
		_background->setPosition(Vec2(this->getContentSize().width / 2, this->getContentSize().height / 2));
		this->addChild(_background, -1);
	}
}

Node* NBar::getBackground() const
{
	return _background;
}

void NBar::setPercentage(float percentage)
{
	if (percentage == _percentage)
	{
		return ;
	}

	if (percentage < 0.0f)
	{ 
		percentage = 0.0f;
	}
	else if (percentage > 1.0f)
	{
		percentage = 1.0f;
	}

	_percentage = percentage;
	this->updateBar();

	this->activate(Bar_Period::kBar_Move);
}

float NBar::getPercentage() const
{
	return _percentage;
}

void NBar::setDirection(const Bar_Direction& direction)
{
	if (_direction != direction)
	{
		_direction = direction;
		this->updateBar();
	}
}

Bar_Direction NBar::getDirection() const
{
	return _direction;
}

void NBar::setMaxValue(int max_value)
{
	if (max_value <= _min_value)
	{
		return ;
	}

	_max_value = max_value;
}

int NBar::getMaxValue() const
{
	return _max_value;
}

void NBar::setMinValue(int min_value)
{
	if (min_value >= _max_value)
	{
		return ;
	}

	_min_value = min_value;
}

int NBar::getMinValue() const
{
	return _min_value;
}

void NBar::setCurrentValue(int current_value)
{
	current_value = MIN(current_value, _max_value);
	current_value = MAX(current_value, _min_value);

	float percentage = 1.0f * current_value / (_max_value - _min_value);

	this->setPercentage(percentage);
}

int NBar::getCurrentValue() const
{
	return (int)(this->getPercentage() * (_max_value - _min_value));
}

void NBar::startValueTo(float duration, int value)
{
	value = MIN(value, _max_value);
	value = MAX(value, _min_value);

	this->startPercentageTo(duration, value * 1.0f / (_max_value - _min_value));
}

void NBar::startPercentageTo(float duration, float percentage)
{
	this->startUpdatePercentage(duration, percentage);
}

void NBar::startValueBy(float duration, int value)
{
	this->startPercentageBy(duration, 1.0f * value / (_max_value - _min_value));
}

void NBar::startPercentageBy(float duration, float percentage)
{
	this->startUpdatePercentage(duration, percentage + this->getPercentage());
}

void NBar::startUpdatePercentage(float duration, float percentage)
{
	if (percentage >= 1.0f)
	{
		_to_percentage = 1.0f;
	}
	else if (percentage <= 0.0f)
	{
		_to_percentage = 0.0f;
	}
	else
	{
		_to_percentage = percentage;
	}

	if (_to_percentage == this->getPercentage())
	{
		return ;
	}

	_cur_time = 0.0f;
	_duration = duration;
	_previous_percentage = this->getPercentage();

	if (_is_update_percentage)
	{
		this->stopUpdatePercentage();
	}

	_is_update_percentage = true;
	this->schedule(schedule_selector(NBar::updatePercentage));	

	this->activate(Bar_Period::kBar_Began);
}

void NBar::updatePercentage(float dt)
{
	bool is_stop = false;
	_cur_time += dt;

	if (_cur_time >= _duration || this->getPercentage() == _to_percentage)
	{
		_cur_time = _duration;
		is_stop = true;		
	}

	float delta = (_to_percentage - _previous_percentage) * (_cur_time / _duration);
	this->setPercentage(delta + _previous_percentage);

	if (is_stop)
	{
		this->stopUpdatePercentage();
	}
}

void NBar::stopUpdatePercentage()
{
	this->unschedule(schedule_selector(NBar::updatePercentage));
	_is_update_percentage = false;

	this->activate(Bar_Period::kBar_End);
}

void NBar::setBarCallback(const Bar_Callback& bar_callback)
{
	_bar_callback = bar_callback;
}

void NBar::activate(const Bar_Period& bar_period)
{
	if (_bar_callback != nullptr)
	{
		_bar_callback(this, bar_period);
	}
}