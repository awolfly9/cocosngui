#ifndef __NCHECK_BOX_H__
#define __NCHECK_BOX_H__

#include "NGUI/NProtocol.h"
#include "NGUI/NButton.h"

class NCheckBox : public NButton
{
public:
	NCheckBox(void);
	~NCheckBox(void);

	static NCheckBox* create(const string& click_effect = "");
	virtual bool init(const string& click_effect);

public:
	void setCheckImage(const string& check_image);
	void setCheckSelectedImage(const string& check_selected_image);
	void setCheckDisableImage(const string& check_disable_image);

	void setCheckImage(Node* check_image);
	void setCheckSelectedImage(Node* check_selected_image);
	void setCheckDisableImage(Node* check_disable_image);

	bool isChecked() const;
	void setChecked(bool is_check);

	void setTextCheckColor(const Color3B& check_color);
	Color3B getTextCheckColor() const;

	void setTextCheckSelectedColor(const Color3B& check_selected_color);	
	Color3B getTextCheckSelectedColor() const;

	void setHideNormalImage(bool is_hide_normal_image);
	bool isHideNormalImage() const;

protected:
	virtual void activate(const Touch_Period& touch_period);
	virtual void updateWidget(const Widget_State& widget_state);

private:
	Node* _check_image;
	Node* _check_selected_image;
	Node* _check_disable_image;

	Color3B _text_check_color;
	Color3B _text_check_selected_color;

	bool _is_check;
	bool _is_hide_normal_image;
};

#endif // __NCHECK_BOX_H__
