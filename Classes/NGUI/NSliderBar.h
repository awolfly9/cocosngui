#ifndef __NSLIDER_BAR_H__
#define __NSLIDER_BAR_H__

#include "NGUI/NProgressBar.h"
#include "NGUI/NButton.h"

class NSliderBar : public NProgressBar
{
public:
	NSliderBar(void);
	~NSliderBar(void);

	static NSliderBar* create();
	static NSliderBar* create(const string& background, const string& progress_bar, NButton* slide_bar = nullptr);
	static NSliderBar* create(const string& background, const string& progress_bar, const string& normal_image, const string& selected_image = "", const string& disabled_image = "", const string& click_effect = "");

	virtual bool init(const string& background, const string& progress_bar, NButton* slide_bar);
	virtual bool init(const string& background, const string& progress_bar, const string& normal_image, const string& selected_image = "", const string& disabled_image = "", const string& click_effect = "");

	void setSliderBar(NButton* slide_bar);
	void setSliderBar(const string& normal_image, const string& selected_image = "", const string& disabled_image = "", const string& click_effect = "");

protected:
	virtual void interceptTouch(const Touch_Period& touch_period, Touch* touch, NWidget* widget);
	virtual void handlePressLogic(Touch *touch);
	virtual void handleMoveLogic(Touch *touch);
	virtual void handleReleaseLogic(Touch *touch);

	virtual void updateBar();

private:
	NButton* _slider_bar;
};

#endif // __NSLIDER_BAR_H__
