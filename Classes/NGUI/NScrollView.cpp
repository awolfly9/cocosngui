#include "NScrollView.h"

NScrollViewContainer::NScrollViewContainer(void)
{
	this->setUniqueID(NSCROLL_VIEW_CONTAINER);
}

NScrollViewContainer::~NScrollViewContainer(void)
{

}

bool NScrollViewContainer::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NWidget::init());

	this->setTouchEnabled(false);

	INIT_DO_WHILE_END
}

void NScrollViewContainer::reset()
{
	this->removeAllChildrenWithCleanup(true);
	this->ignoreAnchorPointForPosition(true);
	this->setAnchorPoint(Vec2(0, 0));
	this->setPosition(Vec2(0, 0));
}

// 
#define N_DECELERATION 1500
#define N_INTERRUPT_TOUCH_DISTANCE 5
#define N_TOUCH_MAX_DURATION 0.5
#define N_TOUCH_MIN_DERATION 0.01666
#define N_MAX_SCROLL_SPEED 1000  
#define N_BOUDCE_SPEED 500
#define N_MIN_SCROLL_SPEED 50
#define N_MOVE_ACTION_RATE 0.25
#define N_MOVE_ACTION_TAG 999

NScrollView::NScrollView(void)
	: _move_touch_point(Vec2(0, 0))
	, _end_touch_point(Vec2(0, 0))
	, _container(nullptr)
	, _direction(ScrollView_Direction::kScrollView_Dir_Vertical)
	, _scroll_speed(0.0f)
	, _drag_time(0.0f)
	, _drag_direction(Vec2(0, 0))
	, _is_bounce(true)
	, _is_bouncing(false)
	, _is_decelerate(true)
	, _is_decelerateing(false)
	, _is_auto_scroll(true)
	, _is_auto_scrolling(false)
	, _is_dragging(false)
	, _is_dragable(true)
	, _bounce_percent(Vec2(1.0f, 1.0f))
	, _scroll_bar(nullptr)
	, _scroll_bar_position_factor(0)
{
	this->setUniqueID(NSCROLL_VIEW);
}

NScrollView::~NScrollView(void)
{
}

NScrollView* NScrollView::create(const Size& view_size)
{
	NScrollView* ret = new NScrollView();

	if (ret && ret->init(view_size))
	{
		ret->autorelease();
	}
	else
	{
		SAFE_DELETE_NULL(ret);
	}

	return ret;
}

bool NScrollView::init(const Size& view_size)
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NWidget::init());

	this->setContentSize(view_size);

	_container = NScrollViewContainer::create();
	this->addChild(_container, false);
	_container->setContentSize(view_size);
	_container->setAnchorPoint(Vec2(0.0f, 0.0f));
	_container->setPosition(Vec2(0, 0));

	INIT_DO_WHILE_END
}

NScrollViewContainer* NScrollView::getContainer()
{
	return _container;
}

void NScrollView::setContainerPosition(const Vec2& position)
{
	Vec2 pos = validatePosition(position);
	_container->setPosition(pos);
	this->onScrolling();

	if (_scroll_bar != nullptr && ! _scroll_bar->isScrolling() && _direction != ScrollView_Direction::kScrollView_Dir_Both)
	{
		float percentage = 0;

		switch (_direction)
		{
		case ScrollView_Direction::kScrollView_Dir_Vertical:
			{
				if (_scroll_bar->getDirection() == Bar_Direction::kBar_Dir_ButtomToTop)
				{
					percentage = (-_container->getPositionY()) / (this->getContainerSize().height - this->getContentSize().height);
				}
				else if (_scroll_bar->getDirection() == Bar_Direction::kBar_Dir_TopToButtom)
				{
					percentage = 1 - (-_container->getPositionY()) / (this->getContainerSize().height - this->getContentSize().height);
				}
			}
			break;
		case ScrollView_Direction::kScrollView_Dir_Horizontal:
			{
				if (_scroll_bar->getDirection() == Bar_Direction::kBar_Dir_LeftToRight)
				{
					percentage = (-_container->getPositionX()) / (this->getContainerSize().width - this->getContentSize().width);
				}
				else if (_scroll_bar->getDirection() == Bar_Direction::kBar_Dir_RightToLeft)
				{
					percentage = (1 - (-_container->getPositionX()) / (this->getContainerSize().width - this->getContentSize().width));
				}				
			}
			break;
		default:
			break;
		}

		_scroll_bar->setPercentage(percentage);
	}
}

Vec2 NScrollView::getContainerPosition() const
{
	return _container->getPosition();
}

void NScrollView::setContainerSize(const Size& size)
{
	float width = MAX(size.width, this->getContentSize().width);
	float height = MAX(size.height, this->getContentSize().height);

	_container->setContentSize(Size(width, height));
}

Size NScrollView::getContainerSize() const
{
	return _container->getContentSize();
}

void NScrollView::setDirection(const ScrollView_Direction& direction)
{
	_direction = direction;
}

ScrollView_Direction NScrollView::getDirection() const
{
	return _direction;
}

void NScrollView::setBounce(bool is_bounce)
{
	_is_bounce = is_bounce;
}

bool NScrollView::isBounce() const
{
	return _is_bounce;
}

void NScrollView::setDecelerate(bool is_decelerate)
{	
	_is_decelerate = is_decelerate;
}

bool NScrollView::isDecelerate() const
{
	return _is_decelerate;
}

void NScrollView::setDragable(bool is_dragable)
{
	_is_dragable = is_dragable;
}

bool NScrollView::isDragable() const
{
	return _is_dragable;
}

void NScrollView::setAutoScroll(bool is_auto_scroll)
{
	_is_auto_scroll = is_auto_scroll;
}

bool NScrollView::isAutoscroll() const
{
	return _is_auto_scroll;
}

void NScrollView::setBouncePercent(const Vec2& bounce_percent)
{
	if (bounce_percent.x <= 1 && bounce_percent.x >= 0 && bounce_percent.y <= 1 && bounce_percent.y >= 0)
	{
		_bounce_percent = bounce_percent;
	}
}

Vec2 NScrollView::getBouncePercent() const
{
	return _bounce_percent;
}

void NScrollView::setScrollBar(NScrollBar* scroll_bar)
{
	if (scroll_bar == nullptr)
	{
		return ;
	}

	if (_scroll_bar != nullptr)
	{
		_scroll_bar->removeFromParent();
		_scroll_bar = nullptr;
	}

	_scroll_bar = scroll_bar;
	this->addChild(_scroll_bar, 1, false);
	_scroll_bar->setAnchorPoint(Vec2(0.5f, 0.5f));
	_scroll_bar->setBarCallback(CALLBACK_2(NScrollView::updateScrollBar, this));

	if (_direction == ScrollView_Direction::kScrollView_Dir_Vertical)
	{
		_scroll_bar->setPosition(Vec2(this->getContentSize().width - _scroll_bar->getContentSize().width / 2 - (this->getContentSize().width - _scroll_bar->getContentSize().width) * _scroll_bar_position_factor, this->getContentSize().height / 2));
	}
	else if (_direction == ScrollView_Direction::kScrollView_Dir_Horizontal)
	{
		_scroll_bar->setPosition(Vec2(this->getContentSize().width / 2,  _scroll_bar->getContentSize().height / 2 + (this->getContentSize().height - _scroll_bar->getContentSize().height) * _scroll_bar_position_factor));
	}	
}

void NScrollView::setScrollBar(const string& background, NButton* btn_scroll_bar)
{
	NScrollBar* scroll_bar = NScrollBar::create(background, btn_scroll_bar);
	this->setScrollBar(scroll_bar);
}

void NScrollView::setScrollBar(NButton* btn_scroll_bar)
{
	if (_scroll_bar != nullptr)
	{
		_scroll_bar->setScrollBar(btn_scroll_bar);
	}
}

void NScrollView::setScrollBar(const string& background, const string& normal_image, const string& selected_image , const string& disabled_image, const string& click_effect)
{
	if (_scroll_bar == nullptr)
	{
		NScrollBar* scroll_bar = NScrollBar::create(background);
		scroll_bar->setScrollBar(normal_image, selected_image, disabled_image, click_effect);
		this->setScrollBar(scroll_bar);
	}
	else
	{
		_scroll_bar->setBackground(background);
		_scroll_bar->setScrollBar(normal_image, selected_image, disabled_image, click_effect);
	}
}

NScrollBar* NScrollView::getScrollBar() const
{
	return _scroll_bar;
}

void NScrollView::setScrollBarDirection(const Bar_Direction& bar_direction)
{
	if (_scroll_bar != nullptr)
	{
		_scroll_bar->setDirection(bar_direction);

		if (bar_direction == Bar_Direction::kBar_Dir_RightToLeft || bar_direction == Bar_Direction::kBar_Dir_TopToButtom)
		{
			_scroll_bar->setPercentage(1.0f);
		}
	}
}

Bar_Direction NScrollView::getScrollBarDirection() const
{
	if (_scroll_bar != nullptr)
	{
		return _scroll_bar->getDirection();
	}

	return Bar_Direction::kBar_Dir_LeftToRight;
}

void NScrollView::setScrollBarPositionFactor(float factor)
{
	if (factor <= 1 && factor >= 0)
	{
		_scroll_bar_position_factor = factor;

		if (_scroll_bar != nullptr)
		{
			if (_direction == ScrollView_Direction::kScrollView_Dir_Vertical)
			{
				_scroll_bar->setPosition(Vec2(this->getContentSize().width - _scroll_bar->getContentSize().width / 2 - (this->getContentSize().width - _scroll_bar->getContentSize().width) * _scroll_bar_position_factor, this->getContentSize().height / 2));
			}
			else if (_direction == ScrollView_Direction::kScrollView_Dir_Horizontal)
			{
				_scroll_bar->setPosition(Vec2(this->getContentSize().width / 2,  _scroll_bar->getContentSize().height / 2 + (this->getContentSize().height - _scroll_bar->getContentSize().height) * _scroll_bar_position_factor));
			}	
		}
	}	
}

float NScrollView::getScrollBarPositionFactor() const
{
	return _scroll_bar_position_factor;
}

void NScrollView::setContentSize(const Size& view_size)
{
	if (_container == nullptr)
	{
		Node::setContentSize(view_size);
		return ;
	}

	float width = MAX(view_size.width, _container->getContentSize().width);
	float height = MAX(view_size.height, _container->getContentSize().height);

	Node::setContentSize(view_size);
	this->setContainerSize(Size(width, height));
}

bool NScrollView::onTouchBegan(Touch* touch, Event* event)
{
	if (! NWidget::onTouchBegan(touch, event))
	{
		return false;
	}

	this->handlePressLogic(touch);

	return true;
}

void NScrollView::onTouchMoved(Touch* touch, Event* event)
{
	NWidget::onTouchMoved(touch, nullptr);
	this->handleMoveLogic(touch);
}

void NScrollView::onTouchEnded(Touch* touch, Event* event)
{
	NWidget::onTouchEnded(touch, event);
	this->handleReleaseLogic(touch);
}

void NScrollView::onTouchCancelled(Touch* touch, Event* event)
{
	NWidget::onTouchCancelled(touch, event);

	_is_dragging = false;
}

void NScrollView::addChild(Node * child, bool is_container)
{
	return addChild(child, child->getZOrder(), child->getTag(), is_container);
}

void NScrollView::addChild(Node * child, int zorder, bool is_container)
{
	return addChild(child, child->getZOrder(), child->getTag(), is_container);
}

void NScrollView::addChild(Node* child, int zorder, int tag, bool is_container)
{
	if (is_container)
	{
		this->getContainer()->addChild(child, zorder, tag);
	}
	else
	{
		Node::addChild(child, zorder, tag);
	}
}

void NScrollView::interceptTouch(const Touch_Period& touch_period, Touch* touch, NWidget* widget)
{
	switch (touch_period)
	{
	case Touch_Period::kTouchBegan:
		{
			this->handlePressLogic(touch);
		}
		break;
	case Touch_Period::kTouchMoved:
		{
			float offset = touch->getLocation().distance(widget->getBeginTouchVec2());

			if (offset >= N_INTERRUPT_TOUCH_DISTANCE)
			{
				widget->setInterruptTouch(true);
				this->handleMoveLogic(touch);
			}			
		}
		break;
	case Touch_Period::kTouchEnded:
		{
			this->handleReleaseLogic(touch);
		}
		break;
	default:
		break;
	}
}

void NScrollView::handlePressLogic(Touch *touch)
{
	_began_touch_point = this->convertToNodeSpace(touch->getLocation());
	_move_touch_point = _began_touch_point;

	if (_is_dragable && ! _is_dragging)
	{
		_is_dragging = true;
		this->stopAutoScroll();
		this->stopAutoBounce();
	}
}

void NScrollView::handleMoveLogic(Touch *touch)
{
	Vec2 touch_point = this->convertToNodeSpace(touch->getLocation());
	Vec2 distance = touch_point - _move_touch_point;

	switch (_direction)
	{
	case ScrollView_Direction::kScrollView_Dir_Vertical:
		{
			distance.x = 0;
		}
		break;
	case ScrollView_Direction::kScrollView_Dir_Horizontal:
		{
			distance.y = 0;
		}
		break;
	default:
		break;
	}

	this->scrollContainer(distance);
	_move_touch_point = touch_point;
}

void NScrollView::handleReleaseLogic(Touch *touch)
{
	_is_dragging = false;

	if (_drag_time <= N_TOUCH_MIN_DERATION || _drag_time >= N_TOUCH_MAX_DURATION)
	{
		if (_is_bounce)
		{
			this->startAutoBounce(this->validatePosition(this->getContainerPosition(), true));
		}
	} 
	else
	{
		_end_touch_point = this->convertToNodeSpace(touch->getLocation());

		if (_is_auto_scroll)
		{
			Vec2 distance = _end_touch_point - _began_touch_point;

			switch (_direction)
			{
			case ScrollView_Direction::kScrollView_Dir_Vertical:
				{
					distance.x = 0;
					_scroll_speed = abs(distance.y / _drag_time);
				}
				break;
			case ScrollView_Direction::kScrollView_Dir_Horizontal:
				{
					distance.y = 0;
					_scroll_speed = abs(distance.x / _drag_time);
				}
				break;
			case ScrollView_Direction::kScrollView_Dir_Both:
				{
					_scroll_speed = abs(distance.getLength() / _drag_time);
				}
			default:
				break;
			}

			if (_scroll_speed > N_MAX_SCROLL_SPEED)
			{
				_scroll_speed = N_MAX_SCROLL_SPEED;
			}

			_drag_direction = distance.getNormalized();

			this->startAutoScroll();
		}
		else
		{
			if (_is_bounce)
			{
				this->startAutoBounce(this->validatePosition(this->getContainerPosition(), true));
			}
		}
	}

	this->onDraggingEnd();
}

void NScrollView::scrollContainer(const Vec2& distance)
{
	Vec2 position = this->getContainerPosition() + distance;

	if (! _is_dragging && ! this->rightfulPosition(position))
	{
		this->stopAutoScroll();
		return ;
	}

	this->setContainerPosition(position);
}

Vec2 NScrollView::validatePosition(const Vec2& position, bool is_ignoce_bounce)
{
	Vec2 point = Vec2(0, 0);

	if (is_ignoce_bounce)
	{
		point.x = MIN(position.x, 0);
		point.x = MAX(point.x, this->getContentSize().width - _container->getContentSize().width);

		point.y = MIN(position.y , 0);
		point.y = MAX(point.y, this->getContentSize().height - _container->getContentSize().height);
	}
	else
	{
		if (_is_bounce)
		{
			point.x = MIN(position.x, this->getContentSize().width * _bounce_percent.x);
			point.x = MAX(point.x, this->getContentSize().width * (1 - _bounce_percent.x) - _container->getContentSize().width);

			point.y = MIN(position.y , this->getContentSize().height * _bounce_percent.x);
			point.y = MAX(point.y, this->getContentSize().height * (1 - _bounce_percent.y) - _container->getContentSize().height);
		}
		else
		{
			point.x = MIN(position.x, 0);
			point.x = MAX(point.x, this->getContentSize().width - _container->getContentSize().width);

			point.y = MIN(position.y , 0);
			point.y = MAX(point.y, this->getContentSize().height - _container->getContentSize().height);
		}
	}

	return point;
}

bool NScrollView::rightfulPosition(const Vec2& position)
{
	bool is_rightful = true;

	switch (_direction)
	{
	case ScrollView_Direction::kScrollView_Dir_Vertical:
		{  
			if (_is_bounce)
			{
				if (position.y + _container->getContentSize().height <= this->getContentSize().height - this->getContentSize().height * _bounce_percent.y || position.y >= this->getContentSize().height * _bounce_percent.y)
				{
					is_rightful = false;
				}
			}
			else
			{
				if (position.y + _container->getContentSize().height <= this->getContentSize().height || position.y >= 0)
				{
					is_rightful = false;
				}
			}			
		}
		break;
	case ScrollView_Direction::kScrollView_Dir_Horizontal:
		{
			if (_is_bounce)
			{
				if (position.x + _container->getContentSize().width <= this->getContentSize().width - this->getContentSize().width * _bounce_percent.x || position.x >= this->getContentSize().width * _bounce_percent.x)
				{
					is_rightful = false;
				}
			}
			else
			{
				if (position.x + _container->getContentSize().width <= this->getContentSize().width || position.x >= 0)
				{
					is_rightful = false;
				}
			}
		}
		break;
	case ScrollView_Direction::kScrollView_Dir_Both:
		{		
			if (_is_bounce)
			{
				if (position.x + _container->getContentSize().width <= this->getContentSize().width - this->getContentSize().width * _bounce_percent.x || 
					position.y + _container->getContentSize().height <= this->getContentSize().height - this->getContentSize().height * _bounce_percent.y || 
					position.x >= this->getContentSize().width * _bounce_percent.x || 
					position.y >= this->getContentSize().height * _bounce_percent.y)
				{
					is_rightful = false;
				}
			}
			else
			{
				if (position.x + _container->getContentSize().width <= this->getContentSize().width || position.y + _container->getContentSize().height <= this->getContentSize().height || position.x >= 0 || position.y >= 0)
				{
					is_rightful = false;
				}
			}
		}
		break;
	default:
		break;
	}

	return is_rightful;
}

void NScrollView::startAutoScroll()
{
	if (_is_auto_scroll)
	{
		_is_auto_scrolling = true;
		this->schedule(schedule_selector(NScrollView::autoScrolling));
	}
	else
	{
		if (_is_bounce)
		{
			this->startAutoBounce(this->validatePosition(this->getContainerPosition(), true));
		}
	}
}

void NScrollView::autoScrolling(float dt)
{
	if (_is_dragging || _scroll_speed <= N_MIN_SCROLL_SPEED)
	{
		this->stopAutoScroll();
		return ;
	}

	if (_is_decelerate)
	{
		_scroll_speed -= (float)(N_DECELERATION * dt);
	}

	Vec2 scroll_distance = _drag_direction * _scroll_speed * dt;

	this->scrollContainer(scroll_distance);
}

void NScrollView::stopAutoScroll()
{
	this->unschedule(schedule_selector(NScrollView::autoScrolling));

	_is_auto_scrolling = false;
	_scroll_speed = 0.0;
	_drag_time = 0.0f;
	_drag_direction = Vec2(0, 0);

	if (! _is_dragging)
	{
		if (_is_bounce)
		{
			this->startAutoBounce(this->validatePosition(this->getContainerPosition(), true));
		}

		this->onScrollingEnd();
	}	
}

void NScrollView::startAutoBounce(const Vec2& position)
{
	if (position.equals(this->getContainerPosition()))
	{
		return ;
	}

	_container->stopActionByTag(N_MOVE_ACTION_TAG);
	Sequence* seq = CCSequence::create(
		EaseIn::create(
		MoveTo::create(this->getContainerPosition().distance(position) / N_BOUDCE_SPEED, position),
		N_MOVE_ACTION_RATE),
		CallFunc::create(this, callfunc_selector(NScrollView::stopAutoBounce)),
		nullptr);
	seq->setTag(N_MOVE_ACTION_TAG);

	_container->runAction(seq);

	_is_bouncing = true;
	this->schedule(schedule_selector(NScrollView::autoBouncing));
}

void NScrollView::autoBouncing(float dt)
{
	this->onScrolling();
}  

void NScrollView::stopAutoBounce()
{
	_is_bouncing = false;
	_container->stopActionByTag(N_MOVE_ACTION_TAG);
	this->unschedule(schedule_selector(NScrollView::autoBouncing));

	if (! _is_dragging)
	{
		this->onBouncingEnd();
	}
}

void NScrollView::updateScrollBar(Ref* object, const Bar_Period& bar_period)
{
	NScrollBar* scroll_bar = (NScrollBar*)(object);

	if (scroll_bar == nullptr)
	{
		return ;
	}

	if (scroll_bar->isScrolling())
	{
		Vec2 pos = Vec2(0, 0);

		switch (_direction)
		{
		case ScrollView_Direction::kScrollView_Dir_Vertical:
			{
				if (scroll_bar->getDirection() == Bar_Direction::kBar_Dir_ButtomToTop)
				{
					pos = Vec2(this->getContainerPosition().x, - (this->getContainerSize().height - this->getContentSize().height) * scroll_bar->getPercentage());
				}
				else if (scroll_bar->getDirection() == Bar_Direction::kBar_Dir_TopToButtom)
				{
					pos = Vec2(this->getContainerPosition().x, -(this->getContainerSize().height - this->getContentSize().height) + (this->getContainerSize().height - this->getContentSize().height) * scroll_bar->getPercentage());
				}
			}
			break;
		case ScrollView_Direction::kScrollView_Dir_Horizontal:
			{
				if (scroll_bar->getDirection() == Bar_Direction::kBar_Dir_LeftToRight)
				{
					pos = Vec2(- (this->getContainerSize().width - this->getContentSize().width) * scroll_bar->getPercentage(), this->getContainerPosition().y);
				}
				else if (scroll_bar->getDirection() == Bar_Direction::kBar_Dir_RightToLeft)
				{
					pos = Vec2(-(this->getContainerSize().width - this->getContentSize().width) + (this->getContainerSize().width - this->getContentSize().width) * scroll_bar->getPercentage(), this->getContainerPosition().y);
				}
			}
			break;
		default:
			break;
		}

		this->setContainerPosition(pos);
	}		
}

void NScrollView::visit(Renderer *renderer, const Mat4& parentTransform, uint32_t parentFlags)
{
	_before_visit.init(_globalZOrder);
	_before_visit.func = CALLBACK_0(NScrollView::beforeVisit, this);
	renderer->addCommand(&_before_visit);

	NWidget::visit(renderer, parentTransform, parentFlags);

	_after_visit.init(_globalZOrder);
	_after_visit.func = CALLBACK_0(NScrollView::afterVisit, this);
	renderer->addCommand(&_after_visit);

	if (_is_dragging)
	{
		_drag_time += Director::getInstance()->getDeltaTime();
	}
}

void NScrollView::beforeVisit()
{
	Rect rect = Rect(0, 0, this->getContentSize().width, this->getContentSize().height);
	rect = RectApplyAffineTransform(rect, nodeToWorldTransform());

	glEnable(GL_SCISSOR_TEST);
	auto glview = Director::getInstance()->getOpenGLView();
	glview->setScissorInPoints(rect.origin.x, rect.origin.y, rect.size.width, rect.size.height);
}

void NScrollView::afterVisit()
{
	glDisable(GL_SCISSOR_TEST);
}