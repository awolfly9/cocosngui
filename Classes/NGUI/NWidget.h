#ifndef __NWIDGET_H__
#define __NWIDGET_H__

#include "NGUI/NProtocol.h"
#include "NGUI/NScale9Sprite.h"

class NWidget : public Node
{
public:
	NWidget(void);
	~NWidget(void);

	static NWidget* create();
	virtual bool init();

	virtual void setCallback(const Touch_Callback& call_back);

	void setName(const string& name);
	string getName() const;

	void setTouchEnabled(bool is_touch_enable);
	bool isTouchEnabled() const;

	void setEnabled(bool is_enabled);
	bool isEnabled() const;

	void setClickEffect(const string& click_effect);
	string getClickEffect() const;

	void setPlayClickEffect(bool is_play_click_effect);
	bool isPlayClickEffect() const;
	
	void setInterruptTouch(bool is_interrupt);
	bool isInterruptTouch() const;

	void setWidgetState(const Widget_State& widget_state);
	Widget_State getWidgetState() const;
	
	Vec2 getBeginTouchVec2() const;

	string getUniqueID() const;
	
	bool checkNodeVisible(Node* node);

	virtual bool onTouchBegan(Touch* touch, Event* event);
	virtual void onTouchMoved(Touch* touch, Event* event);
	virtual void onTouchEnded(Touch* touch, Event* event);
	virtual void onTouchCancelled(Touch* touch, Event* event);

protected:
	void setUniqueID(const string& unique_id);
	
	virtual void interceptTouch(const Touch_Period& touch_period, Touch* touch, NWidget* widget);

	virtual void activate(const Touch_Period& touch_period);
	virtual void updateWidget(const Widget_State& widget_state) { };

	virtual bool hitWidget(const Vec2& point);

protected:
	Vec2 _began_touch_point;

private:
	string _name;
	string _click_effect;
	string _unique_id;

	bool _is_play_click_effect;
	bool _is_touch_enabled;
	bool _is_enabled;
	bool _is_interrupt_touch;

	Touch_Callback _touch_call_back;
	Widget_State _widgt_state;

	EventListenerTouchOneByOne* _touch_listener;
};

#endif // __NWIDGET_H__
