#ifndef __NRICH_TEXT_H__
#define __NRICH_TEXT_H__

#include "NGUI/NProtocol.h"

typedef vector<Node*> Rich_text_line;

class NRichText : public Node
{
public:
	NRichText(void);
	~NRichText(void);

	CREATE_FUNC(NRichText);
	virtual bool init();

	void setMaxLineLength(int max_line_length);
	int getMaxLineLength() const;

	void setVerticalSpace(float vertical_space);
	float getVerticalSpace() const;

	virtual void insertElement(const string str, const string font_name = "Arial", float font_size = 30, const Color3B& color = Color3B(255, 255, 255), bool is_next_line = false);
	virtual void insertElement(Node* node, int length = 1, bool is_next_line = false);

	void removeAllElement();
	void reloadData();

private:
	void pushElement(Node* node);
	void pushLine();

private:
	int _max_line_length;
	float _vertical_space;
	int _cursor;

	vector<Rich_text_line*>* _rich_text_lines;
};

#endif // __NRICH_TEXT_H__
