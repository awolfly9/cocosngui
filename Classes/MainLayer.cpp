#include "MainLayer.h"
#include "BasicLayer.h"
#include "Test/TestNButton.h"
#include "Test/TestNCheckBox.h"
#include "Test/TestNScrollView.h"
#include "Test/TestNListView.h"
#include "Test/TestNRichText.h"
#include "Test/TestNProgressBar.h"
#include "Test/TestNSliderBar.h"
#include "Test/TestNScrollBar.h"
#include "Test/TestNLabel.h"
#include "Test/TestNTableView.h"
#include "Test/TestNPageView.h"
#include "Test/TestNGridView.h"
#include "Test/TestNMessageTips.h"
#include "Test/TestNJoystick.h"
#include "Test/TestNAudioManager.h"
#include "Test/TestNSwitchButton.h"
#include "Test/TestNXMLManager.h"

MainLayer::MainLayer(void)
{
}

MainLayer::~MainLayer(void)
{
}

bool MainLayer::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! Layer::init());

	NListView* list_view = NListView::create(CCSize(VISIBLE_SIZE.width* 0.5f, VISIBLE_SIZE.height * 0.8f));
	this->addChild(list_view);
	list_view->setPosition(VISIBLE_SIZE_CENTER);
	list_view->setAlignType(ListView_Align_Type::kListView_Align_Vertical_Center);

	NButton* btn_button = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_button);
	btn_button->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_button->setTag((int)Widget_ID::kNButton);
	btn_button->setText("button");

	NButton* btn_check_box = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_check_box);
	btn_check_box->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_check_box->setTag((int)Widget_ID::kNCheckBox);
	btn_check_box->setText("check box");

	NButton* btn_label= NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_label);
	btn_label->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_label->setTag((int)Widget_ID::kNLabel);
	btn_label->setText("label");

	NButton* btn_scroll_view = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_scroll_view);
	btn_scroll_view->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_scroll_view->setTag((int)Widget_ID::kNScrollView);
	btn_scroll_view->setText("scroll view");

	NButton* btn_list_view = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_list_view);
	btn_list_view->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_list_view->setTag((int)Widget_ID::kNListView);
	btn_list_view->setText("list view");

	NButton* btn_rich_text = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_rich_text);
	btn_rich_text->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_rich_text->setTag((int)Widget_ID::kNRichText);
	btn_rich_text->setText("rich text");

	NButton* btn_progress_bar = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_progress_bar);
	btn_progress_bar->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_progress_bar->setTag((int)Widget_ID::kNProgressBar);
	btn_progress_bar->setText("progress bar");

	NButton* btn_slider_bar = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_slider_bar);
	btn_slider_bar->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_slider_bar->setTag((int)Widget_ID::kNSliderBar);
	btn_slider_bar->setText("slider bar");

	NButton* btn_scroll_bar = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_scroll_bar);
	btn_scroll_bar->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_scroll_bar->setTag((int)Widget_ID::kNScrollBar);
	btn_scroll_bar->setText("scroll bar");
	
	NButton* btn_table_view = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_table_view);
	btn_table_view->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_table_view->setTag((int)Widget_ID::kNTableView);
	btn_table_view->setText("table view");

	NButton* btn_page_view = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_page_view);
	btn_page_view->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_page_view->setTag((int)Widget_ID::kNPageView);
	btn_page_view->setText("page view");

	NButton* btn_grid_view = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_grid_view);
	btn_grid_view->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_grid_view->setTag((int)Widget_ID::kNGridView);
	btn_grid_view->setText("grid view");

	NButton* btn_message_tips = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_message_tips);
	btn_message_tips->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_message_tips->setTag((int)Widget_ID::kNMessageTips);
	btn_message_tips->setText("message tips");

	NButton* btn_joystick = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_joystick);
	btn_joystick->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_joystick->setTag((int)Widget_ID::kNNJoystick);
	btn_joystick->setText("joystick");

	NButton* btn_audio_manager = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_audio_manager);
	btn_audio_manager->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_audio_manager->setTag((int)Widget_ID::kNAudioManager);
	btn_audio_manager->setText("audio manager");

	NButton* btn_switch_button = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_switch_button);
	btn_switch_button->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_switch_button->setTag((int)Widget_ID::kNSwitchButton);
	btn_switch_button->setText("switch button");

	NButton* btn_xml_namager = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	list_view->insertNodeAtLast(btn_xml_namager);
	btn_xml_namager->setCallback(CALLBACK_2(MainLayer::btnCallback, this));
	btn_xml_namager->setTag((int)Widget_ID::kNXMLManager);
	btn_xml_namager->setText("xml manager");

	list_view->reloadData();
	INIT_DO_WHILE_END
}

void MainLayer::btnCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		switch (Widget_ID(btn->getTag()))  
		{
		case Widget_ID::kNButton:
			{
				NButton_Callback* layer = NButton_Callback::create();

				// 这里的 bug 得到解决。
				/*
				 * 在 removeFromParent 中调用了每一个子节点的析构函数
				 * 
				 */
				this->retain();
				this->removeFromParent();
				this->autorelease();
				
				Director::getInstance()->getRunningScene()->addChild(layer);
				//Scene* scene = Scene::create();
				//scene->addChild(layer);
				//Director::getInstance()->replaceScene(scene);
			}
			break;
		case Widget_ID::kNCheckBox:
			{
				NCheckBox_Callback* layer = NCheckBox_Callback::create();
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);
			}
			break;
		case Widget_ID::kNLabel:
			{
				/*NLabel_Horizontal* layer = NLabel_Horizontal::create();
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);*/
			}
			break;
		case Widget_ID::kNScrollView:
			{
				NScrollView_Drag* layer = NScrollView_Drag::create();			
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);;
			}
			break;
		case Widget_ID::kNListView:
			{
				NListView_Vertical* layer = NListView_Vertical::create();			
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);
			}
			break;
		case Widget_ID::kNRichText:
			{
				NRichText_VerticalSpace* layer = NRichText_VerticalSpace::create();			
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);
			}
			break;
		case Widget_ID::kNProgressBar:
			{
				NProgressBar_Horizontal* layer = NProgressBar_Horizontal::create();
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);
			}
			break;
		case Widget_ID::kNSliderBar:
			{
				NSliderBar_Horizontal* layer = NSliderBar_Horizontal::create();
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);
			}
			break;
		case Widget_ID::kNScrollBar:
			{
				NScrollBar_Horizontal* layer = NScrollBar_Horizontal::create();
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);
			}
			break;
		case Widget_ID::kNTableView:
			{
				NTableView_Horizontal* layer = NTableView_Horizontal::create();
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);
			}
			break;
		case Widget_ID::kNPageView:
			{
				NPageView_Horizontal* layer = NPageView_Horizontal::create();
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);
			}
			break;
		case Widget_ID::kNGridView:
			{
				NGridView_Vertical* layer = NGridView_Vertical::create();
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);
			}
			break;
		case Widget_ID::kNMessageTips:
			{
				NMessageTips_Info* layer = NMessageTips_Info::create();
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);
			}
			break;
		case Widget_ID::kNNJoystick:
			{
				NJoystick_Handle* layer = NJoystick_Handle::create();
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);
			}
			break;
		case Widget_ID::kNAudioManager:
			{
				NAudioManager_Async_Effect* layer = NAudioManager_Async_Effect::create();
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);
			}
			break;
		case Widget_ID::kNSwitchButton:
			{
				NSwitchButton_Show* layer = NSwitchButton_Show::create();
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);
			}
			break;
		case Widget_ID::kNXMLManager:
			{
				NXMLManager_Get* layer = NXMLManager_Get::create();
				Scene* scene = Scene::create();
				scene->addChild(layer);
				Director::getInstance()->replaceScene(scene);
			}
			break;
		default:
			break;
		}
	}
}