#include "MainScene.h"
#include "MainLayer.h"


MainScene::MainScene(void)
{
}

MainScene::~MainScene(void)
{
}

bool MainScene::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! Scene::init());

	MainLayer* layer = MainLayer::create();
	this->addChild(layer);

	INIT_DO_WHILE_END
}