#ifndef __MAIN_LAYER_H__
#define __MAIN_LAYER_H__

#include "NGUI/NProtocol.h"

enum class Widget_ID
{
	kNButton = 1,
	kNCheckBox,
	kNScrollView, 
	kNListView,
	kNRichText,
	kNProgressBar,
	kNSliderBar,
	kNScrollBar,
	kNLabel,
	kNTableView,
	kNPageView,
	kNGridView,
	kNMessageTips,
	kNNJoystick,
	kNAudioManager,
	kNSwitchButton,
	kNXMLManager,
};

class MainLayer : public Layer
{
public:
	MainLayer(void);
	~MainLayer(void);

	CREATE_FUNC(MainLayer);
	virtual bool init();

private:
	void btnCallback(Ref* object, Touch_Period touch_period);
};

#endif // __MAIN_LAYER_H__