#include "BasicLayer.h"
#include "MainLayer.h"

BasicLayer::BasicLayer(void)
	: _btn_next(nullptr)
	, _btn_back(nullptr)
	, _btn_again(nullptr)
	, _lbl_title(nullptr)
	, _lbl_description(nullptr)
	, _index(-1)
{
}

BasicLayer::~BasicLayer(void)
{
}

bool BasicLayer::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! Layer::init());

	_btn_again = NButton::create("again1.png", "again2.png");
	this->addChild(_btn_again);
	_btn_again->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.1f));
	_btn_again->setCallback(CALLBACK_2(BasicLayer::btnAgainCallabck, this));

	_btn_back = NButton::create("back1.png", "back2.png");
	this->addChild(_btn_back);
	_btn_back->setPosition(ccp(_btn_again->getPositionX() - _btn_again->getContentSize().width, _btn_again->getPositionY()));
	_btn_back->setAnchorPoint(ccp(1.0f, 0.5f));
	_btn_back->setCallback(CALLBACK_2(BasicLayer::btnbackCallback, this));

	_btn_next = NButton::create("next1.png", "next2.png");
	this->addChild(_btn_next);
	_btn_next->setPosition(ccp(_btn_again->getPositionX() + _btn_again->getContentSize().width, _btn_again->getPositionY()));
	_btn_next->setAnchorPoint(ccp(0.0f, 0.5f));
	_btn_next->setCallback(CALLBACK_2(BasicLayer::btnNextCallback, this));

	_btn_menu = NButton::create("btn_1.png", "btn_2.png", "btn_3.png");
	this->addChild(_btn_menu);
	_btn_menu->setCallback(CALLBACK_2(BasicLayer::btnMenuCallback, this));
	_btn_menu->setAnchorPoint(ccp(1.0f, 0.0f));
	_btn_menu->setPosition(POS_BY_VISIBLE_SIZE(1.0f, 0.0f));

	_lbl_title = LabelTTF::create("", "Arial", 30);
	_lbl_title->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.9f));
	this->addChild(_lbl_title);

	_lbl_description = LabelTTF::create("", "Arial", 30);
	_lbl_description->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.8f));
	this->addChild(_lbl_description);

	INIT_DO_WHILE_END
}

void BasicLayer::setDescription(const string& description)
{
	_lbl_description->setString(description.c_str());
}

void BasicLayer::setTitle(const string& title)
{
	_lbl_title->setString(title.c_str());
}

void BasicLayer::btnMenuCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		MainLayer* layer = MainLayer::create();
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void BasicLayer::setIndex(int index)
{
	_index = index;
}

int BasicLayer::getIndex() const
{
	return _index;
}