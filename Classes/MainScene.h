#ifndef __MAIN_SCENE_H__
#define __MAIN_SCENE_H__

#include "NGUI/NProtocol.h"

class MainScene : public Scene
{
public:
	MainScene(void);
	~MainScene(void);

	CREATE_FUNC(MainScene);
	virtual bool init();
};

#endif // __MAIN_SCENE_H__