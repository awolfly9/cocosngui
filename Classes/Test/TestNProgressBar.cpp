#include "TestNProgressBar.h"


TestNProgressBar::TestNProgressBar(void)
{
	this->setIndex(-1);
}

TestNProgressBar::~TestNProgressBar(void)
{
}

bool TestNProgressBar::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

void TestNProgressBar::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if(touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextNProgressBarLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNProgressBar::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if(touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackNProgressBarLayer(_index);
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNProgressBar::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if(touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainNProgressBarLayer(_index);
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

/////////////////////////////////////////
NProgressBar_Horizontal::NProgressBar_Horizontal()
{
	this->setIndex(0);
}

NProgressBar_Horizontal::~NProgressBar_Horizontal()
{

}

bool NProgressBar_Horizontal::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNProgressBar::init());

	NScale9Sprite* bg = NScale9Sprite::create("background3.png");
	this->addChild(bg, -1);
	bg->setContentSize(Size(VISIBLE_SIZE.width * 0.85f, VISIBLE_SIZE.height * 0.85f));
	bg->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));

	_progress_bar = NProgressBar::create("progress_bg.png", "progress.png");
	this->addChild(_progress_bar);
	_progress_bar->setDirection(Bar_Direction::kBar_Dir_LeftToRight);
	_progress_bar->setPercentage(0.2f);
	_progress_bar->setPosition(VISIBLE_SIZE_CENTER);
	_progress_bar->setMaxValue(100);
	_progress_bar->setMinValue(0);
	_progress_bar->setBarCallback(CALLBACK_2(NProgressBar_Horizontal::barShowCallback, this));

	NButton* btn_exchange_direction = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_exchange_direction);
	btn_exchange_direction->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.85f));
	btn_exchange_direction->setCallback(CALLBACK_2(NProgressBar_Horizontal::btnDirectionCallback, this));
	btn_exchange_direction->setText("enchange direction");
	
	NButton* btn_add_progress_value = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_add_progress_value);
	btn_add_progress_value->setPosition(POS_BY_VISIBLE_SIZE(0.2f, 0.3f));
	btn_add_progress_value->setCallback(CALLBACK_2(NProgressBar_Horizontal::btnAddProgressCallback, this));
	btn_add_progress_value->setTag(1);
	btn_add_progress_value->setText("add progress value");

	NButton* btn_add_progress_percentage = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_add_progress_percentage);
	btn_add_progress_percentage->setPosition(POS_BY_VISIBLE_SIZE(0.2f, 0.7f));
	btn_add_progress_percentage->setCallback(CALLBACK_2(NProgressBar_Horizontal::btnAddProgressCallback, this));
	btn_add_progress_percentage->setTag(2);
	btn_add_progress_percentage->setText("add progress perventage");

	NButton* btn_reduce_progress_value = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_reduce_progress_value);
	btn_reduce_progress_value->setPosition(POS_BY_VISIBLE_SIZE(0.8f, 0.3f));
	btn_reduce_progress_value->setCallback(CALLBACK_2(NProgressBar_Horizontal::btnReduceProgressCallback, this));
	btn_reduce_progress_value->setTag(1);
	btn_reduce_progress_value->setText("reduce progress value");

	NButton* btn_reduce_progress_percentage = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_reduce_progress_percentage);
	btn_reduce_progress_percentage->setPosition(POS_BY_VISIBLE_SIZE(0.8f, 0.7f));
	btn_reduce_progress_percentage->setCallback(CALLBACK_2(NProgressBar_Horizontal::btnReduceProgressCallback, this));
	btn_reduce_progress_percentage->setTag(2);
	btn_reduce_progress_percentage->setText("reduce progress percentage");

	NButton* btn_stop_progress = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_stop_progress);
	btn_stop_progress->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.15f));
	btn_stop_progress->setCallback(CALLBACK_2(NProgressBar_Horizontal::btnStopCallback, this));
	btn_stop_progress->setText("stop progress");

	INIT_DO_WHILE_END
}

void NProgressBar_Horizontal::btnAddProgressCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		if (btn->getTag() == 1)
		{
			_progress_bar->startValueBy(0.2f, 5);
		}
		else
		{
			_progress_bar->startPercentageTo(2.0f, 1.0f);
		}
	}
}

void NProgressBar_Horizontal::btnReduceProgressCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		if (btn->getTag() == 1)
		{
			_progress_bar->startValueBy(0.2f, -5);
		}
		else
		{
			_progress_bar->startPercentageTo(2.0f, 0.0f);
		}
	}
}

void NProgressBar_Horizontal::btnDirectionCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		if (_progress_bar->getDirection() == Bar_Direction::kBar_Dir_LeftToRight)
		{
			_progress_bar->setDirection(Bar_Direction::kBar_Dir_RightToLeft);
		}
		else
		{
			_progress_bar->setDirection(Bar_Direction::kBar_Dir_LeftToRight);
		}
	}
}

void NProgressBar_Horizontal::btnStopCallback(Ref* object, Touch_Period touch_period)
{
	if ( touch_period == Touch_Period::kTouchEnded)
	{
		_progress_bar->stopUpdatePercentage();
	}
}

void NProgressBar_Horizontal::barShowCallback(Ref* object, Bar_Period bar_period)
{
	NProgressBar* bar = (NProgressBar*)object;
	switch (bar_period)
	{
	case Bar_Period::kBar_Began:
		{
			log("bar began");
			log("Progress Bar current value : %d, current percentage : %f", bar->getCurrentValue(), bar->getPercentage());
		}
		break;
	case Bar_Period::kBar_Move:
		{
			log("bar change");
			log("Progress Bar current value : %d, current percentage : %f", bar->getCurrentValue(), bar->getPercentage());
		}
		break;
	case Bar_Period::kBar_End:
		{
			log("bar end");
			log("Progress Bar current value : %d, current percentage : %f", bar->getCurrentValue(), bar->getPercentage());
		}
		break;
	default:
		break;
	}
}

///////////////////////////////////////////////
NProgressBar_Vertical::NProgressBar_Vertical()
{
	this->setIndex(1);
}

NProgressBar_Vertical::~NProgressBar_Vertical()
{

}

bool NProgressBar_Vertical::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNProgressBar::init());

	NScale9Sprite* bg = NScale9Sprite::create("background3.png");
	this->addChild(bg, -1);
	bg->setContentSize(Size(VISIBLE_SIZE.width * 0.85f, VISIBLE_SIZE.height * 0.85f));
	bg->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));

	_progress_bar = NProgressBar::create("progress_v_bg.png", "progress_v.png");
	this->addChild(_progress_bar);
	_progress_bar->setDirection(Bar_Direction::kBar_Dir_ButtomToTop);
	_progress_bar->setPercentage(0.2f);
	_progress_bar->setPosition(VISIBLE_SIZE_CENTER);
	_progress_bar->setMaxValue(100);
	_progress_bar->setMinValue(0);

	NButton* btn_exchange_direction = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_exchange_direction);
	btn_exchange_direction->setPosition(POS_BY_VISIBLE_SIZE(0.15f, 0.5f));
	btn_exchange_direction->setCallback(CALLBACK_2(NProgressBar_Vertical::btnDirectionCallback, this));
	btn_exchange_direction->setText("enchange direction");

	NButton* btn_add_progress_value = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_add_progress_value);
	btn_add_progress_value->setPosition(POS_BY_VISIBLE_SIZE(0.3f, 0.75f));
	btn_add_progress_value->setCallback(CALLBACK_2(NProgressBar_Vertical::btnAddProgressCallback, this));
	btn_add_progress_value->setTag(1);
	btn_add_progress_value->setText("add progress value");

	NButton* btn_add_progress_percentage = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_add_progress_percentage);
	btn_add_progress_percentage->setPosition(POS_BY_VISIBLE_SIZE(0.7f, 0.75f));
	btn_add_progress_percentage->setCallback(CALLBACK_2(NProgressBar_Vertical::btnAddProgressCallback, this));
	btn_add_progress_percentage->setTag(2);
	btn_add_progress_percentage->setText("add progress perventage");

	NButton* btn_reduce_progress_value = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_reduce_progress_value);
	btn_reduce_progress_value->setPosition(POS_BY_VISIBLE_SIZE(0.3f, 0.25f));
	btn_reduce_progress_value->setCallback(CALLBACK_2(NProgressBar_Vertical::btnReduceProgressCallback, this));
	btn_reduce_progress_value->setTag(1);
	btn_reduce_progress_value->setText("reduce progress value");

	NButton* btn_reduce_progress_percentage = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_reduce_progress_percentage);
	btn_reduce_progress_percentage->setPosition(POS_BY_VISIBLE_SIZE(0.7f, 0.25f));
	btn_reduce_progress_percentage->setCallback(CALLBACK_2(NProgressBar_Vertical::btnReduceProgressCallback, this));
	btn_reduce_progress_percentage->setTag(2);
	btn_reduce_progress_percentage->setText("reduce progress percentage");

	NButton* btn_stop_progress = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_stop_progress);
	btn_stop_progress->setPosition(POS_BY_VISIBLE_SIZE(0.85f, 0.5f));
	btn_stop_progress->setCallback(CALLBACK_2(NProgressBar_Vertical::btnStopCallback, this));
	btn_stop_progress->setText("stop progress");

	INIT_DO_WHILE_END
}

void NProgressBar_Vertical::btnAddProgressCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		if (btn->getTag() == 1)
		{
			_progress_bar->startValueBy(0.2f, 5);
		}
		else
		{
			_progress_bar->startPercentageTo(2.0f, 1.0f);
		}
	}
}

void NProgressBar_Vertical::btnReduceProgressCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		if (btn->getTag() == 1)
		{
			_progress_bar->startValueBy(0.2f, -5);
		}
		else
		{
			_progress_bar->startPercentageTo(2.0f, 0.0f);
		}
	}
}

void NProgressBar_Vertical::btnDirectionCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		if (_progress_bar->getDirection() == Bar_Direction::kBar_Dir_ButtomToTop)
		{
			_progress_bar->setDirection(Bar_Direction::kBar_Dir_TopToButtom);
		}
		else
		{
			_progress_bar->setDirection(Bar_Direction::kBar_Dir_ButtomToTop);
		}
	}
}

void NProgressBar_Vertical::btnStopCallback(Ref* object, Touch_Period touch_period)
{
	if ( touch_period == Touch_Period::kTouchEnded)
	{
		_progress_bar->stopUpdatePercentage();
	}
}