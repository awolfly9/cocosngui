#ifndef __TEST_NPAGE_VIEW_H__
#define __TEST_NPAGE_VIEW_H__

#include "BasicLayer.h"

#define MAX_PAGE_VIEW_LAYER_INDEX 6

/**
TestNPageView
*/
class TestNPageView : public BasicLayer
{
public:
	TestNPageView(void);
	~TestNPageView(void);

	CREATE_FUNC(TestNPageView);
	virtual bool init();

protected:
	Ref* loadCell(Ref* object, int index);

	void pageChangedCallback(Ref* object, int index);

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);

private:
	LabelTTF* _lbl_index;
};

/**
NPageView_Horizontal
*/
class NPageView_Horizontal : public TestNPageView
{
public:
	NPageView_Horizontal();
	~NPageView_Horizontal();

	CREATE_FUNC(NPageView_Horizontal);
	virtual bool init();

protected:
	NPageView* _page_view;
};

/**
NPageView_Horizontal_Relocate
*/
class NPageView_Horizontal_Relocate : public NPageView_Horizontal
{
public:
	NPageView_Horizontal_Relocate();
	~NPageView_Horizontal_Relocate();

	CREATE_FUNC(NPageView_Horizontal_Relocate);
	virtual bool init();
};

/**
NPageView_Vertical
*/
class NPageView_Vertical : public TestNPageView
{
public:
	NPageView_Vertical();
	~NPageView_Vertical();

	CREATE_FUNC(NPageView_Vertical);
	virtual bool init();

protected:
	NPageView* _page_view;
};

/**
NPageView_Vertical_Relocate
*/
class NPageView_Vertical_Relocate : public NPageView_Vertical
{
public:
	NPageView_Vertical_Relocate();
	~NPageView_Vertical_Relocate();

	CREATE_FUNC(NPageView_Vertical_Relocate);
	virtual bool init();
};

/**
NPageView_Horizontal_Tips_Index
*/
class NPageView_Horizontal_Tips_Index : public TestNPageView
{
public:
	NPageView_Horizontal_Tips_Index();
	~NPageView_Horizontal_Tips_Index();

	CREATE_FUNC(NPageView_Horizontal_Tips_Index);
	virtual bool init();

protected:
	NPageView* _page_view;
};

/**
NPageView_Vertical_Tips_Index
*/
class NPageView_Vertical_Tips_Index : public TestNPageView
{
public:
	NPageView_Vertical_Tips_Index();
	~NPageView_Vertical_Tips_Index();

	CREATE_FUNC(NPageView_Vertical_Tips_Index);
	virtual bool init();

protected:
	NPageView* _page_view;
};

////////////////////////////////////////////////////////
static Layer* createNPageViewLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NPageView_Horizontal::create();
		}
		break;
	case 1:
		{
			return NPageView_Horizontal_Relocate::create();
		}
		break;
	case 2:
		{
			return NPageView_Vertical::create();
		}
		break;
	case 3:
		{
			return NPageView_Vertical_Relocate::create();
		}
		break;
	case 4:
		{
			return NPageView_Horizontal_Tips_Index::create();
		}
		break;
	case 5:
		{
			return NPageView_Vertical_Tips_Index::create();
		}
		break;
	default:
		break;
	}

	return nullptr;
}

static Layer* onNextNPageViewLayer(int index)
{
	index ++;

	if (index == MAX_PAGE_VIEW_LAYER_INDEX)
	{
		index = 0;
	}

	return createNPageViewLayer(index);
}

static Layer* onBackNPageViewLayer(int index)
{
	index --;

	if (index == -1)
	{
		index = MAX_PAGE_VIEW_LAYER_INDEX - 1;
	}

	return createNPageViewLayer(index);
}

static Layer* onAgainNPageViewLayer(int index)
{
	return createNPageViewLayer(index);
}

#endif // __TEST_NPAGE_VIEW_H__
