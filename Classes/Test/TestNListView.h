#ifndef __TEST_NLISTVIEW_H__
#define __TEST_NLISTVIEW_H__

#include "BasicLayer.h"

#define MAX_LIST_VIEW_LAYER_INDEX 2

/**
TestNListView
*/
class TestNListView : public BasicLayer
{
public:
	TestNListView(void);
	~TestNListView(void);

	CREATE_FUNC(TestNListView);
	virtual bool init();

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};

/**
NListView_Vertical
*/
class NListView_Vertical : public TestNListView
{
public:
	NListView_Vertical(void);
	~NListView_Vertical(void);

	CREATE_FUNC(NListView_Vertical);
	virtual bool init();

private:
	void btnAlignCallback(Ref* object, Touch_Period touch_period);
	void btnInsertCallback(Ref* object, Touch_Period touch_period);
	void btnRemoveCallback(Ref*object, Touch_Period touch_period);

	Node* createRandomNode();

private:
	NListView* _list_view;

	int _count;
};

/**
NListView_Horizontal
*/
class NListView_Horizontal : public TestNListView
{
public:
	NListView_Horizontal(void);
	~NListView_Horizontal(void);

	CREATE_FUNC(NListView_Horizontal);
	virtual bool init();

private:
	void btnAlignCallback(Ref* object, Touch_Period touch_period);
	void btnInsertCallback(Ref* object, Touch_Period touch_period);
	void btnRemoveCallback(Ref*object, Touch_Period touch_period);

	Node* createRandomNode();

private:
	NListView* _list_view;

	int _count;
};

//////////////////////////////////////////////
static Layer* createListViewLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NListView_Vertical::create();
		}
		break;
	case 1:
		{
			return NListView_Horizontal::create();
		}
		break;
	default:
		break;
	}

	return nullptr;
}

static Layer* onNextListViewLayer(int index)
{
	index ++;

	if (index == MAX_LIST_VIEW_LAYER_INDEX)
	{
		index = 0;
	}

	return createListViewLayer(index);
}

static Layer* onBackListViewLayer(int index)
{
	index --;

	if (index == -1)
	{
		index = MAX_LIST_VIEW_LAYER_INDEX - 1;
	}

	return createListViewLayer(index);
}

static Layer* onAgainListViewLayer(int index)
{
	return createListViewLayer(index);
}

#endif // __TEST_NLISTVIEW_H__
