#ifndef __TEXT_NXMLMANAGER_H__
#define __TEXT_NXMLMANAGER_H__

#include "BasicLayer.h" 

#define MAX_XML_MANAGER_LAYER_INDEX 4

/**
TestNXMLManager
*/
class TestNXMLManager : public BasicLayer 
{
public:
	TestNXMLManager(void);
	~TestNXMLManager(void);

	CREATE_FUNC(TestNXMLManager);
	virtual bool init();

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};

/**
NXMLManager_Get
*/
class NXMLManager_Get : public TestNXMLManager
{
public: 
	NXMLManager_Get();
	~NXMLManager_Get();

	CREATE_FUNC(NXMLManager_Get);
	virtual bool init();
};

/**
NXMLManager_Set
*/
class NXMLManager_Add : public TestNXMLManager
{
public: 
	NXMLManager_Add();
	~NXMLManager_Add();

	CREATE_FUNC(NXMLManager_Add);
	virtual bool init();
};

/**
NXMLManager_Set
*/
class NXMLManager_Set : public TestNXMLManager
{
public: 
	NXMLManager_Set();
	~NXMLManager_Set();

	CREATE_FUNC(NXMLManager_Set);
	virtual bool init();
};

/**
NXMLManager_Delete
*/
class NXMLManager_Delete : public TestNXMLManager
{
public: 
	NXMLManager_Delete();
	~NXMLManager_Delete();

	CREATE_FUNC(NXMLManager_Delete);
	virtual bool init();
};



////////////////////////////////////////////////////////
static Layer* createNXMLManagerLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NXMLManager_Get::create();
		}
		break;
	case 1:
		{
			return NXMLManager_Add::create();
		}
		break;
	case 2:
		{
			return NXMLManager_Set::create();
		}
		break;
	case 3:
		{
			return NXMLManager_Delete::create();
		}
		break;
	default:
		break;
	}

	return nullptr;
}

static Layer* onNextNXMLManagerLayer(int index)
{
	index ++;

	if (index == MAX_XML_MANAGER_LAYER_INDEX)
	{
		index = 0;
	}

	return createNXMLManagerLayer(index);
}

static Layer* onBackNXMLManagerLayer(int index)
{
	index --;

	if (index == -1)
	{
		index = MAX_XML_MANAGER_LAYER_INDEX - 1;
	}

	return createNXMLManagerLayer(index);
}

static Layer* onAgainNXMLManagerLayer(int index)
{
	return createNXMLManagerLayer(index);
}

#endif // __TEXT_NXMLMANAGER_H__