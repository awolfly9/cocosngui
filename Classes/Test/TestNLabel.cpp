//#include "TestNLabel.h"
//
//// 
//// TestNLabel
////
//TestNLabel::TestNLabel(void)
//{
//	this->setIndex(-1);
//}
//
//TestNLabel::~TestNLabel(void)
//{
//}
//
//bool TestNLabel::init()
//{
//	INIT_DO_WHILE_BEGIN
//		BREAK_IF(! BasicLayer::init());
//
//	INIT_DO_WHILE_END
//}
//
//void TestNLabel::btnNextCallback(Ref* object, Touch_Period touch_period)
//{
//	if (touch_period == Touch_Period::kTouchEnded)
//	{
//		Layer* layer = onNextLabelLayer(this->getIndex());
//		Scene* scene = Scene::create();
//		scene->addChild(layer);
//		Director::getInstance()->replaceScene(scene);
//	}
//}
//
//void TestNLabel::btnbackCallback(Ref* object, Touch_Period touch_period)
//{
//	if (touch_period == Touch_Period::kTouchEnded)
//	{
//		Layer* layer = onBackLabelLayer(this->getIndex());
//		Scene* scene = Scene::create();
//		scene->addChild(layer);
//		Director::getInstance()->replaceScene(scene);
//	}
//}
//
//void TestNLabel::btnAgainCallabck(Ref* object, Touch_Period touch_period)
//{
//	if (touch_period == Touch_Period::kTouchEnded)
//	{
//		Layer* layer = onAgainLabelLayer(this->getIndex());
//		Scene* scene = Scene::create();
//		scene->addChild(layer);
//		Director::getInstance()->replaceScene(scene);
//	}
//}
//
////
//// NLabel_Horizontal
////
//NLabel_Horizontal::NLabel_Horizontal()
//	: _label(nullptr)
//{
//	this->setIndex(0);
//}
//
//NLabel_Horizontal::~NLabel_Horizontal()
//{
//}
//
//bool NLabel_Horizontal::init()
//{
//	INIT_DO_WHILE_BEGIN
//		BREAK_IF(! TestNLabel::init());
//
//	NLabel* lbl_click_up = NLabel::create("Click Up");
//	this->addChild(lbl_click_up);
//	lbl_click_up->setPosition(Vec2(VISIBLE_SIZE.width * 0.5f, VISIBLE_SIZE.height * 0.6f));
//	lbl_click_up->setName("Click Up");
//	lbl_click_up->setCallback(CALLBACK_2(NLabel_Horizontal::btnCallback, this));
//
//	NLabel* lbl_click_center = NLabel::create("Click Center");
//	this->addChild(lbl_click_center);
//	lbl_click_center->setPosition(Vec2(VISIBLE_SIZE.width * 0.5f, VISIBLE_SIZE.height * 0.5f));
//	lbl_click_center->setName("Click Center");
//	lbl_click_center->setCallback(CALLBACK_2(NLabel_Horizontal::btnCallback, this));
//
//	NLabel* lbl_click_down = NLabel::create("Click Down");
//	this->addChild(lbl_click_down);
//	lbl_click_down->setPosition(Vec2(VISIBLE_SIZE.width * 0.5f, VISIBLE_SIZE.height * 0.4f));
//	lbl_click_down->setName("Click Down");
//	lbl_click_down->setCallback(CALLBACK_2(NLabel_Horizontal::btnCallback, this));
//
//	_label = NLabelTTF::create("", "Arial", 30);
//	this->addChild(_label);
//	_label->setPosition(Vec2(VISIBLE_SIZE.width * 0.8f, VISIBLE_SIZE.height * 0.5f));
//	_label->setTextDirection(Text_Direction::kText_Dir_Vertical);
//
//	INIT_DO_WHILE_END
//}
//
//void NLabel_Horizontal::btnCallback(Ref* object, Touch_Period touch_period)
//{
//	if (touch_period == Touch_Period::kTouchEnded)
//	{
//		NLabel* lbl = (NLabel*)object;
//		_label->setString(lbl->getName().c_str());
//	}
//}
//
////
//// NLabel_Vertical
////
//NLabel_Vertical::NLabel_Vertical()
//	: _label(nullptr)
//{
//	this->setIndex(1);
//}
//
//NLabel_Vertical::~NLabel_Vertical()
//{
//}
//
//bool NLabel_Vertical::init()
//{
//	INIT_DO_WHILE_BEGIN
//		BREAK_IF(! TestNLabel::init());
//
//	NLabel* lbl_click_left = NLabel::create("Click Left");
//	this->addChild(lbl_click_left);
//	lbl_click_left->setPosition(Vec2(VISIBLE_SIZE.width * 0.4f, VISIBLE_SIZE.height * 0.5f));
//	lbl_click_left->setName("Click Left");
//	lbl_click_left->setCallback(CALLBACK_2(NLabel_Vertical::btnCallback, this));
//	lbl_click_left->setTextDirection(Text_Direction::kText_Dir_Vertical);
//
//	NLabel* lbl_click_center = NLabel::create("Click Center");
//	this->addChild(lbl_click_center);
//	lbl_click_center->setPosition(Vec2(VISIBLE_SIZE.width * 0.5f, VISIBLE_SIZE.height * 0.5f));
//	lbl_click_center->setName("Click Center");
//	lbl_click_center->setCallback(CALLBACK_2(NLabel_Vertical::btnCallback, this));
//	lbl_click_center->setTextDirection(Text_Direction::kText_Dir_Vertical);
//
//	NLabel* lbl_click_right = NLabel::create("Click Right");
//	this->addChild(lbl_click_right);
//	lbl_click_right->setPosition(Vec2(VISIBLE_SIZE.width * 0.6f, VISIBLE_SIZE.height * 0.5f));
//	lbl_click_right->setName("Click Right");
//	lbl_click_right->setCallback(CALLBACK_2(NLabel_Vertical::btnCallback, this));
//	lbl_click_right->setTextDirection(Text_Direction::kText_Dir_Vertical);
//
//	_label = NLabelTTF::create("", "Arial", 30);
//	this->addChild(_label);
//	_label->setPosition(Vec2(VISIBLE_SIZE.width * 0.5f, VISIBLE_SIZE.height * 0.85f));
//
//	INIT_DO_WHILE_END
//}
//
//void NLabel_Vertical::btnCallback(Ref* object, Touch_Period touch_period)
//{
//	if (touch_period == Touch_Period::kTouchEnded)
//	{
//		NLabel* lbl = (NLabel*)object;
//		_label->setString(lbl->getName().c_str());
//	}
//}