#ifndef __TEST_NJoystick_H__
#define __TEST_NJoystick_H__

#include "BasicLayer.h"

#define MAX_JOYSTICK_LAYER_INDEX 1

/**
TestNJoystick
*/
class TestNJoystick : public BasicLayer
{
public:
	TestNJoystick(void);
	~TestNJoystick(void);

	CREATE_FUNC(TestNJoystick);
	virtual bool init();

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};

/**
NJoystick_Handle
*/
class NJoystick_Handle : public TestNJoystick
{
public:
	NJoystick_Handle();
	~NJoystick_Handle();

	CREATE_FUNC(NJoystick_Handle);
	virtual bool init();

private:
	void handleCallback(Ref* ref, Touch_Period touch_period);

private:
	Node* _player;
	float _speed;
};


////////////////////////////////////////////////////////
static Layer* createNJoystickLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NJoystick_Handle::create();
		}
		break;
	default:
		break;
	}

	return nullptr;
}

static Layer* onNextNJoystickLayer(int index)
{
	index ++;

	if (index == MAX_JOYSTICK_LAYER_INDEX)
	{
		index = 0;
	}

	return createNJoystickLayer(index);
}

static Layer* onBackNJoystickLayer(int index)
{
	index --;

	if (index == -1)
	{
		index = MAX_JOYSTICK_LAYER_INDEX - 1;
	}

	return createNJoystickLayer(index);
}

static Layer* onAgainNJoystickLayer(int index)
{
	return createNJoystickLayer(index);
}


#endif // __TEST_NJoystick_H__
