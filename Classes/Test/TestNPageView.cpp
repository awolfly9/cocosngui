#include "TestNPageView.h"


TestNPageView::TestNPageView(void)
	: _lbl_index(nullptr)
{
	this->setIndex(-1);
}

TestNPageView::~TestNPageView(void)
{
}

bool TestNPageView::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	_lbl_index = LabelTTF::create("current page index 0", "Arial", 30);
	this->addChild(_lbl_index);
	_lbl_index->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.85f));

	INIT_DO_WHILE_END
}

Ref* TestNPageView::loadCell(Ref* object, int index)
{
	NPageViewCell* cell = NPageViewCell::create();

	float r = rand() % 255;
	float g = rand() % 255;
	float b = rand() % 255;

	LayerColor* layer = LayerColor::create(ccc4(r, g, b, 255));
	layer->ignoreAnchorPointForPosition(false);
	layer->setColor(Color3B(r, g, b));
	layer->setContentSize(Size(400, 400));
	cell->addChild(layer);
	layer->setPosition(POS_BY_NODE(cell, 0.5f, 0.5f));

	char str[10] = { };
	sprintf(str, "%d", index);

	LabelTTF* lbl = LabelTTF::create(str, "Arial", layer->getContentSize().height * 0.8f);
	layer->addChild(lbl);
	lbl->setPosition(POS_BY_NODE(layer, 0.5f, 0.5f));

	return cell;
}

void TestNPageView::pageChangedCallback(Ref* object, int index)
{
	char str[30] = { };
	sprintf(str, "current page index %d", index);
	_lbl_index->setString(str);
}

void TestNPageView::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextNPageViewLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNPageView::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackNPageViewLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNPageView::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainNPageViewLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NPageView_Horizontal
//
NPageView_Horizontal::NPageView_Horizontal()
	: _page_view(nullptr)
{
	this->setIndex(0);
}

NPageView_Horizontal::~NPageView_Horizontal()
{

}

bool NPageView_Horizontal::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNPageView::init());

	_page_view = NPageView::create(Size(400, 400), Size(400, 400), 10, CALLBACK_2(NPageView_Horizontal::loadCell, this));
	this->addChild(_page_view);
	_page_view->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));
	_page_view->setDirection(ScrollView_Direction::kScrollView_Dir_Horizontal);
	_page_view->setPageChangedCallback(CALLBACK_2(NPageView_Horizontal::pageChangedCallback, this));
	_page_view->reloadData();

	INIT_DO_WHILE_END
}

//
// NPageView_Horizontal_Relocate
// 
NPageView_Horizontal_Relocate::NPageView_Horizontal_Relocate()
{
	this->setIndex(1);
}

NPageView_Horizontal_Relocate::~NPageView_Horizontal_Relocate()
{

}

bool NPageView_Horizontal_Relocate::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NPageView_Horizontal::init());

	_page_view->setAutoRelocate(true);
	_page_view->reloadData();

	INIT_DO_WHILE_END
}

//
// NPageView_Vertical
// 
NPageView_Vertical::NPageView_Vertical()
	: _page_view(nullptr)
{
	this->setIndex(2);
}

NPageView_Vertical::~NPageView_Vertical()
{
}

bool NPageView_Vertical::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNPageView::init());

	_page_view = NPageView::create(Size(400, 400), Size(400, 400), 10, CALLBACK_2(NPageView_Vertical::loadCell, this));
	this->addChild(_page_view);
	_page_view->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));  
	_page_view->setPageChangedCallback(CALLBACK_2(NPageView_Vertical::pageChangedCallback, this));
	_page_view->reloadData();

	INIT_DO_WHILE_END
}

//
// NPageView_Vertical_Relocate
//
NPageView_Vertical_Relocate::NPageView_Vertical_Relocate()
{
	this->setIndex(3);
}

NPageView_Vertical_Relocate::~NPageView_Vertical_Relocate()
{
}

bool NPageView_Vertical_Relocate::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NPageView_Vertical::init());

	_page_view->setAutoRelocate(true);
	_page_view->reloadData();

	INIT_DO_WHILE_END
}

//
// NPageView_Horizontal_Tips_Index
//
NPageView_Horizontal_Tips_Index::NPageView_Horizontal_Tips_Index()
	: _page_view(nullptr)
{
	this->setIndex(4);
}

NPageView_Horizontal_Tips_Index::~NPageView_Horizontal_Tips_Index()
{

}

bool NPageView_Horizontal_Tips_Index::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNPageView::init());

	_page_view = NPageView::create(Size(400, 400), Size(400, 400), 10, CALLBACK_2(NPageView_Horizontal_Tips_Index::loadCell, this));
	this->addChild(_page_view);
	_page_view->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));
	_page_view->setDirection(ScrollView_Direction::kScrollView_Dir_Horizontal);
	_page_view->setPageChangedCallback(CALLBACK_2(NPageView_Horizontal_Tips_Index::pageChangedCallback, this));
	_page_view->setTipsLabelFileName("page_index_fore.png", "page_index_back.png");
	_page_view->reloadData();

	INIT_DO_WHILE_END
}

//
// NPageView_Vertical_Tips_Index
// 
NPageView_Vertical_Tips_Index::NPageView_Vertical_Tips_Index()
	: _page_view(nullptr)
{
	this->setIndex(5);
}

NPageView_Vertical_Tips_Index::~NPageView_Vertical_Tips_Index()
{
}

bool NPageView_Vertical_Tips_Index::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNPageView::init());

	_page_view = NPageView::create(Size(400, 400), Size(400, 400), 10, CALLBACK_2(NPageView_Vertical_Tips_Index::loadCell, this));
	this->addChild(_page_view);
	_page_view->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));  
	_page_view->setPageChangedCallback(CALLBACK_2(NPageView_Vertical_Tips_Index::pageChangedCallback, this));
	_page_view->setTipsLabelFileName("page_index_fore.png", "page_index_back.png");
	_page_view->setTipsLabelDistance(20);
	_page_view->setTipsLabelPositionPercent(0.5f);
	_page_view->reloadData();

	INIT_DO_WHILE_END
}
