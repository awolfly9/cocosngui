#include "TestNTableView.h"

//
// TestNTableView
//
TestNTableView::TestNTableView(void)
{
	this->setIndex(-1);
}

TestNTableView::~TestNTableView(void)
{
}

bool TestNTableView::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

Ref* TestNTableView::loadCell(Ref* object, int index)
{
	NTableViewCell* cell = NTableViewCell::create();

	float r = rand() % 255;
	float g = rand() % 255;
	float b = rand() % 255;

	LayerColor* layer = LayerColor::create(ccc4(r, g, b, 255));
	layer->ignoreAnchorPointForPosition(false);
	layer->setColor(Color3B(r, g, b));
	layer->setContentSize(Size(100, 100));
	cell->addChild(layer);
	layer->setPosition(POS_BY_NODE(cell, 0.5f, 0.5f));

	char str[10] = { };
	sprintf(str, "%d", index);

	LabelTTF* lbl = LabelTTF::create(str, "Arial", layer->getContentSize().height * 0.8f);
	layer->addChild(lbl);
	lbl->setPosition(POS_BY_NODE(layer, 0.5f, 0.5f));

	return cell;
}

void TestNTableView::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextNTableViewLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNTableView::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackNTableViewLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNTableView::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainNTableViewLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NTableView_Horizontal
//
NTableView_Horizontal::NTableView_Horizontal()
	: _table_view(nullptr)
{
	this->setIndex(0);
}

NTableView_Horizontal::~NTableView_Horizontal()
{

}

bool NTableView_Horizontal::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNTableView::init());

	_table_view = NTableView::create(Size(400, 100), Size(100, 100), 80, CALLBACK_2(NTableView_Horizontal::loadCell, this));
	this->addChild(_table_view);
	_table_view->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));
	_table_view->setDirection(ScrollView_Direction::kScrollView_Dir_Horizontal);
	_table_view->reloadData();

	INIT_DO_WHILE_END
}

//
// NTableView_Horizontal_Relocate
// 
NTableView_Horizontal_Relocate::NTableView_Horizontal_Relocate()
{
	this->setIndex(1);
}

NTableView_Horizontal_Relocate::~NTableView_Horizontal_Relocate()
{

}

bool NTableView_Horizontal_Relocate::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NTableView_Horizontal::init());

	_table_view->setAutoRelocate(true);
	_table_view->reloadData();

	INIT_DO_WHILE_END
}

//
// NTableView_Vertical
// 
NTableView_Vertical::NTableView_Vertical()
	: _table_view(nullptr)
{
	this->setIndex(2);
}

NTableView_Vertical::~NTableView_Vertical()
{
}

bool NTableView_Vertical::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNTableView::init());

	_table_view = NTableView::create(Size(100, 400), Size(100, 100), 100, CALLBACK_2(NTableView_Vertical::loadCell, this));
	this->addChild(_table_view);
	_table_view->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));
	_table_view->reloadData();

	INIT_DO_WHILE_END
}

//
// NTableView_Vertical_Relocate
//
NTableView_Vertical_Relocate::NTableView_Vertical_Relocate()
{
	this->setIndex(3);
}

NTableView_Vertical_Relocate::~NTableView_Vertical_Relocate()
{
}

bool NTableView_Vertical_Relocate::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NTableView_Vertical::init());

	_table_view->setAutoRelocate(true);
	_table_view->reloadData();

	INIT_DO_WHILE_END
}

//
// Table_View_ScrollBar_Horizontal
//
Table_View_ScrollBar_Horizontal::Table_View_ScrollBar_Horizontal(void)
{
	this->setIndex(4);
}

Table_View_ScrollBar_Horizontal::~Table_View_ScrollBar_Horizontal(void)
{
}

bool Table_View_ScrollBar_Horizontal::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNTableView::init());

	NTableView* table_view = NTableView::create(Size(500, 120), Size(100, 100), 50, CALLBACK_2(Table_View_ScrollBar_Horizontal::loadCell, this));
	this->addChild(table_view);
	table_view->setDirection(ScrollView_Direction::kScrollView_Dir_Horizontal);
	table_view->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));
	table_view->setScrollBar("progress_bg.png", "scroll_bar_1.png");
	table_view->setScrollBarDirection(Bar_Direction::kBar_Dir_LeftToRight);
	table_view->setAutoRelocate(true);
	table_view->reloadData();

	INIT_DO_WHILE_END
}

//
// NTableView_Vertical_Relocate
//
Table_View_ScrollBar_Vertical::Table_View_ScrollBar_Vertical(void)
{
	this->setIndex(5);
}

Table_View_ScrollBar_Vertical::~Table_View_ScrollBar_Vertical(void)
{
}

bool Table_View_ScrollBar_Vertical::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNTableView::init());

	NTableView* table_view = NTableView::create(Size(120, 500), Size(100, 100), 50, CALLBACK_2(Table_View_ScrollBar_Vertical::loadCell, this));
	this->addChild(table_view);
	table_view->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));

	table_view->setScrollBar("progress_v_bg.png", "scroll_bar_v_1.png");
	table_view->setScrollBarDirection(Bar_Direction::kBar_Dir_ButtomToTop);
	table_view->setAutoRelocate(true);
	table_view->reloadData();

	INIT_DO_WHILE_END
}