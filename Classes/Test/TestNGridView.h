#ifndef __TEST_NGRID_VIEW_H__
#define __TEST_NGRID_VIEW_H__

#include "BasicLayer.h"
 
#define MAX_GRID_VIEW_LAYER_INDEX 1

/**
TestNGridView
*/
class TestNGridView : public BasicLayer
{
public:
	TestNGridView(void);
	~TestNGridView(void);

	CREATE_FUNC(TestNGridView);
	virtual bool init();

protected:
	Ref* loadCell(Ref* object, int index);

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};

/**
NGridView_Vertical
*/
class NGridView_Vertical : public TestNGridView
{
public:
	NGridView_Vertical(void);
	~NGridView_Vertical(void);

	CREATE_FUNC(NGridView_Vertical);
	virtual bool init();
};


////////////////////////////////////////////////////////
static Layer* createNGridViewLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NGridView_Vertical::create();
		}
		break;
	default:
		break;
	}

	return nullptr;
}

static Layer* onNextNGridViewLayer(int index)
{
	index ++;

	if (index == MAX_GRID_VIEW_LAYER_INDEX)
	{
		index = 0;
	}

	return createNGridViewLayer(index);
}

static Layer* onBackNGridViewLayer(int index)
{
	index --;

	if (index == -1)
	{
		index = MAX_GRID_VIEW_LAYER_INDEX - 1;
	}

	return createNGridViewLayer(index);
}

static Layer* onAgainNGridViewLayer(int index)
{
	return createNGridViewLayer(index);
}

#endif // __TEST_NGRID_VIEW_H__
