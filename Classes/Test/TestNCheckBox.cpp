#include "TestNCheckBox.h"

//
// TestNCheckBox
//
TestNCheckBox::TestNCheckBox(void)
{
	this->setIndex(-1);
}

TestNCheckBox::~TestNCheckBox(void)
{
}

bool TestNCheckBox::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

void TestNCheckBox::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextNCheckBoxLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNCheckBox::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackNCheckBoxLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNCheckBox::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainNCheckBoxLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NCheckBox_Callback
//
NCheckBox_Callback::NCheckBox_Callback(void)
	: _lbl_check(nullptr)
{
	this->setIndex(0);
}

NCheckBox_Callback::~NCheckBox_Callback(void)
{

}

bool NCheckBox_Callback::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNCheckBox::init());

	NCheckBox* check_box = NCheckBox::create();
	check_box->setNormalImage("ckb_normal_01.png");
	check_box->setSelectedImage("ckb_normal_02.png");
	check_box->setCheckImage("ckb_selected_01.png");
	check_box->setCheckSelectedImage("ckb_selected_02.png");
	check_box->setDisableImage("ckb_disable_01.png");
	check_box->setCheckDisableImage("ckb_disable_02.png");
	check_box->setCallback(CALLBACK_2(NCheckBox_Callback::ckbCallback, this));
	this->addChild(check_box);
	check_box->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));

	_lbl_check = LabelTTF::create("", "Arial", 30);
	this->addChild(_lbl_check);
	_lbl_check->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.65f));

	INIT_DO_WHILE_END
}

void NCheckBox_Callback::ckbCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NCheckBox* ckb = (NCheckBox*)object;

		if (ckb->isChecked())
		{
			_lbl_check->setString("Check");
		}
		else
		{
			_lbl_check->setString("UnCheck");
		}
	}
}

//
// NCheckBox_HideNormalImage
//
NCheckBox_HideNormalImage::NCheckBox_HideNormalImage(void)
{
	this->setIndex(1);
}

NCheckBox_HideNormalImage::~NCheckBox_HideNormalImage(void)
{

}

bool NCheckBox_HideNormalImage::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNCheckBox::init());

	NCheckBox* check_box = NCheckBox::create();
	check_box->setNormalImage("ckb_normal_01.png");
	check_box->setSelectedImage("ckb_normal_02.png");
	check_box->setCheckImage("ckb_selected_01.png");
	check_box->setCheckSelectedImage("ckb_selected_02.png");
	check_box->setDisableImage("ckb_disable_01.png");
	check_box->setCheckDisableImage("ckb_disable_02.png");
	check_box->setCallback(CALLBACK_2(NCheckBox_HideNormalImage::ckbCallback, this));
	this->addChild(check_box);
	check_box->setPosition(POS_BY_VISIBLE_SIZE(0.4f, 0.5f));

	_lbl_check = LabelTTF::create("", "Arial", 30);
	this->addChild(_lbl_check);
	_lbl_check->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.65f));

	_ckb_hide = NCheckBox::create();
	_ckb_hide->setNormalImage("selected01.png");
	_ckb_hide->setCheckImage("selected02.png");
	_ckb_hide->setHideNormalImage(false);
	this->addChild(_ckb_hide);
	_ckb_hide->setPosition(POS_BY_VISIBLE_SIZE(0.6f, 0.5f));
	_ckb_hide->setChecked(true);

	INIT_DO_WHILE_END
}

void NCheckBox_HideNormalImage::ckbCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NCheckBox* ckb = (NCheckBox*)object;

		if (ckb->isChecked())
		{
			_lbl_check->setString("Check");
		}
		else
		{
			_lbl_check->setString("UnCheck");
		}

		if (_ckb_hide->isHideNormalImage())
		{
			_ckb_hide->setHideNormalImage(false);
		}
		else
		{
			_ckb_hide->setHideNormalImage(true);
		}
	}
}

//
// NCheckBox_Action
//
NCheckBox_Action::NCheckBox_Action(void)
{
	this->setIndex(2);
}

NCheckBox_Action::~NCheckBox_Action(void)
{
}

bool NCheckBox_Action::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNCheckBox::init());

	NCheckBox* check_box = NCheckBox::create();
	check_box->setNormalImage("ckb_normal_01.png");
	check_box->setSelectedImage("ckb_normal_02.png");
	check_box->setCheckImage("ckb_selected_01.png");
	check_box->setCheckSelectedImage("ckb_selected_02.png");
	check_box->setDisableImage("ckb_disable_01.png");
	check_box->setCheckDisableImage("ckb_disable_02.png");
	this->addChild(check_box);
	check_box->setPosition(POS_BY_VISIBLE_SIZE(0.4f, 0.5f));
	check_box->setPressScale(1.2f);
	check_box->setText("Action");

	NCheckBox* ckb_hide = NCheckBox::create();
	ckb_hide->setNormalImage("selected01.png");
	ckb_hide->setCheckImage("selected02.png");
	ckb_hide->setHideNormalImage(false);
	this->addChild(ckb_hide);
	ckb_hide->setPosition(POS_BY_VISIBLE_SIZE(0.6f, 0.5f));
	ckb_hide->setChecked(true);
	ckb_hide->setPressScale(0.8f);
	ckb_hide->setText("Action");

	INIT_DO_WHILE_END
}

//
// NCheckBox_Tips
//
NCheckBox_Tips::NCheckBox_Tips(void)
{
	this->setIndex(3);
}

NCheckBox_Tips::~NCheckBox_Tips(void)
{
}

bool NCheckBox_Tips::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNCheckBox::init());

	NCheckBox* check_box = NCheckBox::create();
	check_box->setNormalImage("ckb_normal_01.png");
	check_box->setSelectedImage("ckb_normal_02.png");
	check_box->setCheckImage("ckb_selected_01.png");
	check_box->setCheckSelectedImage("ckb_selected_02.png");
	check_box->setDisableImage("ckb_disable_01.png");
	check_box->setCheckDisableImage("ckb_disable_02.png");
	this->addChild(check_box);
	check_box->setPosition(POS_BY_VISIBLE_SIZE(0.4f, 0.5f));
	check_box->setPressScale(1.2f);
	check_box->setCallback(CALLBACK_2(NCheckBox_Tips::ckbCallback, this));
	check_box->setTipsImage("tips_image.png");
	check_box->setTipsImagePosition(Vec2(check_box->getContentSize().width / 2, check_box->getContentSize().height / 2));

	NCheckBox* ckb_hide = NCheckBox::create();
	ckb_hide->setNormalImage("selected01.png");
	ckb_hide->setCheckImage("selected02.png");
	ckb_hide->setHideNormalImage(false);
	this->addChild(ckb_hide);
	ckb_hide->setPosition(POS_BY_VISIBLE_SIZE(0.6f, 0.5f));
	ckb_hide->setChecked(true);
	ckb_hide->setPressScale(0.8f);
	ckb_hide->setCallback(CALLBACK_2(NCheckBox_Tips::ckbCallback, this));
	ckb_hide->setTipsImage("tips_image.png");

	INIT_DO_WHILE_END
}

void NCheckBox_Tips::ckbCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NCheckBox* ckb = (NCheckBox*)object;

		if (ckb->isTips())
		{
			ckb->setTips(false);
		}
		else
		{
			ckb->setTips(true);
		}
	}
}