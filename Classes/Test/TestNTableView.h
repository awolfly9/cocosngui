#ifndef __TEST_NTABLE_VIEW_H__
#define __TEST_NTABLE_VIEW_H__

#include "BasicLayer.h"

#define MAX_TABLE_VIEW_LAYER_INDEX 6

/**
TestNTableView
*/
class TestNTableView : public BasicLayer
{
public:
	TestNTableView(void);
	~TestNTableView(void);

	CREATE_FUNC(TestNTableView);
	virtual bool init();

protected:
	Ref* loadCell(Ref* object, int index);

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};

/**
NTableView_Horizontal
*/
class NTableView_Horizontal : public TestNTableView
{
public:
	NTableView_Horizontal();
	~NTableView_Horizontal();

	CREATE_FUNC(NTableView_Horizontal);
	virtual bool init();

protected:
	NTableView* _table_view;
};

/**
NTableView_Horizontal_Relocate
*/
class NTableView_Horizontal_Relocate : public NTableView_Horizontal
{
public:
	NTableView_Horizontal_Relocate();
	~NTableView_Horizontal_Relocate();

	CREATE_FUNC(NTableView_Horizontal_Relocate);
	virtual bool init();
};

/**
NTableView_Vertical
*/
class NTableView_Vertical : public TestNTableView
{
public:
	NTableView_Vertical();
	~NTableView_Vertical();

	CREATE_FUNC(NTableView_Vertical);
	virtual bool init();

protected:
	NTableView* _table_view;
};

/**
NTableView_Vertical_Relocate
*/
class NTableView_Vertical_Relocate : public NTableView_Vertical
{
public:
	NTableView_Vertical_Relocate();
	~NTableView_Vertical_Relocate();

	CREATE_FUNC(NTableView_Vertical_Relocate);
	virtual bool init();
};

/**
Table_View_ScrollBar_Horizontal
*/
class Table_View_ScrollBar_Horizontal : public TestNTableView
{
public:
	Table_View_ScrollBar_Horizontal(void);
	~Table_View_ScrollBar_Horizontal(void);

	CREATE_FUNC(Table_View_ScrollBar_Horizontal);
	virtual bool init();
};

/**
Table_View_ScrollBar_Vertical
*/
class Table_View_ScrollBar_Vertical : public TestNTableView
{
public:
	Table_View_ScrollBar_Vertical(void);
	~Table_View_ScrollBar_Vertical(void);

	CREATE_FUNC(Table_View_ScrollBar_Vertical);
	virtual bool init();
};


////////////////////////////////////////////////////////
static Layer* createNTableViewLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NTableView_Horizontal::create();
		}
		break;
	case 1:
		{
			return NTableView_Horizontal_Relocate::create();
		}
		break;
	case 2:
		{
			return NTableView_Vertical::create();
		}
		break;
	case 3:
		{
			return NTableView_Vertical_Relocate::create();
		}
		break;
	case 4:
		{
			return Table_View_ScrollBar_Horizontal::create();
		}
		break;
	case 5:
		{
			return Table_View_ScrollBar_Vertical::create();
		}
		break;
	default:
		break;
	}

	return nullptr;
}

static Layer* onNextNTableViewLayer(int index)
{
	index ++;

	if (index == MAX_TABLE_VIEW_LAYER_INDEX)
	{
		index = 0;
	}

	return createNTableViewLayer(index);
}

static Layer* onBackNTableViewLayer(int index)
{
	index --;

	if (index == -1)
	{
		index = MAX_TABLE_VIEW_LAYER_INDEX - 1;
	}

	return createNTableViewLayer(index);
}

static Layer* onAgainNTableViewLayer(int index)
{
	return createNTableViewLayer(index);
}

#endif // __TEST_NTABLE_VIEW_H__
