#include "BasicLayer.h"

#define MAX_MESSAGE_TIPS_LAYER_INDEX 1

/**
TestNMessageTips
*/
class TestNMessageTips : public BasicLayer
{
public:
	TestNMessageTips(void);
	~TestNMessageTips(void);

	CREATE_FUNC(TestNMessageTips);
	virtual bool init();

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};

/**
NMessageTips_Info
*/
class NMessageTips_Info : public TestNMessageTips
{
public:
	NMessageTips_Info();
	~NMessageTips_Info();

	CREATE_FUNC(NMessageTips_Info);
	virtual bool init();

	virtual bool onTouchBegan(Touch *touch, Event *unused_event);
	virtual void onTouchMoved(Touch *touch, Event *unused_event);
	virtual void onTouchEnded(Touch *touch, Event *unused_event);
	virtual void onTouchCancelled(Touch *touch, Event *unused_event);
};

////////////////////////////////////////////////////////
static Layer* createNMessageTipsLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NMessageTips_Info::create();
		}
		break;
	default:
		break;
	}

	return nullptr;
}

static Layer* onNextNMessageTipsLayer(int index)
{
	index ++;

	if (index == MAX_MESSAGE_TIPS_LAYER_INDEX)
	{
		index = 0;
	}

	return createNMessageTipsLayer(index);
}

static Layer* onBackNMessageTipsLayer(int index)
{
	index --;

	if (index == -1)
	{
		index = MAX_MESSAGE_TIPS_LAYER_INDEX - 1;
	}

	return createNMessageTipsLayer(index);
}

static Layer* onAgainNMessageTipsLayer(int index)
{
	return createNMessageTipsLayer(index);
}
