#include "TestNXMLManager.h"

//
// TestNXMLManager
//
TestNXMLManager::TestNXMLManager(void)
{
}

TestNXMLManager::~TestNXMLManager(void)
{
}

bool TestNXMLManager::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

void TestNXMLManager::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextNXMLManagerLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNXMLManager::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackNXMLManagerLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNXMLManager::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainNXMLManagerLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NXMLManager_Get
//
#define file_name_get "data/data_get.xml"
NXMLManager_Get::NXMLManager_Get()
{
	this->setIndex(0);
}

NXMLManager_Get::~NXMLManager_Get()
{
	
}

bool NXMLManager_Get::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNXMLManager::init());

	string data = "";
	// Person/Student
	//data = NXMLManager::getInstance()->getNodeValue(file_name_get, "Root/Person/Student/name");
	//log("Person/Student/name :%s", data.c_str());
	//data = NXMLManager::getInstance()->getNodeValue(file_name_get, "Root/Person/Student/age");
	//log("Person/Student/age :%s", data.c_str());

	//// Person/Teacher
	//data = NXMLManager::getInstance()->getNodeValue(file_name_get, "Root/Person/Teacher/name");
	//log("Person/Teacher/name :%s", data.c_str());
	//data = NXMLManager::getInstance()->getNodeValue(file_name_get, "Root/Person/Teacher/age");
	//log("Person/Teacher/age :%s", data.c_str());

	//data = NXMLManager::getInstance()->getNodeAttribute(file_name_get, "Root/Person/Teacher/salary/overtime");
	//log("Person/Teacher/salary/overtime :%s", data.c_str());

	//data = NXMLManager::getInstance()->getNodeAttribute(file_name_get, "Root/Person/Teacher/salary/other");
	//log("Person/Teacher/salary/overtime :%s", data.c_str());
	
	// 获取已经完全没有问题
	INIT_DO_WHILE_END
}

//
// NXMLManager_Add
//
NXMLManager_Add::NXMLManager_Add()
{
	this->setIndex(1);
}

NXMLManager_Add::~NXMLManager_Add()
{

}

bool NXMLManager_Add::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNXMLManager::init());
	
	/*string file_path = FileUtils::getInstance()->getWritablePath() + "data.xml";
	NXMLManager::getInstance()->isExistXML(file_path);

	NXMLManager::getInstance()->addNodeValue(file_path, "Root/Student", "");
	NXMLManager::getInstance()->addNodeAttribute(file_path, "Root/Student/type", "type");
	NXMLManager::getInstance()->addNodeValue(file_path, "Root/Student/name", "name");
	NXMLManager::getInstance()->addNodeValue(file_path, "Root/Teacher", "Teacher");

	string data = "";
	data = NXMLManager::getInstance()->getNodeValue(file_path, "Root/Student/name");
	log("Student :%s", data.c_str());
	data = NXMLManager::getInstance()->getNodeValue(file_path, "Root/Teacher");
	log("Teacher :%s", data.c_str());*/

	INIT_DO_WHILE_END
}

//
// NXMLManager_Set
//
NXMLManager_Set::NXMLManager_Set()
{
	this->setIndex(2);
}

NXMLManager_Set::~NXMLManager_Set()
{

}

bool NXMLManager_Set::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNXMLManager::init());

	/*string file_path = FileUtils::getInstance()->getWritablePath() + "data.xml";
	bool ret = NXMLManager::getInstance()->isExistXML(file_path);

	if ( ret )
	{
	NXMLManager::getInstance()->setNodeValue(file_path, "Root/Student/name", "student");
	NXMLManager::getInstance()->setNodeAttribute(file_path, "Root/Student/type", "1234");
	}*/

	INIT_DO_WHILE_END
}

//
// NXMLManager_Delete
//
NXMLManager_Delete::NXMLManager_Delete()
{
	this->setIndex(3);
}

NXMLManager_Delete::~NXMLManager_Delete()
{

}

bool NXMLManager_Delete::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNXMLManager::init());

	/*string file_path = FileUtils::getInstance()->getWritablePath() + "data.xml";
	bool ret = NXMLManager::getInstance()->isExistXML(file_path);

	if ( ret )
	{
	NXMLManager::getInstance()->deleteNodeValue(file_path, "Root/Teacher");
	NXMLManager::getInstance()->deleteNodeAttribute(file_path, "Root/Student/type");
	}*/
	
	INIT_DO_WHILE_END
}

