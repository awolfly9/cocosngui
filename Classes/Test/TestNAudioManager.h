#ifndef __TEST_NAUDIO_MANAGER_H__
#define __TEST_NAUDIO_MANAGER_H__

#include "BasicLayer.h"

#define MAX_AUDIO_LAYER_INDEX 1

/**
TestNAudioManager
*/
class TestNAudioManager : public BasicLayer
{
public:
	TestNAudioManager(void);
	~TestNAudioManager(void);

	CREATE_FUNC(TestNAudioManager);
	virtual bool init();

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};

/**
NAudioManager_Async_Effect
*/
class NAudioManager_Async_Effect : public TestNAudioManager
{
public:
	NAudioManager_Async_Effect();
	~NAudioManager_Async_Effect();

	CREATE_FUNC(NAudioManager_Async_Effect);
	virtual bool init();

private:
	void callback(const string& file_path);
	void loading(float dt);

private:
	int _count;
	NProgressBar* _bar;

	Label* _lbl;

	vector<string> _audios;
};

/**
NAudioManager_Async_Bg_Music
*/
//class NAudioManager_Async_Bg_Music : public TestNAudioManager
//{
//public:
//	NAudioManager_Async_Bg_Music();
//	~NAudioManager_Async_Bg_Music();
//
//	CREATE_FUNC(NAudioManager_Async_Bg_Music);
//	virtual bool init();
//
//private:
//	void callback(const string& file_path);
//	void btnCallback(Ref* ref, Touch_Period touch_period);
//
//private:
//	int _count;
//	NProgressBar* _bar;
//};

////////////////////////////////////////////////////////
static Layer* createAudioLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NAudioManager_Async_Effect::create();
		}
		break;
		/*case 1:
		{
		return NAudioManager_Async_Bg_Music::create();
		}
		break;*/
	default:
		break;
	}

	return nullptr;
}

static Layer* onNextAudioLayer(int index)
{
	index ++;

	if (index == MAX_AUDIO_LAYER_INDEX)
	{
		index = 0;
	}

	return createAudioLayer(index);
}

static Layer* onBackAudioLayer(int index)
{
	index --;

	if (index == -1)
	{
		index = MAX_AUDIO_LAYER_INDEX - 1;
	}

	return createAudioLayer(index);
}

static Layer* onAgainAudioLayer(int index)
{
	return createAudioLayer(index);
}

#endif // __TEST_NAUDIO_MANAGER_H__