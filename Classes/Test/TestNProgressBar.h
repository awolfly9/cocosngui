#ifndef __TEST_NPROGRESS_BAR_H__
#define __TEST_NPROGRESS_BAR_H__

#include "BasicLayer.h"

#define MAX_PROGRESS_BAR_LAYER_INDEX 2


class TestNProgressBar : public BasicLayer
{
public:
	TestNProgressBar(void);
	~TestNProgressBar(void);
	
	CREATE_FUNC(TestNProgressBar);
	virtual bool init();

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};


class NProgressBar_Horizontal : public TestNProgressBar
{
public:
	NProgressBar_Horizontal();
	~NProgressBar_Horizontal();

	CREATE_FUNC(NProgressBar_Horizontal);
	virtual bool init();

private:
	void btnAddProgressCallback(Ref* object, Touch_Period touch_period);
	void btnReduceProgressCallback(Ref* object, Touch_Period touch_period);
	void btnDirectionCallback(Ref* object, Touch_Period touch_period);
	void btnStopCallback(Ref* object, Touch_Period touch_period);

	void barShowCallback(Ref* object, Bar_Period bar_period);

private:
	NProgressBar* _progress_bar;
};


class NProgressBar_Vertical : public TestNProgressBar
{
public:
	NProgressBar_Vertical();
	~NProgressBar_Vertical();

	CREATE_FUNC(NProgressBar_Vertical);
	virtual bool init();

private:
	void btnAddProgressCallback(Ref* object, Touch_Period touch_period);
	void btnReduceProgressCallback(Ref* object, Touch_Period touch_period);
	void btnDirectionCallback(Ref* object, Touch_Period touch_period);
	void btnStopCallback(Ref* object, Touch_Period touch_period);

private:
	NProgressBar* _progress_bar;
};

////////////////////////////////////////////////////////
static Layer* createNProgressBarLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NProgressBar_Horizontal::create();
		}

		break;
	case 1:
		{
			return NProgressBar_Vertical::create();
		}

		break;
	case 2:
		{
			//return ButtonState::create();
		}

		break;
	case 3:
		{
			//return ButtonText::create();
		}

		break;
	case 4:
		{
			//return ButtonScale9Sprite::create();
		}

		break;
	default:

		break;
	}

	return nullptr;
}

static Layer* onNextNProgressBarLayer(int index)
{
	index ++;

	if(index == MAX_PROGRESS_BAR_LAYER_INDEX)
	{
		index = 0;
	}

	return createNProgressBarLayer(index);
}

static Layer* onBackNProgressBarLayer(int index)
{
	index --;

	if(index == -1)
	{
		index = MAX_PROGRESS_BAR_LAYER_INDEX - 1;
	}

	return createNProgressBarLayer(index);
}

static Layer* onAgainNProgressBarLayer(int index)
{
	return createNProgressBarLayer(index);
}

#endif // __TEST_NPROGRESS_BAR_H__
