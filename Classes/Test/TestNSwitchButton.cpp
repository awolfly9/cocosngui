#include "TestNSwitchButton.h"


TestNSwitchButton::TestNSwitchButton(void)
{
}


TestNSwitchButton::~TestNSwitchButton(void)
{
}


bool TestNSwitchButton::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

void TestNSwitchButton::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextNSwitchButtonLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNSwitchButton::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackNSwitchButtonLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNSwitchButton::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainNSwitchButtonLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NSwitchButton_Show
//
NSwitchButton_Show::NSwitchButton_Show()
{
	this->setIndex(0);
}

NSwitchButton_Show::~NSwitchButton_Show()
{

}

bool NSwitchButton_Show::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNSwitchButton::init());

	_lbl = Label::createWithSystemFont("Switch State On", "Arial", 30);
	this->addChild(_lbl);
	_lbl->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.75f));

	NSwitchButton* btn_switch = NSwitchButton::create("btn_sw.png");
	this->addChild(btn_switch);
	btn_switch->setCallback(CALLBACK_2(NSwitchButton_Show::callback, this));
	btn_switch->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));

	INIT_DO_WHILE_END
}

void NSwitchButton_Show::callback(Ref* ref, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NSwitchButton* btn = (NSwitchButton*)ref;

		if (btn->getSwitchState() == Switch_State::kSwitch_On)
		{
			_lbl->setString("Switch State On");
		}
		else
		{
			_lbl->setString("Switch State off");
		}
	}
}