#include "TestNRichText.h"

//
// TestNRichText
// 
TestNRichText::TestNRichText(void)
{
	this->setIndex(-1);
}

TestNRichText::~TestNRichText(void)
{
}

bool TestNRichText::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

void TestNRichText::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextNRichTextLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNRichText::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackNRichTextLayer(_index);
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNRichText::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainNRichTextLayer(_index);
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NRichText_Element
//
NRichText_Element::NRichText_Element(void)
{
	this->setIndex(0);
}

NRichText_Element::~NRichText_Element(void)
{

}

bool NRichText_Element::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNRichText::init());

	NRichText* rich_text = NRichText::create();
	rich_text->setMaxLineLength(10);
	rich_text->insertElement("01234", "LiShu", 30, Color3B(255, 204, 0));

	Sprite* sp_1 = Sprite::create("CloseNormal.png");
	rich_text->insertElement(sp_1, 1, true);
	sp_1->runAction(CCRepeatForever::create(
		CCRotateBy::create(0.5f, 90, 90)));

	Sprite* sp_2 = Sprite::create("CloseNormal.png");
	rich_text->insertElement(sp_2, 1);

	sp_2->runAction(CCRepeatForever::create(
		CCSequence::create(
		CCMoveBy::create(0.2f, Vec2(0, 15)),
		CCMoveBy::create(0.2f, Vec2(0, -15)),
		nullptr)));

	rich_text->insertElement("0123456789", "Arial", 20, Color3B(255, 0, 0));	
	rich_text->reloadData();
	this->addChild(rich_text);
	rich_text->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));

	INIT_DO_WHILE_END
}

//
// NRichText_VerticalSpace
//
NRichText_VerticalSpace::NRichText_VerticalSpace(void)
{
	this->setIndex(1);
}

NRichText_VerticalSpace::~NRichText_VerticalSpace(void)
{

}

bool NRichText_VerticalSpace::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNRichText::init());

	NRichText* rich_text = NRichText::create();
	rich_text->setMaxLineLength(10);
	rich_text->insertElement("01234", "LiShu", 30, Color3B(255, 204, 0));

	Sprite* sp_1 = Sprite::create("CloseNormal.png");
	rich_text->insertElement(sp_1, 1, true);
	sp_1->runAction(CCRepeatForever::create(
		CCRotateBy::create(0.5f, 90, 90)));

	Sprite* sp_2 = Sprite::create("CloseNormal.png");
	rich_text->insertElement(sp_2, 1);

	sp_2->runAction(CCRepeatForever::create(
		CCSequence::create(
		CCMoveBy::create(0.2f, Vec2(0, 15)),
		CCMoveBy::create(0.2f, Vec2(0, -15)),
		nullptr)));

	rich_text->insertElement("0123456789", "Arial", 10, Color3B(255, 0, 0));	
	rich_text->setVerticalSpace(50);
	rich_text->reloadData();
	this->addChild(rich_text);
	rich_text->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));

	INIT_DO_WHILE_END
}
