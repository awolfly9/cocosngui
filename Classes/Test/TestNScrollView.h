#ifndef __TEST_NSCROLL_VIEW_H__
#define __TEST_NSCROLL_VIEW_H__

#include "BasicLayer.h"

#define MAX_SCROLL_VIEW_LAYER_INDEX 5

class TestNScrollView : public BasicLayer
{
public:
	TestNScrollView(void);
	~TestNScrollView(void);

	CREATE_FUNC(TestNScrollView);
	virtual bool init();

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};

/**
NScrollView_Drag
*/
class NScrollView_Drag : public TestNScrollView
{
public:
	NScrollView_Drag(void);
	~NScrollView_Drag(void);

	CREATE_FUNC(NScrollView_Drag);
	virtual bool init();
};

/**
NScrollView_Dirction
*/
class NScrollView_Dirction : public TestNScrollView
{
public:
	NScrollView_Dirction(void);
	~NScrollView_Dirction(void);

	CREATE_FUNC(NScrollView_Dirction);
	virtual bool init();

private:
	void btnCallback(Ref* object, Touch_Period touch_period);

private:
	NScrollView* _scroll_view;
};

/**
NScrollView_Embed
*/
class NScrollView_Embed : public NScrollView_Dirction
{
public:
	NScrollView_Embed(void);
	~NScrollView_Embed(void);

	CREATE_FUNC(NScrollView_Embed);
	virtual bool init();
};

/**
NScrollView_ScrollBar_Horizontal
*/
class NScrollView_ScrollBar_Horizontal : public TestNScrollView
{
public:
	NScrollView_ScrollBar_Horizontal(void);
	~NScrollView_ScrollBar_Horizontal(void);

	CREATE_FUNC(NScrollView_ScrollBar_Horizontal);
	virtual bool init();
};

/**
NScrollView_ScrollBar_Vertical
*/
class NScrollView_ScrollBar_Vertical : public TestNScrollView
{
public:
	NScrollView_ScrollBar_Vertical(void);
	~NScrollView_ScrollBar_Vertical(void);

	CREATE_FUNC(NScrollView_ScrollBar_Vertical);
	virtual bool init();
};


//////////////////////////////////////////////////////
static Layer* createNScrollViewLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NScrollView_Drag::create();
		}
		break;
	case 1:
		{
			return NScrollView_Dirction::create();
		}
		break;
	case 2:
		{
			return NScrollView_Embed::create();
		}
		break;
	case  3:
		{
			return NScrollView_ScrollBar_Horizontal::create();
		}
		break;
	case 4:
		{
			return NScrollView_ScrollBar_Vertical::create();
		}
	default:
		break;
	}

	return nullptr;
}

static Layer* onNextNScrollViewLayer(int index)
{
	index ++;

	if (index == MAX_SCROLL_VIEW_LAYER_INDEX)
	{
		index = 0;
	}

	return createNScrollViewLayer(index);
}

static Layer* onBackNScrollViewLayer(int index)
{
	index --;

	if (index == -1)
	{
		index = MAX_SCROLL_VIEW_LAYER_INDEX - 1;
	}

	return createNScrollViewLayer(index);
}

static Layer* onAgainNScrollViewLayer(int index)
{
	return createNScrollViewLayer(index);
}


#endif // __TEST_NSCROLL_VIEW_H__
