#include "TestNListView.h"

//
// TestNListView
//
TestNListView::TestNListView(void)
{
	this->setIndex(-1);
}

TestNListView::~TestNListView(void)
{
}

bool TestNListView::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

void TestNListView::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextListViewLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNListView::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackListViewLayer(_index);
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNListView::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainListViewLayer(_index);
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NListView_Vertical
//
NListView_Vertical::NListView_Vertical(void)
	: _count(1)
{
	this->setIndex(0);
}

NListView_Vertical::~NListView_Vertical(void)
{

}

bool NListView_Vertical::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNListView::init());

	srand((unsigned) time (nullptr));

	_list_view = NListView::create(Size(VISIBLE_SIZE.width * 0.5f, VISIBLE_SIZE.height * 0.5f));
	this->addChild(_list_view);
	_list_view->setPosition(VISIBLE_SIZE_CENTER);

	LayerColor* layer = LayerColor::create(ccc4(255, 255, 255, 255));
	layer->ignoreAnchorPointForPosition(false);
	layer->setColor(Color3B(255, 255, 255));
	this->addChild(layer, -10);
	layer->setPosition(VISIBLE_SIZE_CENTER);
	layer->setAnchorPoint(Vec2(0.5f, 0.5f));
	layer->setContentSize(Size(VISIBLE_SIZE.width * 0.5f, VISIBLE_SIZE.height * 0.5f));

	// left align
	NButton* btn_left_align = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_left_align);
	btn_left_align->setPosition(POS_BY_VISIBLE_SIZE(0.25f, 0.85f));
	btn_left_align->setTag((int)ListView_Align_Type::kListView_Align_Vertical_Left);
	btn_left_align->setCallback(CALLBACK_2(NListView_Vertical::btnAlignCallback, this));
	btn_left_align->setText("left align");

	// center align
	NButton* btn_center_align = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_center_align);
	btn_center_align->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.85f));
	btn_center_align->setTag((int)ListView_Align_Type::kListView_Align_Vertical_Center);
	btn_center_align->setCallback(CALLBACK_2(NListView_Vertical::btnAlignCallback, this));
	btn_center_align->setText("center align");

	// right align
	NButton* btn_right_align = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_right_align);
	btn_right_align->setPosition(POS_BY_VISIBLE_SIZE(0.75f, 0.85f));
	btn_right_align->setTag((int)ListView_Align_Type::kListView_Align_Vertical_Right);
	btn_right_align->setCallback(CALLBACK_2(NListView_Vertical::btnAlignCallback, this));
	btn_right_align->setText("right align");

	// insert
	// insert front
	NButton* btn_insert_front = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_insert_front);
	btn_insert_front->setPosition(POS_BY_VISIBLE_SIZE(0.12f, 0.7f));
	btn_insert_front->setCallback(CALLBACK_2(NListView_Vertical::btnInsertCallback, this));
	btn_insert_front->setText("insert front");
	btn_insert_front->setTag(1);

	// insert last
	NButton* btn_insert_last = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_insert_last);
	btn_insert_last->setPosition(POS_BY_VISIBLE_SIZE(0.12f, 0.5f));
	btn_insert_last->setCallback(CALLBACK_2(NListView_Vertical::btnInsertCallback, this));
	btn_insert_last->setText("insert last");
	btn_insert_last->setTag(2);

	// insert random
	NButton* btn_insert_random = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_insert_random);
	btn_insert_random->setPosition(POS_BY_VISIBLE_SIZE(0.12f, 0.3f));
	btn_insert_random->setCallback(CALLBACK_2(NListView_Vertical::btnInsertCallback, this));
	btn_insert_random->setText("insert random");
	btn_insert_random->setTag(3);

	// remove
	// remove front
	NButton* btn_remove_front = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_remove_front);
	btn_remove_front->setPosition(POS_BY_VISIBLE_SIZE(0.88f, 0.7f));
	btn_remove_front->setCallback(CALLBACK_2(NListView_Vertical::btnRemoveCallback, this));
	btn_remove_front->setText("remove front");
	btn_remove_front->setTag(1);

	// remove last
	NButton* btn_remove_last = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_remove_last);
	btn_remove_last->setPosition(POS_BY_VISIBLE_SIZE(0.88f, 0.5f));
	btn_remove_last->setCallback(CALLBACK_2(NListView_Vertical::btnRemoveCallback, this));
	btn_remove_last->setText("remove last");
	btn_remove_last->setTag(2);

	// remove random
	NButton* btn_remove_random = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_remove_random);
	btn_remove_random->setPosition(POS_BY_VISIBLE_SIZE(0.88f, 0.3f));
	btn_remove_random->setCallback(CALLBACK_2(NListView_Vertical::btnRemoveCallback, this));
	btn_remove_random->setText("remove random");
	btn_remove_random->setTag(3);

	INIT_DO_WHILE_END
}

void NListView_Vertical::btnAlignCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		_list_view->setAlignType((ListView_Align_Type)btn->getTag());
	}
}

void NListView_Vertical::btnInsertCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		if (btn->getTag() == 1)
		{
			_list_view->insertNodeAtFront(this->createRandomNode());
			_list_view->reloadData();
		}
		else if (btn->getTag() == 2)
		{
			_list_view->insertNodeAtLast(this->createRandomNode());
			_list_view->reloadData();
		}
		else
		{
			int count = _list_view->getNodesCount();
			
			if (count == 0)
			{
				_list_view->insertNode(this->createRandomNode(), 0);
				_list_view->reloadData();
			}
			else
			{
				int r = rand() % count;

				_list_view->insertNode(this->createRandomNode(), r);
				_list_view->reloadData();
			}			
		}
	} 
}

void NListView_Vertical::btnRemoveCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		if (btn->getTag() == 1)
		{
			_list_view->removeFrontNode();
			_list_view->reloadData();
		}
		else if (btn->getTag() == 2)
		{
			_list_view->removeLastNode();
			_list_view->reloadData();
		}
		else
		{
			int count = _list_view->getNodesCount();

			if (count == 0)
			{
				return ;
			}
			int r = rand() % count;
			_list_view->removeNodeAtIndex(r);
			_list_view->reloadData();
		}
	}
}

Node* NListView_Vertical::createRandomNode()
{
	float width = rand() % 200 + 24;
	float height = rand() % 80 + 10;

	float r = rand() % 255;
	float g = rand() % 255;
	float b = rand() % 255;

	LayerColor* layer = LayerColor::create(ccc4(r, g, b, 255));
	layer->ignoreAnchorPointForPosition(false);
	layer->setColor(Color3B(r, g, b));
	layer->setContentSize(Size(width, height));
	
	char str[10] = { };
	sprintf(str, "%d", _count);
	_count ++;

	LabelTTF* lbl = LabelTTF::create(str, "Arial", layer->getContentSize().height * 0.8f);
	layer->addChild(lbl);
	lbl->setPosition(POS_BY_NODE(layer, 0.5f, 0.5f));

	return layer;
}

//
// NListView_Horizontal
//
NListView_Horizontal::NListView_Horizontal(void)
	: _count(1)
{
	this->setIndex(1);
}

NListView_Horizontal::~NListView_Horizontal(void)
{

}

bool NListView_Horizontal::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNListView::init());

	srand((unsigned) time (nullptr));

	_list_view = NListView::create(Size(VISIBLE_SIZE.width * 0.5f, VISIBLE_SIZE.height * 0.5f));
	this->addChild(_list_view);
	_list_view->setPosition(VISIBLE_SIZE_CENTER);
	_list_view->setDirection(ScrollView_Direction::kScrollView_Dir_Horizontal);
	_list_view->setAlignType(ListView_Align_Type::kListView_Aligan_Horizontal_Top);
	
	LayerColor* layer = LayerColor::create(ccc4(255, 255, 255, 255));
	layer->ignoreAnchorPointForPosition(false);
	layer->setColor(Color3B(255, 255, 255));
	this->addChild(layer, -10);
	layer->setPosition(VISIBLE_SIZE_CENTER);
	layer->setAnchorPoint(Vec2(0.5f, 0.5f));
	layer->setContentSize(Size(VISIBLE_SIZE.width * 0.5f, VISIBLE_SIZE.height * 0.5f));

	// top align
	NButton* btn_top_align = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_top_align);
	btn_top_align->setPosition(POS_BY_VISIBLE_SIZE(0.1f, 0.76f));
	btn_top_align->setTag((int)ListView_Align_Type::kListView_Aligan_Horizontal_Top);
	btn_top_align->setCallback(CALLBACK_2(NListView_Horizontal::btnAlignCallback, this));
	btn_top_align->setText("top align");

	// center align
	NButton* btn_center_align = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_center_align);
	btn_center_align->setPosition(POS_BY_VISIBLE_SIZE(0.1f, 0.5f));
	btn_center_align->setTag((int)ListView_Align_Type::kListView_Aligan_Horizontal_Center);
	btn_center_align->setCallback(CALLBACK_2(NListView_Horizontal::btnAlignCallback, this));
	btn_center_align->setText("center align");

	// buttom align
	NButton* btn_buttom_align = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_buttom_align);
	btn_buttom_align->setPosition(POS_BY_VISIBLE_SIZE(0.1f, 0.25f));
	btn_buttom_align->setTag((int)ListView_Align_Type::kListView_Aligan_Horizontal_Bottom);
	btn_buttom_align->setCallback(CALLBACK_2(NListView_Horizontal::btnAlignCallback, this));
	btn_buttom_align->setText("buttom align");

	// insert
	// insert front
	NButton* btn_insert_front = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_insert_front);
	btn_insert_front->setPosition(POS_BY_VISIBLE_SIZE(0.2f, 0.9f));
	btn_insert_front->setCallback(CALLBACK_2(NListView_Horizontal::btnInsertCallback, this));
	btn_insert_front->setText("insert front");
	btn_insert_front->setTag(1);

	// insert last
	NButton* btn_insert_last = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_insert_last);
	btn_insert_last->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.9f));
	btn_insert_last->setCallback(CALLBACK_2(NListView_Horizontal::btnInsertCallback, this));
	btn_insert_last->setText("insert last");
	btn_insert_last->setTag(2);

	// insert random
	NButton* btn_insert_random = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_insert_random);
	btn_insert_random->setPosition(POS_BY_VISIBLE_SIZE(0.8f, 0.9f));
	btn_insert_random->setCallback(CALLBACK_2(NListView_Horizontal::btnInsertCallback, this));
	btn_insert_random->setText("insert random");
	btn_insert_random->setTag(3);

	// remove
	// remove front
	NButton* btn_remove_front = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_remove_front);
	btn_remove_front->setPosition(POS_BY_VISIBLE_SIZE(0.2f, 0.1f));
	btn_remove_front->setCallback(CALLBACK_2(NListView_Horizontal::btnRemoveCallback, this));
	btn_remove_front->setText("remove front");
	btn_remove_front->setTag(1);

	// remove last
	NButton* btn_remove_last = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_remove_last);
	btn_remove_last->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.1f));
	btn_remove_last->setCallback(CALLBACK_2(NListView_Horizontal::btnRemoveCallback, this));
	btn_remove_last->setText("remove last");
	btn_remove_last->setTag(2);

	// remove random
	NButton* btn_remove_random = NButton::create("btn_1.png", "btn_2.png");
	this->addChild(btn_remove_random);
	btn_remove_random->setPosition(POS_BY_VISIBLE_SIZE(0.8f, 0.1f));
	btn_remove_random->setCallback(CALLBACK_2(NListView_Horizontal::btnRemoveCallback, this));
	btn_remove_random->setText("remove random");
	btn_remove_random->setTag(3);

	INIT_DO_WHILE_END
}

void NListView_Horizontal::btnAlignCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		_list_view->setAlignType((ListView_Align_Type)btn->getTag());
	}
}

void NListView_Horizontal::btnInsertCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		if (btn->getTag() == 1)
		{
			_list_view->insertNodeAtFront(this->createRandomNode());
			_list_view->reloadData();
		}
		else if (btn->getTag() == 2)
		{
			_list_view->insertNodeAtLast(this->createRandomNode());
			_list_view->reloadData();
		}
		else
		{
			int count = _list_view->getNodesCount();

			if (count == 0)
			{
				_list_view->insertNode(this->createRandomNode(), 0);
				_list_view->reloadData();
			}
			else
			{
				int r = rand() % count;

				_list_view->insertNode(this->createRandomNode(), r);
				_list_view->reloadData();
			}			
		}
	} 
}

void NListView_Horizontal::btnRemoveCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		if (btn->getTag() == 1)
		{
			_list_view->removeFrontNode();
			_list_view->reloadData();
		}
		else if (btn->getTag() == 2)
		{
			_list_view->removeLastNode();
			_list_view->reloadData();
		}
		else
		{
			int count = _list_view->getNodesCount();

			if (count == 0)
			{
				return ;
			}
			int r = rand() % count;
			_list_view->removeNodeAtIndex(r);
			_list_view->reloadData();
		}
	}
}

Node* NListView_Horizontal::createRandomNode()
{
	float width = rand() % 80 + 15;
	float height = rand() % 250 + 10;

	float r = rand() % 255;
	float g = rand() % 255;
	float b = rand() % 255;

	LayerColor* layer = LayerColor::create(ccc4(r, g, b, 255));
	layer->ignoreAnchorPointForPosition(false);
	layer->setColor(Color3B(r, g, b));
	layer->setContentSize(Size(width, height));

	char str[10] = { };
	sprintf(str, "%d", _count);
	_count ++;

	LabelTTF* lbl = LabelTTF::create(str, "Arial", layer->getContentSize().width * 0.8f);
	layer->addChild(lbl);
	lbl->setPosition(POS_BY_NODE(layer, 0.5f, 0.5f));

	return layer;
}