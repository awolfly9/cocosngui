#ifndef __TEST_NRICH_TEXT_H__
#define __TEST_NRICH_TEXT_H__

#include "BasicLayer.h"

#define  MAX_RICH_TEXT_LAYER_INDEX 2

class TestNRichText : public BasicLayer
{
public:
	TestNRichText(void);
	~TestNRichText(void);

	CREATE_FUNC(TestNRichText);
	virtual bool init();

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};

class NRichText_Element : public TestNRichText
{
public:
	NRichText_Element(void);
	~NRichText_Element(void);

	CREATE_FUNC(NRichText_Element);
	virtual bool init();
};

class NRichText_VerticalSpace : public TestNRichText
{
public:
	NRichText_VerticalSpace(void);
	~NRichText_VerticalSpace(void);

	CREATE_FUNC(NRichText_VerticalSpace);
	virtual bool init();
};

////////////////////////////////////////////////////////
static Layer* createNRichTextLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NRichText_Element::create();
		}
		break;
	case 1:
		{
			return NRichText_VerticalSpace::create();
		}
		break;
	default:
		break;
	}

	return nullptr;
}

static Layer* onNextNRichTextLayer(int index)
{
	index ++;

	if (index == MAX_RICH_TEXT_LAYER_INDEX)
	{
		index = 0;
	}

	return createNRichTextLayer(index);
}

static Layer* onBackNRichTextLayer(int index)
{
	index --;

	if (index == -1)
	{
		index = MAX_RICH_TEXT_LAYER_INDEX - 1;
	}

	return createNRichTextLayer(index);
}

static Layer* onAgainNRichTextLayer(int index)
{
	return createNRichTextLayer(index);
}

#endif // __TEST_NRICH_TEXT_H__
