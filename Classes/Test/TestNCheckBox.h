#ifndef __TEST_NCHECK_BOX_H__
#define __TEST_NCHECK_BOX_H__

#include "BasicLayer.h"

#define MAX_CHECK_BOX_LAYER_INDEX 4

/**
TestNCheckBox
*/
class TestNCheckBox : public BasicLayer
{
public:
	TestNCheckBox(void);
	~TestNCheckBox(void);

	CREATE_FUNC(TestNCheckBox);
	virtual bool init();

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};

/**
NCheckBox_Callback
*/
class NCheckBox_Callback : public TestNCheckBox
{
public: 
	NCheckBox_Callback(void);
	~NCheckBox_Callback(void);

	CREATE_FUNC(NCheckBox_Callback);
	virtual bool init();

private:
	void ckbCallback(Ref* object, Touch_Period touch_period);

private:
	LabelTTF* _lbl_check;
};

/**
NCheckBox_HideNormalImage
*/
class NCheckBox_HideNormalImage : public TestNCheckBox
{
public: 
	NCheckBox_HideNormalImage(void);
	~NCheckBox_HideNormalImage(void);

	CREATE_FUNC(NCheckBox_HideNormalImage);
	virtual bool init();

private:
	void ckbCallback(Ref* object, Touch_Period touch_period);

private:
	LabelTTF* _lbl_check;
	NCheckBox* _ckb_hide;
};

/**
NCheckBox_Action
*/
class NCheckBox_Action : public TestNCheckBox
{
public: 
	NCheckBox_Action(void);
	~NCheckBox_Action(void);

	CREATE_FUNC(NCheckBox_Action);
	virtual bool init();
};

/**
NCheckBox_Tips
*/
class NCheckBox_Tips : public TestNCheckBox
{
public: 
	NCheckBox_Tips(void);
	~NCheckBox_Tips(void);

	CREATE_FUNC(NCheckBox_Tips);
	virtual bool init();

private:
	void ckbCallback(Ref* object, Touch_Period touch_period);
};


////////////////////////////////////////////////////////
static Layer* createNCheckBoxLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NCheckBox_Callback::create();
		}
		break;
	case 1:
		{
			return NCheckBox_HideNormalImage::create();
		}
		break;
	case 2:
		{
			return NCheckBox_Action::create();
		}
		break;
	case 3:
		{
			return NCheckBox_Tips::create();
		}
		break;
	default:
		break;
	}

	return nullptr;
}

static Layer* onNextNCheckBoxLayer(int index)
{
	index ++;

	if (index == MAX_CHECK_BOX_LAYER_INDEX)
	{
		index = 0;
	}

	return createNCheckBoxLayer(index);
}

static Layer* onBackNCheckBoxLayer(int index)
{
	index --;

	if (index == -1)
	{
		index = MAX_CHECK_BOX_LAYER_INDEX - 1;
	}

	return createNCheckBoxLayer(index);
}

static Layer* onAgainNCheckBoxLayer(int index)
{
	return createNCheckBoxLayer(index);
}


#endif // __TEST_NCHECK_BOX_H__
