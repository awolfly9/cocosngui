#include "TestNAudioManager.h"

//
// TestNAudioManager
//
TestNAudioManager::TestNAudioManager(void)
{
}

TestNAudioManager::~TestNAudioManager(void)
{
}

bool TestNAudioManager::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

void TestNAudioManager::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextAudioLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNAudioManager::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackAudioLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNAudioManager::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainAudioLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NAudioManager_Async_Effect
//
NAudioManager_Async_Effect::NAudioManager_Async_Effect()
	: _count(0)
	, _bar(nullptr)
{
	this->setIndex(0);
}

NAudioManager_Async_Effect::~NAudioManager_Async_Effect()
{
	AUDIO_MANAGER->stopAllEffects();
}

bool NAudioManager_Async_Effect::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNAudioManager::init());

	_audios.push_back("audio/DefaultBadDropAudio.mp3");
	_audios.push_back("audio/DefaultClickAudio.mp3");
	_audios.push_back("audio/DefaultDropAudio.mp3");
	_audios.push_back("audio/DefaultMovingBuidlingAudioA.mp3");
	_audios.push_back("audio/DefaultMovingBuidlingAudioB.mp3");
	_audios.push_back("audio/DefaultSelectAudio.mp3");
	_audios.push_back("audio/common_craft_success.mp3");

	_bar = NProgressBar::create("progress_bg.png", "progress.png");
	_bar->setCurrentValue(0);
	_bar->setMaxValue(7);
	_bar->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));
	this->addChild(_bar);

	_lbl = Label::createWithSystemFont("", "Arial", 30);
	this->addChild(_lbl);
	_lbl->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.75f));
	_lbl->setColor(Color3B::RED);

	Director::getInstance()->getScheduler()->schedule(CALLBACK_1(NAudioManager_Async_Effect::loading, this), this, 0.5f, false, "loading");

	INIT_DO_WHILE_END
}

void NAudioManager_Async_Effect::callback(const string& file_path)
{
	log("file path :%s", file_path.c_str());
	_count ++;
	_bar->setCurrentValue(_count);

	_lbl->setString(file_path + " load finish");
}

void NAudioManager_Async_Effect::loading(float dt)
{
	if (_audios.size() >= 1)
	{
		string audio = _audios.at(0);
		_audios.erase(_audios.begin());
		AUDIO_MANAGER->preloadEffectAsync(audio, CALLBACK_1(NAudioManager_Async_Effect::callback, this));
	}
	else
	{
		_lbl->setString("load finish");
	}
}

//
// Audio_Callback
//
//NAudioManager_Async_Bg_Music::NAudioManager_Async_Bg_Music()
//	: _count(0)
//	, _bar(nullptr)
//{
//	this->setIndex(1);
//}
//
//NAudioManager_Async_Bg_Music::~NAudioManager_Async_Bg_Music()
//{
//	AUDIO_MANAGER->stopBgMusic(true);
//}
//
//bool NAudioManager_Async_Bg_Music::init()
//{
//	INIT_DO_WHILE_BEGIN
//		BREAK_IF(! TestNAudioManager::init());
//
//	AUDIO_MANAGER->preloadBgMusicAsync("audio/City01.mp3", CALLBACK_1(NAudioManager_Async_Bg_Music::callback, this));
//	AUDIO_MANAGER->preloadBgMusicAsync("audio/DefaultBackgroundMusic.mp3", CALLBACK_1(NAudioManager_Async_Bg_Music::callback, this));
//	AUDIO_MANAGER->preloadBgMusicAsync("audio/jail01.mp3", CALLBACK_1(NAudioManager_Async_Bg_Music::callback, this));
//	AUDIO_MANAGER->preloadBgMusicAsync("audio/protoss01.mp3", CALLBACK_1(NAudioManager_Async_Bg_Music::callback, this));
//	AUDIO_MANAGER->preloadBgMusicAsync("audio/lab01.mp3", CALLBACK_1(NAudioManager_Async_Bg_Music::callback, this));
//	AUDIO_MANAGER->preloadBgMusicAsync("audio/terran03.mp3", CALLBACK_1(NAudioManager_Async_Bg_Music::callback, this));
//	AUDIO_MANAGER->preloadBgMusicAsync("audio/City01.mp3", CALLBACK_1(NAudioManager_Async_Bg_Music::callback, this));
//
//
//	_bar = NProgressBar::create("progress_bg.png", "progress.png");
//	this->addChild(_bar);
//	_bar->setCurrentValue(0);
//	_bar->setMaxValue(7);
//	_bar->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));
//
//	NButton* btn = NButton::create("btn_1.png", "btn_2.png");
//	this->addChild(btn);
//	btn->setCallback(CALLBACK_2(NAudioManager_Async_Bg_Music::btnCallback, this));
//	btn->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.3f));
//
//	INIT_DO_WHILE_END
//}
//
//void NAudioManager_Async_Bg_Music::callback(const string& file_path)
//{
//	log("file path :%s", file_path.c_str());
//	_count ++;
//	_bar->setCurrentValue(_count);
//}
//
//void NAudioManager_Async_Bg_Music::btnCallback(Ref* ref, Touch_Period touch_period)
//{
//	if (touch_period == Touch_Period::kTouchEnded)
//	{
//		AUDIO_MANAGER->playBgMusic("audio/battle_bgm_arena.mp3");
//	}
//}