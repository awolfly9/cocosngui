#ifndef __TEST_NSCROLL_BAR_H__
#define __TEST_NSCROLL_BAR_H__

#include "BasicLayer.h"

#define MAX_SCROLL_BAR_LAYER_INDEX 2

class TestNScrollBar : public BasicLayer
{
public:
	TestNScrollBar(void);
	~TestNScrollBar(void);

	CREATE_FUNC(TestNScrollBar);
	virtual bool init();

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};

/**
NScrollBar Horizontal
*/
class NScrollBar_Horizontal : public TestNScrollBar
{
public:
	NScrollBar_Horizontal(void);
	~NScrollBar_Horizontal(void);

	CREATE_FUNC(NScrollBar_Horizontal);
	virtual bool init();

private:
	void barCallback(Ref* object, Bar_Period bar_period);

	NScrollBar* _scroll_bar;
	LabelTTF* _lbl_value;
	LabelTTF* _lbl_percentage;
};

/**
NScrollBar Vertical
*/
class NScrollBar_Vertical : public TestNScrollBar
{
public:
	NScrollBar_Vertical();
	~NScrollBar_Vertical();

	CREATE_FUNC(NScrollBar_Vertical);
	virtual bool init();

private:
	void barCallback(Ref* object, Bar_Period bar_period);

private:
	NLabelTTF* _lbl_value;
	NLabelTTF* _lbl_percentage;

};



////////////////////////////////////////////////////////
static Layer* createNScrollBarLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NScrollBar_Horizontal::create();
		}
		break;
	case 1:
		{
			return NScrollBar_Vertical::create();
		}
		break;
	case 2:
		{
			//return ButtonState::create();
		}
		break;
	case 3:
		{
			//return ButtonText::create();
		}
		break;
	case 4:
		{
			//return ButtonScale9Sprite::create();
		}
		break;
	default:
		break;
	}

	return nullptr;
}

static Layer* onNextNScrollBarLayer(int index)
{
	index ++;

	if (index == MAX_SCROLL_BAR_LAYER_INDEX)
	{
		index = 0;
	}

	return createNScrollBarLayer(index);
}

static Layer* onBackNScrollBarLayer(int index)
{
	index --;

	if (index == -1)
	{
		index = MAX_SCROLL_BAR_LAYER_INDEX - 1;
	}

	return createNScrollBarLayer(index);
}

static Layer* onAgainNScrollBarLayer(int index)
{
	return createNScrollBarLayer(index);
}

#endif // __TEST_NSCROLL_BAR_H__
