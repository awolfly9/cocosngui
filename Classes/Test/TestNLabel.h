//#ifndef __TEST_NLABEL_H__
//#define __TEST_NLABEL_H__
//
//#include "BasicLayer.h"
//
//#define MAX_LABEL_LAYER_INDEX 2
//
//class TestNLabel : public BasicLayer
//{
//public:
//	TestNLabel(void);
//	~TestNLabel(void);
//
//	CREATE_FUNC(TestNLabel);
//	virtual bool init();
//
//private:
//	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
//	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
//	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
//};
//
//class NLabel_Horizontal : public TestNLabel
//{
//public:
//	NLabel_Horizontal();
//	~NLabel_Horizontal();
//
//	CREATE_FUNC(NLabel_Horizontal);
//	virtual bool init();
//
//private:
//	void btnCallback(Ref* object, Touch_Period touch_period);
//
//private:
//	NLabelTTF* _label;
//};
//
//class NLabel_Vertical : public TestNLabel
//{
//public:
//	NLabel_Vertical();
//	~NLabel_Vertical();
//
//	CREATE_FUNC(NLabel_Vertical);
//	virtual bool init();
//
//private:
//	void btnCallback(Ref* object, Touch_Period touch_period);
//
//private:
//	NLabelTTF* _label;
//};
//
//////////////////////////////////////////////////////////
//static Layer* createLabelLayer(int index)
//{
//	switch (index)
//	{
//	case 0:
//		{
//			return NLabel_Horizontal::create();
//		}
//		break;
//	case 1:
//		{
//			return NLabel_Vertical::create();
//		}
//		break;
//	case 2:
//		{
//		
//		}
//		break;
//	case 3:
//		{
//			
//		}
//		break;
//	case 4:
//		{
//			
//		}
//		break;
//	default:
//		break;
//	}
//
//	return nullptr;
//}
//
//static Layer* onNextLabelLayer(int index)
//{
//	index ++;
//
//	if (index == MAX_LABEL_LAYER_INDEX)
//	{
//		index = 0;
//	}
//
//	return createLabelLayer(index);
//}
//
//static Layer* onBackLabelLayer(int index)
//{
//	index --;
//
//	if (index == -1)
//	{
//		index = MAX_LABEL_LAYER_INDEX - 1;
//	}
//
//	return createLabelLayer(index);
//}
//
//static Layer* onAgainLabelLayer(int index)
//{
//	return createLabelLayer(index);
//}
//
//#endif // __TEST_NLABEL_H__
