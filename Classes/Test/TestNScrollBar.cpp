#include "TestNScrollBar.h"

//
// TestNScrollBar
//
TestNScrollBar::TestNScrollBar(void)
{
	this->setIndex(-1);
}

TestNScrollBar::~TestNScrollBar(void)
{
}

bool TestNScrollBar::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

void TestNScrollBar::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if(touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextNScrollBarLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNScrollBar::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if(touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackNScrollBarLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNScrollBar::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainNScrollBarLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NScrollBar_Horizontal
//
NScrollBar_Horizontal::NScrollBar_Horizontal(void)
{
	this->setIndex(0);
}

NScrollBar_Horizontal::~NScrollBar_Horizontal(void)
{

}

bool NScrollBar_Horizontal::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNScrollBar::init());

	_lbl_value = LabelTTF::create();
	this->addChild(_lbl_value);
	_lbl_value->setPosition(Vec2(VISIBLE_SIZE.width * 0.3f, VISIBLE_SIZE.height * 0.56f));
	_lbl_value->setFontSize(30);

	_lbl_percentage = LabelTTF::create();
	this->addChild(_lbl_percentage);
	_lbl_percentage->setPosition(Vec2(VISIBLE_SIZE.width * 0.7f, VISIBLE_SIZE.height * 0.56f));
	_lbl_percentage->setFontSize(30);

	_scroll_bar = NScrollBar::create("progress_bg.png");
	this->addChild(_scroll_bar);
	_scroll_bar->setScrollBar("scroll_bar_2.png", "scroll_bar_1.png", "scroll_bar_1.png");
	_scroll_bar->setPosition(VISIBLE_SIZE_CENTER);
	_scroll_bar->setBarCallback(CALLBACK_2(NScrollBar_Horizontal::barCallback, this));
	_scroll_bar->setMaxValue(100);
	_scroll_bar->setMinValue(0);
	_scroll_bar->setPercentage(0.5f);	
	_scroll_bar->setDirection(Bar_Direction::kBar_Dir_LeftToRight);

	INIT_DO_WHILE_END
}

void NScrollBar_Horizontal::barCallback(Ref* object, Bar_Period bar_period)
{
	NScrollBar* bar = (NScrollBar*)object;

	char str_1[50] = {};
	sprintf(str_1, "current value %d,", bar->getCurrentValue());

	char str_2[50] = {};
	sprintf(str_2, "current percentage %2f,", bar->getPercentage());

	_lbl_value->setString(str_1);
	_lbl_percentage->setString(str_2);
}

//
// NScrollBar_Vertical
//
NScrollBar_Vertical::NScrollBar_Vertical()
{
	this->setIndex(1);
}

NScrollBar_Vertical::~NScrollBar_Vertical()
{
}

bool NScrollBar_Vertical::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNScrollBar::init());

	_lbl_value = NLabelTTF::create("", "Arial", 30, Text_Direction::kText_Dir_Vertical);
	this->addChild(_lbl_value);
	_lbl_value->setPosition(Vec2(VISIBLE_SIZE.width * 0.3f, VISIBLE_SIZE.height * 0.56f));

	_lbl_percentage = NLabelTTF::create("", "Arial", 30, Text_Direction::kText_Dir_Vertical);
	this->addChild(_lbl_percentage);
	_lbl_percentage->setPosition(Vec2(VISIBLE_SIZE.width * 0.7f, VISIBLE_SIZE.height * 0.56f));

	NScrollBar* scroll_bar = NScrollBar::create("progress_v_bg.png");
	this->addChild(scroll_bar);
	scroll_bar->setScrollBar("scroll_bar_v_2.png", "scroll_bar_v_1.png", "scroll_bar_v_1.png");
	scroll_bar->setPosition(VISIBLE_SIZE_CENTER);
	scroll_bar->setBarCallback(CALLBACK_2(NScrollBar_Vertical::barCallback, this));
	scroll_bar->setMaxValue(100);
	scroll_bar->setMinValue(0);
	scroll_bar->setPercentage(0.5f);	
	scroll_bar->setDirection(Bar_Direction::kBar_Dir_ButtomToTop);

	INIT_DO_WHILE_END
}

void NScrollBar_Vertical::barCallback(Ref* object, Bar_Period bar_period)
{
	NScrollBar* bar = (NScrollBar*)object;

	char str_1[50] = {};
	sprintf(str_1, "current value %d,", bar->getCurrentValue());

	char str_2[50] = {};
	sprintf(str_2, "current percentage %2f,", bar->getPercentage());

	// 这样写 在我这种电脑上运行起来很卡，在 NLabelTTF 中还存在不足，每一次动态的更新字符信息都需要去遍历然后手动的加上 \n 
	_lbl_value->setString(str_1);
	_lbl_percentage->setString(str_2);
}