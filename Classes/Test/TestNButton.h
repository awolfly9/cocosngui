#ifndef __TEST_NBUTTON__H__
#define __TEST_NBUTTON__H__


#include <BasicLayer.h>

#define MAX_BUTTON_LAYER_INDEX 6

/**
TestNButtonLayer
*/
class TestNButton : public BasicLayer 
{
public:
	TestNButton(void);
	~TestNButton(void);

	CREATE_FUNC(TestNButton);
	virtual bool init();

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};

/**
NButton_Callback
*/
class NButton_Callback : public TestNButton
{
public:
	NButton_Callback(void);
	~NButton_Callback(void);

	CREATE_FUNC(NButton_Callback);
	virtual bool init();

private:
	void btnCallback(Ref* object, Touch_Period touch_period);

private:
	NLabelTTF* _lbl_text;
};

/**
NButton_Zorder
*/
class NButton_Zorder : public TestNButton
{
public:
	NButton_Zorder(void);
	~NButton_Zorder(void);

	CREATE_FUNC(NButton_Zorder);
	virtual bool init();

private:
	void btnCallback(Ref* object, Touch_Period touch_period);

private:
	NButton* _btn_1;
	NButton* _btn_2;
};

/**
NButton_State
*/
class NButton_State : public TestNButton
{
public:
	NButton_State(void);
	~NButton_State(void);

	CREATE_FUNC(NButton_State);
	virtual bool init();

};

/**
NButton_Text
*/
class NButton_Text : public TestNButton
{
public:
	NButton_Text(void);
	~NButton_Text(void);

	CREATE_FUNC(NButton_Text);
	virtual bool init();
};

/**
NButton_Action
*/ 
class NButton_Action : public TestNButton
{
public:
	NButton_Action(void);
	~NButton_Action(void);

	CREATE_FUNC(NButton_Action);
	virtual bool init();
};

/**
NButton_Tips
*/ 
class NButton_Tips : public TestNButton
{
public:
	NButton_Tips(void);
	~NButton_Tips(void);

	CREATE_FUNC(NButton_Tips);
	virtual bool init();

private:
	void btnCallback(Ref* object, Touch_Period touch_period);
};

////////////////////////////////////////////////////////
static Layer* createNButtonLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NButton_Callback::create();
		}
		break;
	case 1:
		{
			return NButton_Zorder::create();
		}
		break;
	case 2:
		{
			return NButton_State::create();
		}
		break;
	case 3:
		{
			return NButton_Text::create();
		}
		break;
	case 4:
		{
			return NButton_Action::create();
		}
		break;
	case 5:
		{
			return NButton_Tips::create();
		}
		break;
	default:
		break;
	}

	return nullptr;
}

static Layer* onNextNButtonLayer(int index)
{
	index ++;

	if (index == MAX_BUTTON_LAYER_INDEX)
	{
		index = 0;
	}

	return createNButtonLayer(index);
}

static Layer* onBackNButtonLayer(int index)
{
	index --;

	if (index == -1)
	{
		index = MAX_BUTTON_LAYER_INDEX - 1;
	}

	return createNButtonLayer(index);
}

static Layer* onAgainNButtonLayer(int index)
{
	return createNButtonLayer(index);
}


#endif // __TEST_NBUTTON__H__
