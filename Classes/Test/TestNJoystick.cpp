#include "TestNJoystick.h"

//
// TestNJoystick
//
TestNJoystick::TestNJoystick(void)
{
}

TestNJoystick::~TestNJoystick(void)
{
}

bool TestNJoystick::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

void TestNJoystick::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextNJoystickLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNJoystick::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackNJoystickLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNJoystick::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainNJoystickLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NJoystick_Handle
//
NJoystick_Handle::NJoystick_Handle()
	: _player(nullptr)
	, _speed(5)
{
	this->setIndex(0);
}

NJoystick_Handle::~NJoystick_Handle()
{

}

bool NJoystick_Handle::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNJoystick::init());

	_player = Sprite::create("icon.png");
	_player->setPosition(VISIBLE_SIZE_CENTER);
	this->addChild(_player);

	NJoystick* jk = NJoystick::create("control_baseboard.png", "control_joystick.png", 64);
	this->addChild(jk);
	jk->setPosition(VISIBLE_SIZE.width * 0.2f, VISIBLE_SIZE.height * 0.3f);
	jk->setCallback(CALLBACK_2(NJoystick_Handle::handleCallback, this));

	INIT_DO_WHILE_END
}

void NJoystick_Handle::handleCallback(Ref* ref, Touch_Period touch_period)
{
	if (_player != nullptr)
	{
		NJoystick* jk = (NJoystick*)(ref);

		if (jk != nullptr)
		{
			Vec2 pos = Vec2(_speed * jk->getDir().x, _speed * jk->getDir().y);
			_player->setPosition(pos + _player->getPosition());
		}		
	}
}