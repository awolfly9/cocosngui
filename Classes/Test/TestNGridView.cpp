#include "TestNGridView.h"

//
// TestNGridView
//
TestNGridView::TestNGridView(void)
{
	this->setIndex(-1);
}

TestNGridView::~TestNGridView(void)
{
}

bool TestNGridView::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

Ref* TestNGridView::loadCell(Ref* object, int index)
{
	NGridViewCell* cell = NGridViewCell::create();

	float r = rand() % 255;
	float g = rand() % 255;
	float b = rand() % 255;

	LayerColor* layer = LayerColor::create(ccc4(r, g, b, 255));
	layer->ignoreAnchorPointForPosition(false);
	layer->setColor(Color3B(r, g, b));
	layer->setContentSize(Size(100, 100));
	cell->addChild(layer);
	layer->setPosition(POS_BY_NODE(cell, 0.5f, 0.5f));

	char str[10] = { };
	sprintf(str, "%d", index);

	LabelTTF* lbl = LabelTTF::create(str, "Arial", layer->getContentSize().height * 0.8f);
	layer->addChild(lbl);
	lbl->setPosition(POS_BY_NODE(layer, 0.5f, 0.5f));

	return cell;
}

void TestNGridView::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextNGridViewLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNGridView::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackNGridViewLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNGridView::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainNGridViewLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NGridView_Vertical
//
NGridView_Vertical::NGridView_Vertical(void)
{
	this->setIndex(0);
}

NGridView_Vertical::~NGridView_Vertical(void)
{

}

bool NGridView_Vertical::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNGridView::init());
	
	NGridView* grid_view = NGridView::create(Size(400, 400), Size(100, 100), 100, 4, CALLBACK_2(NGridView_Vertical::loadCell, this));
	this->addChild(grid_view);
	grid_view->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));
	grid_view->setAutoRelocate(true);
	grid_view->reloadData();

	INIT_DO_WHILE_END
}