#include "TestNSliderBar.h"

//
// TestNSliderBar
//
TestNSliderBar::TestNSliderBar(void)
{
	this->setIndex(-1);
}

TestNSliderBar::~TestNSliderBar(void)
{
}

bool TestNSliderBar::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

void TestNSliderBar::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if(touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextNSliderBarLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNSliderBar::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if(touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackNSliderBarLayer(_index);
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNSliderBar::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if(touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainNSliderBarLayer(_index);
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NSliderBar_Horizontal
//
NSliderBar_Horizontal::NSliderBar_Horizontal()
{
	this->setIndex(0);
}

NSliderBar_Horizontal::~NSliderBar_Horizontal()
{

}

bool NSliderBar_Horizontal::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNSliderBar::init());

	NScale9Sprite* bg = NScale9Sprite::create("background3.png");
	this->addChild(bg, -1);
	bg->setContentSize(Size(VISIBLE_SIZE.width * 0.85f, VISIBLE_SIZE.height * 0.85f));
	bg->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));

	_slider_bar = NSliderBar::create("progress_bg.png", "progress.png");
	_slider_bar->setSliderBar("slider.png", "slider.png", "slider.png", "");
	this->addChild(_slider_bar);
	_slider_bar->setDirection(Bar_Direction::kBar_Dir_LeftToRight);
	_slider_bar->setPercentage(0.2f);
	_slider_bar->setPosition(VISIBLE_SIZE_CENTER);
	_slider_bar->setMaxValue(100);
	_slider_bar->setMinValue(0);
	_slider_bar->setBarCallback(CALLBACK_2(NSliderBar_Horizontal::barShowCallback, this));

	NButton* btn_exchange_direction = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_exchange_direction);
	btn_exchange_direction->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.85f));
	btn_exchange_direction->setCallback(CALLBACK_2(NSliderBar_Horizontal::btnDirectionCallback, this));
	btn_exchange_direction->setText("enchange direction");

	NButton* btn_add_progress_value = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_add_progress_value);
	btn_add_progress_value->setPosition(POS_BY_VISIBLE_SIZE(0.2f, 0.3f));
	btn_add_progress_value->setCallback(CALLBACK_2(NSliderBar_Horizontal::btnAddProgressCallback, this));
	btn_add_progress_value->setTag(1);
	btn_add_progress_value->setText("add progress value");

	NButton* btn_add_progress_percentage = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_add_progress_percentage);
	btn_add_progress_percentage->setPosition(POS_BY_VISIBLE_SIZE(0.2f, 0.7f));
	btn_add_progress_percentage->setCallback(CALLBACK_2(NSliderBar_Horizontal::btnAddProgressCallback, this));
	btn_add_progress_percentage->setTag(2);
	btn_add_progress_percentage->setText("add progress perventage");

	NButton* btn_reduce_progress_value = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_reduce_progress_value);
	btn_reduce_progress_value->setPosition(POS_BY_VISIBLE_SIZE(0.8f, 0.3f));
	btn_reduce_progress_value->setCallback(CALLBACK_2(NSliderBar_Horizontal::btnReduceProgressCallback, this));
	btn_reduce_progress_value->setTag(1);
	btn_reduce_progress_value->setText("reduce progress value");

	NButton* btn_reduce_progress_percentage = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_reduce_progress_percentage);
	btn_reduce_progress_percentage->setPosition(POS_BY_VISIBLE_SIZE(0.8f, 0.7f));
	btn_reduce_progress_percentage->setCallback(CALLBACK_2(NSliderBar_Horizontal::btnReduceProgressCallback, this));
	btn_reduce_progress_percentage->setTag(2);
	btn_reduce_progress_percentage->setText("reduce progress percentage");

	NButton* btn_stop_progress = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_stop_progress);
	btn_stop_progress->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.15f));
	btn_stop_progress->setCallback(CALLBACK_2(NSliderBar_Horizontal::btnStopCallback, this));
	btn_stop_progress->setText("stop progress");

	INIT_DO_WHILE_END
}

void NSliderBar_Horizontal::btnAddProgressCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		if (btn->getTag() == 1)
		{
			_slider_bar->startValueBy(0.2f, 5);
		}
		else
		{
			_slider_bar->startPercentageTo(2.0f, 1.0f);
		}
	}
}

void NSliderBar_Horizontal::btnReduceProgressCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		if (btn->getTag() == 1)
		{
			_slider_bar->startValueBy(0.2f, -5);
		}
		else
		{
			_slider_bar->startPercentageTo(2.0f, 0.0f);
		}
	}
}

void NSliderBar_Horizontal::btnDirectionCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		if (_slider_bar->getDirection() == Bar_Direction::kBar_Dir_LeftToRight)
		{
			_slider_bar->setDirection(Bar_Direction::kBar_Dir_RightToLeft);
		}
		else
		{
			_slider_bar->setDirection(Bar_Direction::kBar_Dir_LeftToRight);
		}
	}
}

void NSliderBar_Horizontal::btnStopCallback(Ref* object, Touch_Period touch_period)
{
	if ( touch_period == Touch_Period::kTouchEnded)
	{
		_slider_bar->stopUpdatePercentage();
	}
}

void NSliderBar_Horizontal::barShowCallback(Ref* object, Bar_Period bar_period)
{
	NSliderBar* bar = (NSliderBar*)object;
	switch (bar_period)
	{
	case Bar_Period::kBar_Began:
		{
			log("bar began");
			log("Progress Bar current value : %d, current percentage : %f", bar->getCurrentValue(), bar->getPercentage());
		}
		break;
	case Bar_Period::kBar_Move:
		{
			log("bar change");
			log("Progress Bar current value : %d, current percentage : %f", bar->getCurrentValue(), bar->getPercentage());
		}
		break;
	case Bar_Period::kBar_End:
		{
			log("bar end");
			log("Progress Bar current value : %d, current percentage : %f", bar->getCurrentValue(), bar->getPercentage());
		}
		break;
	default:
		break;
	}
}

//
// NSliderBar_Vertical
//
NSliderBar_Vertical::NSliderBar_Vertical()
{
	this->setIndex(1);
}

NSliderBar_Vertical::~NSliderBar_Vertical()
{

}

bool NSliderBar_Vertical::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNSliderBar::init());

	NScale9Sprite* bg = NScale9Sprite::create("background3.png");
	this->addChild(bg, -1);
	bg->setContentSize(Size(VISIBLE_SIZE.width * 0.85f, VISIBLE_SIZE.height * 0.85f));
	bg->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));

	_slider_bar = NSliderBar::create("progress_v_bg.png", "progress_v.png");
	_slider_bar->setSliderBar("slider.png");
	this->addChild(_slider_bar);
	_slider_bar->setDirection(Bar_Direction::kBar_Dir_ButtomToTop);
	_slider_bar->setPercentage(0.2f);
	_slider_bar->setPosition(VISIBLE_SIZE_CENTER);
	_slider_bar->setMaxValue(100);
	_slider_bar->setMinValue(0);
	_slider_bar->setBarCallback(CALLBACK_2(NSliderBar_Vertical::barShowCallback, this));

	NButton* btn_exchange_direction = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_exchange_direction);
	btn_exchange_direction->setPosition(POS_BY_VISIBLE_SIZE(0.15f, 0.5f));
	btn_exchange_direction->setCallback(CALLBACK_2(NSliderBar_Vertical::btnDirectionCallback, this));
	btn_exchange_direction->setText("enchange direction");

	NButton* btn_add_progress_value = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_add_progress_value);
	btn_add_progress_value->setPosition(POS_BY_VISIBLE_SIZE(0.3f, 0.75f));
	btn_add_progress_value->setCallback(CALLBACK_2(NSliderBar_Vertical::btnAddProgressCallback, this));
	btn_add_progress_value->setTag(1);
	btn_add_progress_value->setText("add progress value");

	NButton* btn_add_progress_percentage = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_add_progress_percentage);
	btn_add_progress_percentage->setPosition(POS_BY_VISIBLE_SIZE(0.7f, 0.75f));
	btn_add_progress_percentage->setCallback(CALLBACK_2(NSliderBar_Vertical::btnAddProgressCallback, this));
	btn_add_progress_percentage->setTag(2);
	btn_add_progress_percentage->setText("add progress perventage");

	NButton* btn_reduce_progress_value = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_reduce_progress_value);
	btn_reduce_progress_value->setPosition(POS_BY_VISIBLE_SIZE(0.3f, 0.25f));
	btn_reduce_progress_value->setCallback(CALLBACK_2(NSliderBar_Vertical::btnReduceProgressCallback, this));
	btn_reduce_progress_value->setTag(1);
	btn_reduce_progress_value->setText("reduce progress value");

	NButton* btn_reduce_progress_percentage = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_reduce_progress_percentage);
	btn_reduce_progress_percentage->setPosition(POS_BY_VISIBLE_SIZE(0.7f, 0.25f));
	btn_reduce_progress_percentage->setCallback(CALLBACK_2(NSliderBar_Vertical::btnReduceProgressCallback, this));
	btn_reduce_progress_percentage->setTag(2);
	btn_reduce_progress_percentage->setText("reduce progress percentage");

	NButton* btn_stop_progress = NButton::create("btn_1.png", "btn_2.png", "btn_2.png", "");
	this->addChild(btn_stop_progress);
	btn_stop_progress->setPosition(POS_BY_VISIBLE_SIZE(0.85f, 0.5f));
	btn_stop_progress->setCallback(CALLBACK_2(NSliderBar_Vertical::btnStopCallback, this));
	btn_stop_progress->setText("stop progress");

	INIT_DO_WHILE_END
}

void NSliderBar_Vertical::btnAddProgressCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		if (btn->getTag() == 1)
		{
			_slider_bar->startValueBy(0.2f, 5);
		}
		else
		{
			_slider_bar->startPercentageTo(2.0f, 1.0f);
		}
	}
}

void NSliderBar_Vertical::btnReduceProgressCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		if (btn->getTag() == 1)
		{
			_slider_bar->startValueBy(0.2f, -5);
		}
		else
		{
			_slider_bar->startPercentageTo(2.0f, 0.0f);
		}
	}
}

void NSliderBar_Vertical::btnDirectionCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		if (_slider_bar->getDirection() == Bar_Direction::kBar_Dir_ButtomToTop)
		{
			_slider_bar->setDirection(Bar_Direction::kBar_Dir_TopToButtom);
		}
		else
		{
			_slider_bar->setDirection(Bar_Direction::kBar_Dir_ButtomToTop);
		}
	}
}

void NSliderBar_Vertical::btnStopCallback(Ref* object, Touch_Period touch_period)
{
	if ( touch_period == Touch_Period::kTouchEnded)
	{
		_slider_bar->stopUpdatePercentage();
	}
}

void NSliderBar_Vertical::barShowCallback(Ref* object, Bar_Period bar_period)
{
	NSliderBar* bar = (NSliderBar*)object;
	switch (bar_period)
	{
	case Bar_Period::kBar_Began:
		{
			log("bar began");
			log("Progress Bar current value : %d, current percentage : %f", bar->getCurrentValue(), bar->getPercentage());
		}
		break;
	case Bar_Period::kBar_Move:
		{
			log("bar change");
			log("Progress Bar current value : %d, current percentage : %f", bar->getCurrentValue(), bar->getPercentage());
		}
		break;
	case Bar_Period::kBar_End:
		{
			log("bar end");
			log("Progress Bar current value : %d, current percentage : %f", bar->getCurrentValue(), bar->getPercentage());
		}
		break;
	default:
		break;
	}
}