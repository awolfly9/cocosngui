#include "TestNMessageTips.h"

//
// TestNMessageTips
//
TestNMessageTips::TestNMessageTips(void)
{
}

TestNMessageTips::~TestNMessageTips(void)
{
}

bool TestNMessageTips::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

void TestNMessageTips::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextNMessageTipsLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNMessageTips::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackNMessageTipsLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNMessageTips::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainNMessageTipsLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NMessageTips_Info
//
NMessageTips_Info::NMessageTips_Info()
{
	this->setIndex(0);
}

NMessageTips_Info::~NMessageTips_Info()
{
	NMessageTips::getInstance()->clear();
}

bool NMessageTips_Info::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNMessageTips::init());

	NMessageTips::getInstance()->init("bg.png", "Arial", 30, Color3B::RED);

	auto listener = EventListenerTouchOneByOne::create();
	listener->onTouchBegan = CALLBACK_2(NMessageTips_Info::onTouchBegan, this);
	listener->onTouchEnded = CALLBACK_2(NMessageTips_Info::onTouchMoved, this);
	listener->onTouchEnded = CALLBACK_2(NMessageTips_Info::onTouchEnded, this);
	listener->onTouchCancelled = CALLBACK_2(NMessageTips_Info::onTouchCancelled, this);

	listener->setSwallowTouches(true);
	Director::getInstance()->getEventDispatcher()->addEventListenerWithSceneGraphPriority(listener, this);

	INIT_DO_WHILE_END
}

bool NMessageTips_Info::onTouchBegan(Touch *touch, Event *unused_event)
{
	return true;
}

void NMessageTips_Info::onTouchMoved(Touch *touch, Event *unused_event)
{

}

void NMessageTips_Info::onTouchEnded(Touch *touch, Event *unused_event)
{
	NMessageTips::getInstance()->addTips("test message tips");
}

void NMessageTips_Info::onTouchCancelled(Touch *touch, Event *unused_event)
{

}