#ifndef __TEST_SWITCH_BUTTON_H__
#define __TEST_SWITCH_BUTTON_H__

#include "BasicLayer.h"
#define MAX_SWITCH_BUTTON_LAYER_INDEX 1

/**
TestNSwitchButton
*/
class TestNSwitchButton : public BasicLayer
{
public:
	TestNSwitchButton(void);
	~TestNSwitchButton(void);

	CREATE_FUNC(TestNSwitchButton);
	virtual bool init();

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};

/**
TestNSwitchButton
*/
class NSwitchButton_Show : public TestNSwitchButton
{
public:
	NSwitchButton_Show();
	~NSwitchButton_Show();

	CREATE_FUNC(NSwitchButton_Show);
	virtual bool init();

private:
	void callback(Ref* ref, Touch_Period touch_period);

private:
	Label* _lbl;
};

////////////////////////////////////////////////////////
static Layer* createNSwitchButtonLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NSwitchButton_Show::create();
		}
		break;
	default:
		break;
	}

	return nullptr;
}

static Layer* onNextNSwitchButtonLayer(int index)
{
	index ++;

	if (index == MAX_SWITCH_BUTTON_LAYER_INDEX)
	{
		index = 0;
	}

	return createNSwitchButtonLayer(index);
}

static Layer* onBackNSwitchButtonLayer(int index)
{
	index --;

	if (index == -1)
	{
		index = MAX_SWITCH_BUTTON_LAYER_INDEX - 1;
	}

	return createNSwitchButtonLayer(index);
}

static Layer* onAgainNSwitchButtonLayer(int index)
{
	return createNSwitchButtonLayer(index);
}


#endif // __TEST_SWITCH_BUTTON_H__