#include "TestNScrollView.h"



TestNScrollView::TestNScrollView(void)
{
	this->setIndex(-1);
}

TestNScrollView::~TestNScrollView(void)
{
}

bool TestNScrollView::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

void TestNScrollView::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextNScrollViewLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNScrollView::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackNScrollViewLayer(_index);
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNScrollView::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainNScrollViewLayer(_index);
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NScrollView_Drag
//
NScrollView_Drag::NScrollView_Drag(void)
{
	this->setIndex(0);
}

NScrollView_Drag::~NScrollView_Drag(void)
{

}

bool NScrollView_Drag::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNScrollView::init());

	this->setTitle("NScrollViewSlide");
	this->setDescription("you can drag");

	NScrollView* scroll_view = NScrollView::create(Size(VISIBLE_SIZE.width / 2, VISIBLE_SIZE.height / 2));
	this->addChild(scroll_view);
	scroll_view->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));
	scroll_view->setDirection(ScrollView_Direction::kScrollView_Dir_Both);

	Sprite* sprite = Sprite::create("scrollcontent.png");
	scroll_view->addChild(sprite);
	scroll_view->setContainerSize(sprite->getContentSize());
	sprite->setPosition(Vec2(0, 0));
	sprite->setAnchorPoint(Vec2(0.0f, 0.0f));
	scroll_view->setBouncePercent(Vec2(0.5f, 0.5f));

	INIT_DO_WHILE_END
}

//
// NScrollView_Dirction
//
NScrollView_Dirction::NScrollView_Dirction(void)
{
	this->setIndex(1);
}

NScrollView_Dirction::~NScrollView_Dirction(void)
{

}

bool NScrollView_Dirction::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNScrollView::init());

	_scroll_view = NScrollView::create(Size(VISIBLE_SIZE.width * 0.5f, VISIBLE_SIZE.height * 0.5f));
	this->addChild(_scroll_view);
	_scroll_view->setPosition(VISIBLE_SIZE_CENTER);

	Sprite* sprite = Sprite::create("scrollcontent.png");
	_scroll_view->addChild(sprite);
	_scroll_view->setContainerSize(sprite->getContentSize());
	sprite->setAnchorPoint(Vec2(0.0f, 0.0f));
	sprite->setPosition(Vec2(0, 0));

	NButton* btn_vertical = NButton::create("btn_1.png");
	this->addChild(btn_vertical);
	btn_vertical->setCallback(CALLBACK_2(NScrollView_Dirction::btnCallback, this));
	btn_vertical->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.85f));
	btn_vertical->setTag((int)ScrollView_Direction::kScrollView_Dir_Vertical);
	btn_vertical->setText("Vertical");

	NButton* btn_horizontal = NButton::create("btn_1.png");
	this->addChild(btn_horizontal);
	btn_horizontal->setCallback(CALLBACK_2(NScrollView_Dirction::btnCallback, this));
	btn_horizontal->setPosition(POS_BY_VISIBLE_SIZE(0.15f, 0.5f));
	btn_horizontal->setTag((int)ScrollView_Direction::kScrollView_Dir_Horizontal);
	btn_horizontal->setText("Horizontal");

	NButton* btn_both = NButton::create("btn_1.png");
	this->addChild(btn_both);
	btn_both->setCallback(CALLBACK_2(NScrollView_Dirction::btnCallback, this));
	btn_both->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.15f));
	btn_both->setTag((int)ScrollView_Direction::kScrollView_Dir_Both);
	btn_both->setText("Both");
	INIT_DO_WHILE_END
}

void NScrollView_Dirction::btnCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;

		_scroll_view->setDirection((ScrollView_Direction)btn->getTag());
	}
}

//
// NScrollView_Embed
//
NScrollView_Embed::NScrollView_Embed(void)
{
	this->setIndex(2);
}

NScrollView_Embed::~NScrollView_Embed(void)
{

}

bool NScrollView_Embed::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! NScrollView_Dirction::init());

	NScrollView* scroll = NScrollView::create(Size(VISIBLE_SIZE.width * 0.25f, VISIBLE_SIZE.height * 0.25f));
	this->addChild(scroll);
	scroll->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.5f));

	Sprite* sp = Sprite::create("scrollcontent.png");
	scroll->addChild(sp);
	scroll->setContainerSize(sp->getContentSize());
	sp->setAnchorPoint(Vec2(0.0f, 0.0f));
	sp->setPosition(Vec2(0, 0));

	INIT_DO_WHILE_END
}

//
// NScrollView_ScrollBar_Horizontal
//
NScrollView_ScrollBar_Horizontal::NScrollView_ScrollBar_Horizontal(void)
{
	this->setIndex(3);
}

NScrollView_ScrollBar_Horizontal::~NScrollView_ScrollBar_Horizontal(void)
{

}

bool NScrollView_ScrollBar_Horizontal::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNScrollView::init());

	NScrollView* scroll_view = NScrollView::create(Size(500, 300));
	this->addChild(scroll_view);
	scroll_view->setPosition(VISIBLE_SIZE_CENTER);
	scroll_view->setDirection(ScrollView_Direction::kScrollView_Dir_Horizontal);

	scroll_view->setScrollBar("progress_bg.png", "scroll_bar_2.png", "scroll_bar_1.png");
	scroll_view->setScrollBarDirection(Bar_Direction::kBar_Dir_RightToLeft);
	scroll_view->setScrollBarPositionFactor(0.0f);

	Sprite* sprite = Sprite::create("scrollcontent.png");
	scroll_view->setContainerSize(sprite->getContentSize());
	scroll_view->addChild(sprite);
	sprite->setAnchorPoint(Vec2(0.0f, 0.0f));
	sprite->setPosition(Vec2(0, 0));	

	INIT_DO_WHILE_END
}

//
// NScrollView_ScrollBar_Horizontal
//
NScrollView_ScrollBar_Vertical::NScrollView_ScrollBar_Vertical(void)
{
	this->setIndex(4);
}

NScrollView_ScrollBar_Vertical::~NScrollView_ScrollBar_Vertical(void)
{

}

bool NScrollView_ScrollBar_Vertical::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNScrollView::init());

	NScrollView* scroll_view = NScrollView::create(Size(300, 500));
	this->addChild(scroll_view);
	scroll_view->setPosition(VISIBLE_SIZE_CENTER);
	scroll_view->setDirection(ScrollView_Direction::kScrollView_Dir_Vertical);

	scroll_view->setScrollBar("progress_v_bg.png", "scroll_bar_v_2.png", "scroll_bar_v_1.png");
	scroll_view->setScrollBarDirection(Bar_Direction::kBar_Dir_TopToButtom);
	scroll_view->setScrollBarPositionFactor(0.0f);

	Sprite* sprite = Sprite::create("scrollcontent.png");
	scroll_view->setContainerSize(sprite->getContentSize());
	scroll_view->addChild(sprite);
	sprite->setAnchorPoint(Vec2(0.0f, 0.0f));
	sprite->setPosition(Vec2(0, 0));	

	INIT_DO_WHILE_END
}