#include "TestNButton.h"

//
// TestNButton
// 
TestNButton::TestNButton(void)
{
	this->setIndex(-1);
}

TestNButton::~TestNButton(void)
{
}

bool TestNButton::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! BasicLayer::init());

	INIT_DO_WHILE_END
}

void TestNButton::btnNextCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onNextNButtonLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNButton::btnbackCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onBackNButtonLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

void TestNButton::btnAgainCallabck(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		Layer* layer = onAgainNButtonLayer(this->getIndex());
		Scene* scene = Scene::create();
		scene->addChild(layer);
		Director::getInstance()->replaceScene(scene);
	}
}

//
// NButton_Callback
// 
NButton_Callback::NButton_Callback(void)
	: _lbl_text(nullptr)
{
	this->setIndex(0);
}

NButton_Callback::~NButton_Callback(void)
{
}

bool NButton_Callback::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNButton::init());

	NButton* btn_1 = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	this->addChild(btn_1);
	btn_1->setPosition(POS_BY_VISIBLE_SIZE(0.3f, 0.4f));
	btn_1->setCallback(CALLBACK_2(NButton_Callback::btnCallback, this));
	btn_1->setName("normal btn");
	btn_1->setText("normal btn");

	NButton* btn_2 = NButton::create("sprite9_btn1.png", "sprite9_btn2.png", Size(200, 80));
	this->addChild(btn_2);
	btn_2->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.4f));
	btn_2->setCallback(CALLBACK_2(NButton_Callback::btnCallback, this));
	btn_2->setName("scale btn");
	btn_2->setText("scale btn");

	_lbl_text = NLabelTTF::create("", "Airal", 30);
	_lbl_text->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.6f));
	this->addChild(_lbl_text);

	INIT_DO_WHILE_END
}

void NButton_Callback::btnCallback(Ref* object, Touch_Period touch_period)
{
	NButton* btn = (NButton*)object;
	if (touch_period == Touch_Period::kTouchBegan)
	{
		string str = btn->getName() + "  Touch began";
		_lbl_text->setString(str.c_str());
	}
	else if (touch_period == Touch_Period::kTouchMoved)
	{
		string str = btn->getName() + "  Touch move";
		_lbl_text->setString(str.c_str());
	}
	else if (touch_period == Touch_Period::kTouchEnded)
	{
		string str = btn->getName() + "  Touch End";
		_lbl_text->setString(str.c_str());
	}
	else
	{
		
	}
}

//
// NButton_Zorder
//
NButton_Zorder::NButton_Zorder(void)
	: _btn_1(nullptr)
	, _btn_2(nullptr)
{
	this->setIndex(1);
}

NButton_Zorder::~NButton_Zorder(void)
{
}

bool NButton_Zorder::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNButton::init());

	_btn_1 = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	this->addChild(_btn_1);
	_btn_1->setPosition(POS_BY_VISIBLE_SIZE(0.4f, 0.4f));
	_btn_1->setCallback(CALLBACK_2(NButton_Zorder::btnCallback, this));
	_btn_1->setName("normal btn");
	_btn_1->setText("normal btn");

	_btn_2 = NButton::create("sprite9_btn1.png", "sprite9_btn2.png", Size(200, 80));
	this->addChild(_btn_2);
	_btn_2->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.4f));
	_btn_2->setCallback(CALLBACK_2(NButton_Zorder::btnCallback, this));
	_btn_2->setName("scale btn");
	_btn_2->setText("scale btn");

	INIT_DO_WHILE_END
}

void NButton_Zorder::btnCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;
		
		if (btn->getName() == "normal button")
		{
			btn->setZOrder(_btn_2->getZOrder() + 1);
		}
		else
		{
			btn->setZOrder(_btn_1->getZOrder() + 1);
		}
	}
}

//
// NButton_State
//
NButton_State::NButton_State(void)
{
	this->setIndex(2);
}

NButton_State::~NButton_State(void)
{

}

bool NButton_State::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNButton::init());

	// touch enabled
	NLabelTTF* lbl_2 = NLabelTTF::create("setTouchEnable", "Arial", 30);
	this->addChild(lbl_2);
	lbl_2->setPosition(POS_BY_VISIBLE_SIZE(0.2f, 0.6f));

	NButton* btn_2 = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	this->addChild(btn_2);
	btn_2->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.6f));
	btn_2->setText("ture");

	NButton* btn_2_1 = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	this->addChild(btn_2_1);
	btn_2_1->setPosition(POS_BY_VISIBLE_SIZE(0.8f, 0.6f));
	btn_2_1->setTouchEnabled(false);
	btn_2_1->setText("false");

	// enabled
	NLabelTTF* lbl_3 = NLabelTTF::create("setEnabled", "Arial", 30);
	this->addChild(lbl_3);
	lbl_3->setPosition(POS_BY_VISIBLE_SIZE(0.2f, 0.45f));

	NButton* btn_3 = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	this->addChild(btn_3);
	btn_3->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.45f));
	btn_3->setText("true");

	NButton* btn_3_1 = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	this->addChild(btn_3_1);
	btn_3_1->setPosition(POS_BY_VISIBLE_SIZE(0.8f, 0.45f));
	btn_3_1->setEnabled(false);
	btn_3_1->setText("false");

	// viaible
	NLabelTTF* lbl_4 = NLabelTTF::create("setVisible", "Arial", 30);
	this->addChild(lbl_4);
	lbl_4->setPosition(POS_BY_VISIBLE_SIZE(0.2f, 0.3f));

	NButton* btn_4 = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	this->addChild(btn_4);
	btn_4->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.3f));
	btn_4->setText("true");

	NButton* btn_4_1 = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	this->addChild(btn_4_1);
	btn_4_1->setPosition(POS_BY_VISIBLE_SIZE(0.8f, 0.3f));
	btn_4_1->setVisible(false);
	btn_4_1->setText("false");
	
	INIT_DO_WHILE_END
}

//
// NButton_Text
//
NButton_Text::NButton_Text(void)
{
	this->setIndex(3);
}

NButton_Text::~NButton_Text(void)
{

}

bool NButton_Text::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNButton::init());

	NButton* btn_1 = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	this->addChild(btn_1);
	btn_1->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.6f));
	btn_1->setText("Text");
	
	NButton* btn_2 = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	this->addChild(btn_2);
	btn_2->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.45f));
	btn_2->setText("Text", "LiShu", 50);
	btn_2->setTextNormalColor(Color3B(255, 0, 0));
	btn_2->setTextSelectedColor(Color3B(0, 255, 0));
	btn_2->setTextDisabledColor(Color3B(0, 0, 255));

	NButton* btn_3 = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	this->addChild(btn_3);
	btn_3->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.3f));
	btn_3->setText("Text", "LiShu", 50);
	btn_3->setTextNormalColor(Color3B(255, 0, 0));
	btn_3->setTextSelectedColor(Color3B(0, 255, 0));
	btn_3->setTextDisabledColor(Color3B(0, 0, 255));
	btn_3->setEnabled(false);

	INIT_DO_WHILE_END
}

//
// NButton_Action
//
NButton_Action::NButton_Action(void)
{
	this->setIndex(4);
}

NButton_Action::~NButton_Action(void)
{
}

bool NButton_Action::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNButton::init());

	NButton* btn_1 = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	this->addChild(btn_1);
	btn_1->setPosition(POS_BY_VISIBLE_SIZE(0.3f, 0.4f));
	btn_1->setText("Action");
	btn_1->setPressScale(1.2f);

	NButton* btn_2 = NButton::create("sprite9_btn1.png", "sprite9_btn2.png", Size(200, 80));
	this->addChild(btn_2);
	btn_2->setPosition(POS_BY_VISIBLE_SIZE(0.68f, 0.4f));
	btn_2->setText("Action");
	btn_2->setPressScale(0.8f);
	
	NButton* btn_3 = NButton::create("sprite9_btn1.png", "sprite9_btn2.png", Size(200, 80));
	this->addChild(btn_3);
	btn_3->setPosition(POS_BY_VISIBLE_SIZE(0.5f, 0.6f));
	btn_3->setText("Action");
	btn_3->setPressScale(1.2f);

	INIT_DO_WHILE_END
}

//
// NButton_Tips
//
NButton_Tips::NButton_Tips(void)
{
	this->setIndex(5);
}

NButton_Tips::~NButton_Tips(void)
{
}

bool NButton_Tips::init()
{
	INIT_DO_WHILE_BEGIN
		BREAK_IF(! TestNButton::init());

	NButton* btn_1 = NButton::create("btn_1.png", "btn_2.png", "btn_3.png", "");
	this->addChild(btn_1);
	btn_1->setPosition(POS_BY_VISIBLE_SIZE(0.3f, 0.4f));
	btn_1->setText("Tips");
	btn_1->setPressScale(1.2f);
	btn_1->setTipsImage("tips_image.png");
	btn_1->setTipsImagePosition(Vec2(btn_1->getContentSize().width / 2, btn_1->getContentSize().height / 2));
	btn_1->setCallback(CALLBACK_2(NButton_Tips::btnCallback, this));


	NButton* btn_2 = NButton::create("sprite9_btn1.png", "sprite9_btn2.png", Size(200, 80));
	this->addChild(btn_2);
	btn_2->setPosition(POS_BY_VISIBLE_SIZE(0.6f, 0.4f));
	btn_2->setText("Tips");
	btn_2->setPressScale(0.8f);
	btn_2->setTipsImage("tips_image.png");
	btn_2->setCallback(CALLBACK_2(NButton_Tips::btnCallback, this));

	INIT_DO_WHILE_END
}

void NButton_Tips::btnCallback(Ref* object, Touch_Period touch_period)
{
	if (touch_period == Touch_Period::kTouchEnded)
	{
		NButton* btn = (NButton*)object;
		
		if (btn->isTips())
		{
			btn->setTips(false);
		}
		else
		{
			btn->setTips(true);
		}
	}
}