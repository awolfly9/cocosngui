#ifndef __TEST_NSLIDER_BAR_H__
#define __TEST_NSLIDER_BAR_H__

#include "BasicLayer.h"

#define MAX_SLIDER_BAR_LAYER_INDEX 2

class TestNSliderBar : public BasicLayer
{
public:
	TestNSliderBar(void);
	~TestNSliderBar(void);

	CREATE_FUNC(TestNSliderBar);
	virtual bool init();

private:
	virtual void btnNextCallback(Ref* object, Touch_Period touch_period);
	virtual void btnbackCallback(Ref* object, Touch_Period touch_period);
	virtual void btnAgainCallabck(Ref* object, Touch_Period touch_period);
};

class NSliderBar_Horizontal : public TestNSliderBar
{
public:
	NSliderBar_Horizontal();
	~NSliderBar_Horizontal();

	CREATE_FUNC(NSliderBar_Horizontal);
	virtual bool init();

private:
	void btnAddProgressCallback(Ref* object, Touch_Period touch_period);
	void btnReduceProgressCallback(Ref* object, Touch_Period touch_period);
	void btnDirectionCallback(Ref* object, Touch_Period touch_period);
	void btnStopCallback(Ref* object, Touch_Period touch_period);

	void barShowCallback(Ref* object, Bar_Period bar_period);

private:
	NSliderBar* _slider_bar;
};


class NSliderBar_Vertical : public TestNSliderBar
{
public:
	NSliderBar_Vertical();
	~NSliderBar_Vertical();

	CREATE_FUNC(NSliderBar_Vertical);
	virtual bool init();

private:
	void btnAddProgressCallback(Ref* object, Touch_Period touch_period);
	void btnReduceProgressCallback(Ref* object, Touch_Period touch_period);
	void btnDirectionCallback(Ref* object, Touch_Period touch_period);
	void btnStopCallback(Ref* object, Touch_Period touch_period);

	void barShowCallback(Ref* object, Bar_Period bar_period);

private:
	NSliderBar* _slider_bar;
};

////////////////////////////////////////////////////////
static Layer* createNSliderBarLayer(int index)
{
	switch (index)
	{
	case 0:
		{
			return NSliderBar_Horizontal::create();
		}

		break;
	case 1:
		{
			return NSliderBar_Vertical::create();
		}

		break;
	case 2:
		{
			//return ButtonState::create();
		}

		break;
	case 3:
		{
			//return ButtonText::create();
		}

		break;
	case 4:
		{
			//return ButtonScale9Sprite::create();
		}

		break;
	default:

		break;
	}

	return nullptr;
}

static Layer* onNextNSliderBarLayer(int index)
{
	index ++;

	if(index == MAX_SLIDER_BAR_LAYER_INDEX)
	{
		index = 0;
	}

	return createNSliderBarLayer(index);
}

static Layer* onBackNSliderBarLayer(int index)
{
	index --;

	if(index == -1)
	{
		index = MAX_SLIDER_BAR_LAYER_INDEX - 1;
	}

	return createNSliderBarLayer(index);
}

static Layer* onAgainNSliderBarLayer(int index)
{
	return createNSliderBarLayer(index);
}

#endif // __TEST_NSLIDER_BAR_H__
